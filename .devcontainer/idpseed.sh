#!/bin/sh

# Read .env file
export $(grep -v '^#' /workspaces/egendata-wallet/secrets.env | xargs)

# -s -o /dev/null -w "%{json}"
curl -v --insecure https://identity-provider.localhost/.admin/serviceUser \
  -H "Expect:" \
  -H "Accept: application/json" \
  -H "X-Auth-Key: ${CSS_ADMIN_ACCESS_KEY}" \
  -H "Content-Type: application/json" \
  -d "{
    \"podName\": \"template-test1-sink\",
    \"name\": \"TemplateTest1Sink\",
    \"clientId\": \"${DC_CLIENT_ID}\",
    \"clientSecret\": \"${DC_CLIENT_SECRET}\"
    }"
    #  | jq --raw-output '("devcontainer/idpseed.sh:" + (.response_code | tostring))'

curl -v --insecure https://identity-provider.localhost/.admin/serviceUser \
  -H "Expect:" \
  -H "Accept: application/json" \
  -H "X-Auth-Key: ${CSS_ADMIN_ACCESS_KEY}" \
  -H "Content-Type: application/json" \
  -d "{
    \"podName\": \"template-test1-source\",
    \"name\": \"TemplateTest1Source\",
    \"clientId\": \"${CLIENT_ID}\",
    \"clientSecret\": \"${CLIENT_SECRET}\"
    }"
    # \"logo\": \"data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgdmVyc2lvbj0iMS4xInhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI-DQogICA8cmVjdCBzdHlsZT0iZmlsbDojMDAwMDVhO3N0cm9rZS13aWR0aDoxIiB3aWR0aD0iMjAiIGhlaWdodD0iMjAiIHg9IjIiIHk9IjIiIHJ4PSIxIiByeT0iMSIgLz4NCjwvc3ZnPg0K\", 
    # \"adminWebId\": \"https://identity-provider.localhost/7e13d163-d0e0-45f8-b2ef-6c8fd5d5ff8a/profile/card#me\",
    # \"template\": {\"name\": \"JobSeekerRegistrationStatus\", \"content\": {\"objectType\": \"ProviderTemplate\", \"documentTitle\": \"TestData\"}}
    #  | jq --raw-output '("devcontainer/idpseed.sh:" + (.response_code | tostring))'
