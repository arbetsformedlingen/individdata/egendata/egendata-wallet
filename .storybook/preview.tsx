import { withThemeByDataAttribute } from '@storybook/addon-styling';
import React from 'react';
import { IntlProvider } from 'react-intl';
import LOCALES from '../src/locales';

import '../src/styles/globals.css';

export const parameters = {
  darkMode: { darkClass: 'dark', lightClass: 'light', classTarget: 'html', stylePreview: true },
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

export const decorators = [
  (Story) => {
    return (
      <IntlProvider locale={'en'} messages={LOCALES['en']}>
        <Story />
      </IntlProvider>
    );
  },
  withThemeByDataAttribute({
    themes: {
      light: 'light',
      dark: 'dark',
    },
    defaultTheme: 'dark',
    attributeName: 'class',
  }),
];
