# syntax=docker/dockerfile:1

# Install dependencies
FROM node:16.19.1-bullseye-slim as deps
RUN npm upgrade -g npm

WORKDIR /app

COPY package.json package-lock.json .npmrc ./
# RUN npm clean-install
RUN npm clean-install

# Install git and extract commit hash
FROM deps as git
RUN apt-get update && \
  apt-get install -y git && \
  rm -rf /var/lib/apt/lists/*
WORKDIR /app
COPY .git ./

## Echo and store current git commit in file.
# RUN touch /app/git-commit && git rev-parse --short HEAD > >(tee -a ./git-commit)
RUN touch /app/git-commit && git rev-parse --short HEAD 2>&1 | tee -a ./git-commit

# Build from source when needed
FROM node:16.19.1-bullseye-slim as build
RUN npm upgrade -g npm

WORKDIR /app

COPY --from=deps /app/node_modules ./node_modules
COPY . .
COPY --from=git /app/git-commit ./

ENV NEXT_TELEMETRY_DISABLED=1

RUN SKIP_ENV_VALIDATION=1 npm run build

# Production image, copy files and run
FROM node:16.19.1-bullseye-slim

WORKDIR /app

ENV NODE_ENV=production \
  NEXT_TELEMETRY_DISABLED=1

RUN adduser --system --uid 1001 nextjs

COPY --chown=nextjs:node ./entrypoint.sh /
COPY --chown=nextjs:node ./get_credentials.js ./
COPY --from=deps --chown=nextjs:node /app/node_modules/dotenv ./node_modules/dotenv

COPY --from=build --chown=nextjs:node /app/public ./public
COPY --from=build --chown=nextjs:node /app/.next/standalone ./
COPY --from=build --chown=nextjs:node /app/.next/static ./.next/static
COPY --from=git --chown=nextjs:node /app/git-commit ./

USER nextjs:node

EXPOSE 3000

ENV PORT 3000

ENTRYPOINT [ "/entrypoint.sh" ]
CMD ["node", "server.js"]
