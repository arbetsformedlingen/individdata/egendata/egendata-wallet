# Digital wallet

This is the digital wallet application. It is a React application built with TypeScript, Next.JS, tRPC, TailwindCSS and HeadlessUI.

Documentation of the system can be found at [Egendata Wiki](https://gitlab.com/groups/arbetsformedlingen/individdata/egendata/-/wikis/home).

It uses devcontainers and development is done using podman but it should work with docker as well.
To use podman with Visual Studio Code change the settings `dev.containers.dockerPath` and `dev.containers.dockerComposePath` to use `podman` and `podman-compose` respectively.

## Getting Started

The repository uses devcontainers.

1. For the Identity Provider to function you need to provide it with credentials for the integration with Visma.
   Create a file named `secrets.env` in the repository root. Add the following keys.
   ( Non-secret environment variables are defined in files in `.devcontainer/env/`. )

```env
# Wallet secrets
EGENDATA_INFRA_ID=<client id>
EGENDATA_INFRA_SECRET=<client secret>
#
# Identity provider secrets
CSS_ADMIN_ACCESS_KEY=<admin api access key>
CSS_VISMA_CUSTOMER_KEY=<visma oauth customer key>
CSS_VISMA_SERVICE_KEY=<visma oauth service key>
CSS_CLIENT_ID=<client id>
CSS_CLIENT_SECRET=<client secret>
#
# Data provider
CLIENT_ID=<data provider pod client name>
CLIENT_SECRET=<data provider pod client secret>
AIS_CLIENT_ID=<AIS client id>
AIS_CLIENT_SECRET=<AIS client secret>
#
# Data consumer
DC_CLIENT_ID=<data requestor pod client name>
DC_CLIENT_SECRET=<data requestor pod client secret>
```

2. Rebuild the devcontainer.

3. Default environment variables for the wallet can be found in `.env.example`.
   Create a copy in `.env` file and update `NEXTAUTH_SECRET`.
   For more information about environment variable load order see the
   [Next.js documentation](https://nextjs.org/docs/basic-features/environment-variables#environment-variable-load-order).

4. Copy the root certficate created by the Caddy container.
   Located in `.devcontainer/container-storage/caddy/data/pki/authorities/local/root.crt` or
   use the provided task in VS Code to copy it to the project root.

5. Add the `root.crt` to your web browser or OS.

6. Start the development server in the devcontainer: `npm run dev`

7. Open [https://wallet.localhost](https://wallet.localhost) with your browser to see the result.

_Note:_ `AIS_CLIENT_ID` and `AIS_CLIENT_SECRET` does not matter when `USE_AIS_MOCK` is set for the data provider.

_Note:_ Depending on your DNS setup you might need to add `wallet.localhost`, `identity-provider.localhost`,
`pod-provider.localhost` and `data-provider.localhost` to your systems hosts file.

## Container

When using podman you must authenticate against JobTech's Nexus server.
It can be done with the `podman login docker-images.jobtechdev.se` in a terminal.

## Internationalization (i18n)

All messages should have unique ids. ESlint enforces and generates them when you run:

```
npm run lint:fix
```

To extract all messages from the code base run the following command.
The `lang/en.json` file will be replaced and the `lang/sv.json` will be updated.

```
npm run lang:extract
```

To compile new language files run:

```
npm run lang:compile
```

## Podman compose

To control the podman compose services the `-f` and `-p` parameters can be used.
`podman-compose -f .devcontainer/docker-compose.development.yml -p egendatawalletdevcontainer logs`

## Self-signed certificates

Caddy automatically creates self-signed certificates. As long as you keep the `.devcontainer/container-storage/caddy` folder they should not change.

The easiest way to get the root certificate onto your development machine run the task _Copy root certificate to host_. The `root.crt` file will appear in the project root.

Install the certificate as an _authority_ certificate in your browser, make sure to allow it to be used for _identification of web pages_.

## Tests

The project uses Vitest, Storybook and Playwright for testing.

Tests run with Vitest is unit and integration tests.
Storybook is used for visual testing of UI components.
Playwright is used for end-to-end tests.

To run the unit and integration tests with vitest use `npm run test` for watch mode and `npm run test:run` for a single run.

If you want coverage use `npm run test:coverage` and the coverage report will be available at `coverage/index.html`.

### Storybook

All visual components of the project can be inspected and manually tested with the help of Storybook. `npm run storybook` to get started.

### Playwright

E2E tests can be run using Playwright. They will be run with real web browsers and use the entire Egendata infrastructure (within the devcontainer system).

Before running the tests make sure that the development server is running (`npm run dev`).
Run the tests with `npx playwright test` and show the results with `npx playwright show-report`.

When running outside of the devcontainer you need to run `npx playwright install` first.
