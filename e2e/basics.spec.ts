import { expect, test } from '@playwright/test';

test.beforeEach(async ({ page }, testInfo) => {
  console.log(`Running ${testInfo.title}`);

  await page.route('**', async (route) => {
    const response = await route.fetch();
    await route.fulfill({ response });
  });
});

test('index page contains correct elements', async ({ page }) => {
  await page.goto('/');

  await expect(page).toHaveTitle(/Egendata/);

  await expect(page.getByTestId('title')).toBeDefined();
  await expect(page.getByTestId('subtitle')).toBeDefined();
  await expect(page.getByTestId('welcome-text')).toBeDefined();
  await expect(page.getByTestId('identify-to-continue')).toBeDefined();
  await expect(page.getByTestId('login-button')).toBeDefined();
  await expect(page.getByTestId('how-to-login')).toBeDefined();
});

test('can login to application', async ({ page }) => {
  await page.goto('/');

  await page.getByRole('button').click();

  await page.getByLabel('fname').fill('Ture');
  await page.getByLabel('lname').fill('Testare');
  await page.getByLabel('pnr').fill('19990101-2332');

  await page.getByRole('button').click();

  await expect(page.url()).toMatch(/wallet.localhost\/home/);
});
