#!/bin/sh
set -e

# Dynamic registration of client credentials
eval `./get_credentials.js`

exec "$@"
