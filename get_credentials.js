#!/usr/bin/env node

const fetch = async (...args) => {
  const { default: fetch } = await import('node-fetch');
  return fetch(...args);
};
require('dotenv').config();

const registerCredentials = async () => {
  // Call the registration endpoint.
  // openid-client has some support for this?
  // See https://github.com/panva/node-openid-client/blob/main/docs/README.md#clientregistermetadata-other

  const body = {
    client_name: 'Egendata Wallet',
    application_type: 'web',
    redirect_uris: [new URL('/api/auth/callback/solid', process.env.NEXTAUTH_URL).toString()],
    subject_type: 'public',
    token_endpoint_auth_method: 'client_secret_basic',
    id_token_signed_response_alg: 'ES256',
    grant_types: ['authorization_code', 'refresh_token'],
    post_logout_redirect_uris: [new URL('/', process.env.NEXTAUTH_URL).toString()],
  };

  const response = await fetch(new URL('.oidc/reg', process.env.IDP_BASE_URL), {
    headers: { 'Content-Type': 'application/json' },
    method: 'POST',
    body: JSON.stringify(body),
  });

  const json = await response.json();

  if (!response.ok) {
    throw Error('Failed to register client credentials with IDP.');
  }

  process.stdout.write(`export SOLID_CLIENT_ID=${json.client_id}\nexport SOLID_CLIENT_SECRET=${json.client_secret}\n`);
};

registerCredentials();
