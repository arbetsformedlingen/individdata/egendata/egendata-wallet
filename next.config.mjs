import { readFileSync } from 'fs';
import { execSync } from 'child_process';

!process.env.SKIP_ENV_VALIDATION && (await import('./src/env.mjs'));

/** @type {import('next').NextConfig} */
const nextConfig = {
  // typescript: {
  //   tsconfigPath: 'tsconfig.build.json',
  // },
  reactStrictMode: true,
  output: 'standalone',
  async headers() {
    return [
      {
        source: '/:path*',
        headers: [
          {
            key: 'X-Frame-Options',
            value: 'DENY',
          },
        ],
      },
    ];
  },
  env: {
    NEXT_PUBLIC_COMMIT: (() => {
      try {
        return readFileSync('/app/git-commit').toString();
      } catch (error) {
        return 'unknown';
      }
    })().slice(0, 7),
  },
};

export default nextConfig;
