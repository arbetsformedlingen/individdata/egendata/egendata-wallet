// eslint-disable-next-line tsdoc/syntax
/** @type {import("prettier").Config} */
module.exports = {
  printWidth: 120,
  tabWidth: 2,
  useTabs: false,
  semi: true,
  singleQuote: true,
  trailingComma: 'all',
  bracketSpacing: true,
  jsxBracketSameLine: false,
  plugins: [require.resolve('prettier-plugin-tailwindcss')],
  pluginSearchDirs: false,
  tailwindConfig: './tailwind.config.cjs',
};
