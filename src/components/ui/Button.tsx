import React from 'react';

type Props = {
  children?: React.ReactNode;
  variant: 'primary' | 'secondary' | 'outlined';
  disabled?: Boolean;
  text?: string;
  endIcon?: React.ReactNode;
  onClick: () => void;
};

// const Button = (style: 'primary' | 'secondary' = 'primary') => {
const Button = ({ children, text, variant, disabled, onClick, endIcon }: Props) => {
  return (
    <button
      className={`flex rounded-full px-4 py-2 disabled:bg-zinc-300 dark:disabled:bg-zinc-600 ${
        variant === 'outlined'
          ? 'bg-zinc-100 text-green-600 outline outline-2 outline-green-600 hover:bg-white disabled:text-zinc-600 disabled:outline-zinc-600 dark:bg-zinc-900 dark:hover:bg-black'
          : 'bg-green-600 text-zinc-900 hover:text-black'
      }`}
      onClick={onClick}
      {...(disabled === undefined || disabled === false ? {} : { disabled: true })}
    >
      {children ? children : text} {endIcon}
    </button>
  );
};

export default Button;
