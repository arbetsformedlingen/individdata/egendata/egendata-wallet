import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import Checkbox from './Checkbox';

export default {
  title: 'Checkbox',
  component: Checkbox,
  argTypes: {},
} as Meta<typeof Checkbox>;

const Template: StoryFn<typeof Checkbox> = (args) => <Checkbox {...args} />;

export const Standard = Template.bind({});
Standard.args = {};
