import React, { ChangeEvent } from 'react';

type Props = {
  onChange: (evt: ChangeEvent<HTMLInputElement>) => void;
  name: string;
  text: string;
};

const CheckBox = ({ onChange, name, text }: Props) => {
  return (
    <div className="flex select-none gap-2">
      <input id={name} type="checkbox" name={name} onChange={onChange}></input>
      <label className="" htmlFor={name}>
        {text}
      </label>
    </div>
  );
};

export default CheckBox;
