import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import ConsentDialog from './ConsentDialog';
import { RequestState } from '@app/types';

export default {
  title: 'ConsentDialog',
  component: ConsentDialog,
  argTypes: {},
} as Meta<typeof ConsentDialog>;

const Template: StoryFn<typeof ConsentDialog> = (args) => <ConsentDialog {...args} />;

export const Preview = Template.bind({});
Preview.args = {
  request: {
    uuid: '3a4601e2-7028-4f37-929f-4c60df075733',
    url: new URL('https://example.com/request'),
    state: RequestState.RECEIVED,
    type: 'someType',
    purpose: 'some purpose',
    created: new Date(),
    provider: {
      name: 'Some Name',
      storage: 'https://example.com/provider/storage',
      documentTitle: { default: 'Title', en: 'Title', sv: 'Titel' },
    },
    providerWebId: new URL('https://example.com/provider'),
    requestor: {
      name: 'Some Name',
      storage: 'https://example.com/requestor/storage',
      consentChecks: [
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        'Proin eu massa massa.',
        'Phasellus dictum orci purus, lobortis rhoncus sem mattis in.',
      ],
    },
    requestorWebId: new URL('https://example.com/requestor'),
    returnUrl: new URL('https://example.com/some/path'),
    documentType: 'Some type',
    unread: true,
  },
  credential: {
    id: 'x',
    type: 'someCredentialType',
    subject: '19930223-1293',
    isRegisted: true,
    registrationDate: '2023-04-02',
  },
  state: 'preview',
  open: true,
};

export const Consent = Template.bind({});
Consent.args = {
  ...Preview.args,
  state: 'consent',
};

export const Shared = Template.bind({});
Shared.args = {
  ...Preview.args,
  state: 'shared',
};
