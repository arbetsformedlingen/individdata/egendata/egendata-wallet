import React, { useState } from 'react';
import Modal, { ModalActions, ModalContent, ModalTitle } from './Modal';

import { ExtendedRequestState, RequestWithDetails } from '@app/types';
import { OkIcon } from '../../icons/dialog';
import { FormattedMessage, useIntl } from 'react-intl';
import { useRouter } from 'next/router';

import Button from './Button';
import Checkbox from './Checkbox';
import { stringFromLocalizedString, useLanguage } from '@app/lib/i18n';

type ConsentDialogProps = {
  request: RequestWithDetails;
  credential: { [key: string]: any };
  state: ExtendedRequestState;
  open: boolean;
  onClose: () => void;
  onContinue: () => void;
  onConsent: () => void;
};

const ConsentDialog = ({ request, credential, state, open, onClose, onContinue, onConsent }: ConsentDialogProps) => {
  const router = useRouter();
  const intl = useIntl();
  const { language } = useLanguage();

  const [buttonDisabled, setButtonDisabled] = useState(true);

  function handleCheckboxChange(evt: React.ChangeEvent<HTMLInputElement>) {
    const boxes: HTMLInputElement[] = Array.from(document.querySelectorAll('input[type=checkbox]'));
    setButtonDisabled(!boxes.reduce((prev, checkbox): boolean => prev && checkbox.checked, true));
  }

  const documentTitle = stringFromLocalizedString(language, request.provider.documentTitle);
  const consentText = stringFromLocalizedString(language, request.requestor.consentText, 'Loading...');

  if (state === 'preview') {
    return (
      <Modal open={open} onClose={onClose}>
        <ModalTitle>{documentTitle}</ModalTitle>
        <ModalContent>
          <>
            <dl className="flex flex-wrap py-4">
              {Object.entries(credential).map(([key, value]) => (
                <>
                  <dt
                    className="w-1/3 flex-auto pr-2 text-right italic text-zinc-700 dark:text-zinc-300"
                    key={`${key}-dt`}
                  >
                    {key}
                  </dt>
                  <dd className="w-2/3 flex-auto" key={`${key}-dd`}>
                    {value.toString()}
                  </dd>
                </>
              ))}
            </dl>
            <hr />
            <div className="py-4">{request && request.purpose}</div>
          </>
        </ModalContent>
        <ModalActions>
          <Button onClick={onContinue} text="Continue" variant={'primary'}></Button>
        </ModalActions>
      </Modal>
    );
  }
  if (state === 'consent') {
    return (
      <Modal open={open} onClose={onClose}>
        <ModalTitle>
          <FormattedMessage
            defaultMessage="Consent document transfer"
            id="bKaiaS"
            description="Consent dialog title."
          />
        </ModalTitle>
        <ModalContent>
          {consentText}

          <form className="py-4">
            {request.requestor.consentChecks?.map((text: string, index: number) => {
              return (
                <Checkbox onChange={handleCheckboxChange} key={`box-${index}`} name={`box-${index}`} text={text} />
              );
            })}
          </form>
        </ModalContent>
        <ModalActions>
          <Button onClick={onConsent} disabled={buttonDisabled} text="Consent" variant="primary">
            <FormattedMessage
              defaultMessage="Consent to share"
              id="q0lp0j"
              description="Button for consenting to share data."
            />
          </Button>
        </ModalActions>
      </Modal>
    );
  }

  if (state === 'sharing') {
    return (
      <Modal open={open} onClose={onClose}>
        <ModalTitle>
          <div className="flex justify-center">
            <OkIcon className="h-6 w-6" />
          </div>
        </ModalTitle>
        <ModalContent>
          <p className="py-4">
            <FormattedMessage
              id="vLQYZV"
              defaultMessage="Your {documentType} is now being shared with {requestor}."
              description="Completion of sharing flow."
              values={{
                documentType: documentTitle,
                requestor: request.requestor.name,
              }}
            />
          </p>
          <hr />
          <p className="py-4">
            <FormattedMessage
              id="JOxSJR"
              defaultMessage="You can always revoke your consent under Consents."
              description="Completion of sharing flow, subtitle."
            />
          </p>
        </ModalContent>
        <ModalActions>
          <Button
            variant="primary"
            onClick={() => router.push(request.returnUrl)}
            text={intl.formatMessage({
              defaultMessage: 'Go back to requesting service',
              id: 'tMR/UK',
              description: 'Completion of sharing flow.',
            })}
          />
        </ModalActions>
      </Modal>
    );
  }

  return <></>;
};

export default ConsentDialog;
