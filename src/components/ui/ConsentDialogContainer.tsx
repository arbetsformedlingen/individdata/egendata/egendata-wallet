import ConsentDialog from './ConsentDialog';
import { ExtendedRequestState } from '@app/types';

import { api } from '@app/utils/api';

type Props = {
  requestId: string;
  state: ExtendedRequestState;
  open: boolean;
  onClose: () => void;
  onContinue: () => void;
  onConsent: () => void;
};

const ConsentDialogContainer = (props: Props) => {
  const { requestId } = props;

  const request = api.request.getRequestDetails.useQuery({ id: requestId });
  const credential = api.request.getCredential.useQuery({ id: requestId });

  if (request.data && credential.data) {
    return <ConsentDialog request={request.data} credential={credential.data} {...props} />;
  }

  return <></>;
};

export default ConsentDialogContainer;
