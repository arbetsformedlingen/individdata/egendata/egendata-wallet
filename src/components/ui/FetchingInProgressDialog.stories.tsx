import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import FetchingInProgressDialog from './FetchingInProgressDialog';

export default {
  title: 'FetchingInProgressDialog',
  component: FetchingInProgressDialog,
  argTypes: {},
} as Meta<typeof FetchingInProgressDialog>;

const Template: StoryFn<typeof FetchingInProgressDialog> = (args) => <FetchingInProgressDialog {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  open: true,
  state: 'fetching',
};

export const Available = Template.bind({});
Available.args = {
  ...Standard.args,
  state: 'available',
};

export const Timeout = Template.bind({});
Timeout.args = {
  ...Standard.args,
  state: 'timeout',
};

export const Missing = Template.bind({});
Missing.args = {
  ...Standard.args,
  state: 'missing',
};
