import { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { ExtendedRequestState } from '../../types';
import { CloseIcon, EmailIcon, ErrorIcon, OkIcon } from '../../icons/dialog';
import Button from './Button';
import ProgressBar from './ProgressBar';
import { Dialog } from '@headlessui/react';

type FetchingInProgressDialogProps = {
  open: boolean;
  onClose: () => void;
  state: ExtendedRequestState;
};

const FetchingInProgressDialog = ({ open, onClose, state }: FetchingInProgressDialogProps) => {
  const [email, setEmail] = useState('');

  function handleAddEmail() {
    console.log(`Adding email: ${email}`);
  }

  return (
    <Dialog open={open} onClose={onClose} className="relative z-50">
      {/* Backdrop */}
      <div className="fixed inset-0 flex bg-black/20 backdrop-blur-sm"></div>

      {/* Centering container */}
      <div className="fixed inset-0 flex items-center justify-center p-4">
        <Dialog.Panel className="w-full max-w-sm rounded-lg border border-zinc-300 bg-zinc-50 p-4 text-center shadow-lg dark:border-zinc-900 dark:bg-zinc-800">
          {state === 'fetching' && (
            <>
              <Dialog.Title className="py-4">
                <FormattedMessage defaultMessage="Fetching..." id="F50OAe" description="Fetching document modal." />
              </Dialog.Title>
              <div className="w-full max-w-sm flex-none py-2">
                <ProgressBar />
              </div>
            </>
          )}
          {state === 'available' && (
            <>
              <Dialog.Title className="flex justify-center py-4">
                <OkIcon className="h-8 w-8" />
              </Dialog.Title>
              <p className="text-center">
                <FormattedMessage
                  defaultMessage="Your {document} from {provider} has now been fetched. Click view data to review all the fetched data."
                  id="Tf52zN"
                  description="Fetching document modal, successful."
                  values={{
                    document: '_document_',
                    provider: '_provider_',
                  }}
                />
              </p>
            </>
          )}
          {state === 'timeout' && (
            <>
              <FormattedMessage
                defaultMessage="Your fetching is processing, add your email and get notified when it's ready."
                id="pHE0rC"
                description="Fetching document modal, took too long time."
              />
              <form className="flex flex-col items-center gap-4">
                <div>
                  <label htmlFor="email-input">
                    <FormattedMessage
                      defaultMessage="Email"
                      id="YCdA/w"
                      description="Fetching document modal, label for email input."
                    />
                    <EmailIcon className="mx-2 inline-block h-6 w-6" />
                  </label>

                  <input
                    id="email-input"
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                    className="dark:bg-zinc-800 "
                  />
                </div>
                <div>
                  <Button variant="outlined" onClick={handleAddEmail}>
                    <FormattedMessage
                      defaultMessage="Add email"
                      id="sN26ST"
                      description="Fetching document modal, button for adding email."
                    />
                  </Button>
                </div>
              </form>
            </>
          )}
          {state === 'missing' && (
            <>
              {' '}
              <Dialog.Title className="flex justify-center py-4">
                <ErrorIcon className="h-8 w-8" />
              </Dialog.Title>{' '}
              <p>
                <FormattedMessage
                  defaultMessage="We can't find your {document}. Contact {provider}."
                  id="rlwh6h"
                  description="Fetching document modal, missing prerequisites."
                  values={{
                    document: '_document_',
                    provider: '_provider_',
                  }}
                />
              </p>
            </>
          )}
        </Dialog.Panel>
      </div>
    </Dialog>
  );

  // return (
  //   <ControlFlowBaseDialog canBeClosed>
  //     <DialogTitle sx={{ m: 0, p: 2 }}>
  //       {['timeout', 'error'].includes(state) && (
  //         <IconButton
  //           aria-label="close"
  //           onClick={handleClose}
  //           sx={{
  //             position: 'absolute',
  //             right: 8,
  //             top: 8,
  //             color: (theme) => theme.palette.grey[500],
  //           }}
  //         >
  //           <CloseIcon />
  //         </IconButton>
  //       )}
  //     </DialogTitle>
  //     <DialogContent>
  //       {state === 'fetching' && !error && (
  //         <Box marginY={8}>
  //           <Typography variant="h5" align="center" marginBottom={2}>
  //             <FormattedMessage defaultMessage="Fetching..." id="F50OAe" description="Fetching document modal." />
  //           </Typography>
  //           <LinearProgress
  //             sx={{
  //               height: '10px',
  //               backgroundColor: '#434343',
  //               borderRadius: '5px',
  //               '& *': {
  //                 borderRadius: '5px',
  //               },
  //             }}
  //           />
  //         </Box>
  //       )}
  //       {state === 'available' && !error && (
  //         <Box>
  //           <Typography variant="h5" align="center">
  //             <OkIcon />
  //             <FormattedMessage
  //               defaultMessage="Your {document} from {provider} has now been fetched. Click view data to review all the fetched data."
  //               id="Tf52zN"
  //               description="Fetching document modal, successful."
  //               values={{
  //                 document: '_document_',
  //                 provider: '_provider_',
  //               }}
  //             />
  //           </Typography>
  //         </Box>
  //       )}
  //       {state === 'timeout' && !error && (
  //         <Grid container direction="column" alignItems="center">
  //           <Grid item m={4}>
  //             <Typography variant="h5" align="center">
  //               <FormattedMessage
  //                 defaultMessage="Your fetching is processing, add your email and get notified when it's ready."
  //                 id="pHE0rC"
  //                 description="Fetching document modal, took too long time."
  //               />
  //             </Typography>
  //           </Grid>
  //           <Grid item>
  //             <FormControl variant="filled">
  //               <InputLabel htmlFor="email-input">
  //                 <FormattedMessage
  //                   defaultMessage="Email"
  //                   id="YCdA/w"
  //                   description="Fetching document modal, label for email input."
  //                 />
  //               </InputLabel>
  //               <FilledInput
  //                 id="email-input"
  //                 value={email}
  //                 onChange={(event) => setEmail(event.target.value)}
  //                 startAdornment={
  //                   <InputAdornment position="start">
  //                     <EmailIcon />
  //                   </InputAdornment>
  //                 }
  //               />
  //             </FormControl>
  //           </Grid>
  //           <Grid item marginTop={2} marginBottom={4}>
  //             <FormControl>
  //               <Button variant="contained" color="success" size="large" onClick={handleAddEmail}>
  //                 <FormattedMessage
  //                   defaultMessage="Add email"
  //                   id="sN26ST"
  //                   description="Fetching document modal, button for adding email."
  //                 />
  //               </Button>
  //             </FormControl>
  //           </Grid>
  //         </Grid>
  //       )}
  //       {state === 'missing' && (
  //         <Box>
  //           <Typography variant="h5" align="center">
  //             <FormattedMessage
  //               defaultMessage="We can't find your {document}. Contact {provider}."
  //               id="rlwh6h"
  //               description="Fetching document modal, missing prerequisites."
  //               values={{
  //                 document: '_document_',
  //                 provider: '_provider_',
  //               }}
  //             />
  //           </Typography>
  //         </Box>
  //       )}
  //       {error && (
  //         <Grid container direction="column" alignItems="center">
  //           <Grid item m={4} textAlign="center">
  //             <ErrorIcon />
  //           </Grid>
  //           <Grid item>
  //             <Typography variant="h5" align="center">
  //               <FormattedMessage
  //                 defaultMessage="Something went wrong with your transfer. Try again later."
  //                 id="pqiVfp"
  //                 description="Fetching document modal, error."
  //               />
  //             </Typography>
  //           </Grid>
  //         </Grid>
  //       )}
  //     </DialogContent>
  //   </ControlFlowBaseDialog>
  // );
};
export default FetchingInProgressDialog;
