import FetchingInProgressDialog from './FetchingInProgressDialog';
import { ExtendedRequestState } from '@app/types';

import { api } from '@app/utils/api';
import { setDatetime } from '@inrupt/solid-client';
import { useEffect } from 'react';

type Props = {
  open: boolean;
  state: ExtendedRequestState;
  onClose: () => void;
};

const FetchingInProgressDialogContainer = (props: Props) => {
  useEffect(() => {
    console.log('STATE IN DIALOG', props.state, props.open);
  }, [props.state, props.open]);

  return <FetchingInProgressDialog {...props} />;
};

export default FetchingInProgressDialogContainer;
