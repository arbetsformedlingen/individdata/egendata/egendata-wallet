import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import Footer from './Footer';

export default {
  title: 'Footer',
  component: Footer,
  argTypes: {
    commit: { type: 'string' },
  },
} as Meta<typeof Footer>;

const Template: StoryFn<typeof Footer> = (args) => <Footer {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  commit: '11094fd',
};

export const WithChildren: StoryFn<typeof Footer> = (args) => (
  <Footer {...args}>
    <p>Hello from footer</p>
  </Footer>
);
WithChildren.args = {
  commit: '11094fd',
};
