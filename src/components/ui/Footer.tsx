import React from 'react';

type Props = {
  children: React.ReactNode;
  commit: string;
};

export default function Footer(props: Props) {
  const { children, commit } = props;
  return (
    <footer className="text-center">
      <div>{children}</div>
      <div className="y-6 py-2 text-sm text-zinc-200 dark:text-zinc-700">commit: {commit}</div>
    </footer>
  );
}
