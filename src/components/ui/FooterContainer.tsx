import Footer from './Footer';
import { env } from '@app/env.mjs';

type Props = {
  children?: React.ReactNode;
};

const FooterContainer = (props: Props) => {
  const { children } = props;

  const commit = env.NEXT_PUBLIC_COMMIT ?? 'unknown';

  return <Footer commit={commit}>{children}</Footer>;
};

export default FooterContainer;
