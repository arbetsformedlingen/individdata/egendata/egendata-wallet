import { Meta, StoryFn } from '@storybook/react';

import React from 'react';
import LanguageSwitch from './LanguageSwitch';

export default {
  title: 'LanguageSwitch',
  component: LanguageSwitch,
  argTypes: {
    language: {
      options: ['sv', 'en'],
      control: { type: 'select' },
      defaultValue: 'sv',
    },
  },
} as Meta<typeof LanguageSwitch>;

const Template: StoryFn<typeof LanguageSwitch> = (args) => <LanguageSwitch {...args} />;

export const Swedish = Template.bind({});
Swedish.args = {
  language: 'sv',
};

export const English = Template.bind({});
English.args = {
  language: 'en',
};
