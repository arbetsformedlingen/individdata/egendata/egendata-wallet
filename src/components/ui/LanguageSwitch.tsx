import { Listbox } from '@headlessui/react';
import { AVAILABLE_LANGUAGES } from '@app/locales';
import { GlobeIcon } from '@app/icons/icons';

export const LanguageSwitch = ({ language, setLanguage }: { language: string; setLanguage: (arg: string) => void }) => {
  return (
    <div className="w-26 mx-1">
      <Listbox value={language} onChange={setLanguage}>
        <div className="relative">
          <Listbox.Button className="relative flex w-full cursor-default gap-1 rounded-full bg-zinc-50 p-2 text-left hover:bg-zinc-200 data-[headlessui-state=open]:rounded-b-none data-[headlessui-state=open]:rounded-t-[20px] data-[headlessui-state=open]:bg-zinc-200 dark:bg-zinc-800 dark:hover:bg-zinc-900 data-[headlessui-state=open]:dark:bg-zinc-900">
            <GlobeIcon className="h-6 w-6 flex-shrink-0" />
            <span className="block truncate">{AVAILABLE_LANGUAGES[language]}</span>
          </Listbox.Button>
          <Listbox.Options className="absolute w-full rounded-b-[20px] bg-zinc-200 pl-4 text-zinc-900 dark:bg-zinc-900 dark:text-zinc-200">
            {Object.keys(AVAILABLE_LANGUAGES).map((lang) => (
              <Listbox.Option key={lang} value={lang} className="relative cursor-default select-none py-2">
                {AVAILABLE_LANGUAGES[lang]}
              </Listbox.Option>
            ))}
          </Listbox.Options>
        </div>
      </Listbox>
    </div>
  );
};

export default LanguageSwitch;
