import { useContext } from 'react';
import { LanguageContext, persistLanguage, useLanguage } from '@app/lib/i18n';
import LanguageSwitch from './LanguageSwitch';

const LanguageSwitchContainer = () => {
  const { language, setLanguage } = useLanguage();

  return <LanguageSwitch language={language} setLanguage={setLanguage} />;
};

export default LanguageSwitchContainer;
