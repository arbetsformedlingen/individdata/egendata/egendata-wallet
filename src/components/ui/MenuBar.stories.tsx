import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import MenuBar from './MenuBar';

export default {
  title: 'MenuBar',
  component: MenuBar,
  argTypes: {
    user: {
      control: 'object',
    },
    currentPath: {
      options: ['/home', '/consents', '/mydata', false],
      control: { type: 'select' },
    },
    disabledNav: { control: 'boolean' },

    onNav: { action: 'clicked' },
    onSignOut: { action: 'clicked' },
  },
} as Meta<typeof MenuBar>;

const Template: StoryFn<typeof MenuBar> = (args) => <MenuBar {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  user: { name: 'Kalle Anka', firstName: 'Kalle', lastName: 'Anka' },
  currentPath: '/home',
  onSignOut: () => null,
  disabledNav: false,
};

export const SignedOut = Template.bind({});
SignedOut.args = {
  ...Standard.args,
  user: undefined,
};

export const DisabledNav = Template.bind({});
DisabledNav.args = {
  ...Standard.args,
  disabledNav: true,
};

export const Mobile = Template.bind({});
Mobile.parameters = {
  ...Standard.parameters,
  viewport: {
    defaultViewport: 'mobile2',
  },
};
Mobile.args = {
  ...Standard.args,
};
