import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { ExitIcon, MenuIcon, ProfileIcon } from '../../icons/icons';
import Link from 'next/link';
import ThemeSwitchContainer from './ThemeSwitchContainer';
import LanguageSwitchContainer from './LanguageSwitchContainer';
import { Popover, Transition } from '@headlessui/react';
import { useLanguage } from '@app/lib/i18n';
import { useRouter } from 'next/router';
import { switchColorMode } from '@app/lib/darkMode';

type Props = {
  user: { name: string; firstName: string | undefined; lastName: string | undefined } | undefined;
  onSignOut: () => void;
} & (
  | { disabledNav: true }
  | {
      currentPath: string | false;
      disabledNav: false;
      onNav: (path: string) => void;
    }
);

export default function MenuBar(props: Props) {
  const router = useRouter();
  const { language, setLanguage } = useLanguage();

  const { disabledNav, onSignOut } = props;
  const { user, currentPath, onNav } = disabledNav
    ? { user: props.user ?? undefined, currentPath: false, onNav: () => null }
    : props;

  const [drawerOpen, setDrawerOpen] = useState(false);

  const pages = [
    {
      route: 'inbox',
      path: '/home',
      status: false, // Change to dynamic value.
      message: <FormattedMessage id="edtIll" defaultMessage="Inbox" description="Menubar inbox tab." />,
    },
    {
      state: 'consents',
      path: '/consents',
      status: false,
      message: <FormattedMessage id="Yy59up" defaultMessage="Contents" description="Menubar consents tab." />,
    },
    {
      state: 'mydata',
      path: '/mydata',
      status: false,
      message: <FormattedMessage id="1aMDvI" defaultMessage="My data" description="Menubar my data tab." />,
    },
  ];

  return (
    <nav className="flex flex-wrap items-center justify-between bg-zinc-50 p-4 dark:bg-zinc-800 lg:p-6">
      <div className="w-0 flex-1 items-center text-zinc-800 dark:text-white">
        <span className="text-xl font-semibold tracking-tight">Egendata</span>
      </div>
      {!disabledNav && (
        <div className="flex w-1/2 items-center rounded-full bg-zinc-200 p-2 dark:bg-zinc-900 max-lg:hidden">
          {pages.map((page) => (
            <div className="flex-grow text-center" key={page.path}>
              <Link
                href={page.path}
                className="text-zinc-800 hover:text-black dark:text-zinc-200 dark:hover:text-white"
              >
                {page.message}
              </Link>
            </div>
          ))}
        </div>
      )}
      <div className="flex w-0 flex-1 justify-end max-lg:hidden">
        <ThemeSwitchContainer />
        <LanguageSwitchContainer />
        {user && router.pathname !== '/request' && (
          <div className="mx-1 w-36">
            <Popover>
              <div className="relative">
                <Popover.Button className="flex w-full gap-1 rounded-full bg-zinc-50 p-2 text-left text-zinc-800 hover:bg-zinc-200 hover:text-black dark:bg-zinc-800 dark:text-zinc-300 dark:hover:bg-zinc-900 dark:hover:text-white">
                  <ProfileIcon className="h-6 w-6 flex-shrink-0" />
                  <span className="block truncate">{user.name}</span>
                </Popover.Button>

                <Popover.Panel className="absolute right-0 z-10 min-w-[150%] translate-y-2 rounded-xl bg-zinc-200 px-4 dark:bg-zinc-900">
                  <ul className="grid grid-cols-1 gap-2 py-2 text-xl dark:divide-zinc-600">
                    <li>
                      <Link href="#">
                        <div className="h-full w-full">Account details</div>
                      </Link>
                    </li>{' '}
                    <li>
                      <Link href="#">
                        <div className="h-full w-full">Settings</div>
                      </Link>
                    </li>{' '}
                    <li>
                      <button
                        className="h-full w-full text-left hover:text-black dark:hover:text-white"
                        onClick={onSignOut}
                      >
                        Log out
                      </button>
                    </li>
                  </ul>
                </Popover.Panel>
              </div>
            </Popover>
          </div>
        )}
      </div>

      {/* ================ Mobile menu ================ */}
      <div className="lg:hidden">
        <button
          className="rounded-full p-2 text-zinc-800 hover:text-black dark:bg-zinc-800 dark:text-zinc-300  dark:hover:bg-zinc-900 dark:hover:text-white "
          onClick={() => setDrawerOpen(true)}
        >
          <MenuIcon className="h-6 w-6" />
        </button>
      </div>

      <Transition
        show={drawerOpen}
        enter="transition ease-in-out duration-75 transform"
        enterFrom="-translate-x-full"
        enterTo="translate-x-0"
        leave="transition ease-in-out duration-75 transform"
        leaveFrom="translate-x-0"
        leaveTo="-translate-x-full"
        className="fixed left-0 top-0 min-h-screen w-full -translate-x-full bg-zinc-50 p-4 text-zinc-800 dark:bg-zinc-800 dark:text-white"
      >
        <div className="flex flex-col p-4">
          <div className="flex w-full flex-row items-center justify-between ">
            <span className="text-xl font-semibold tracking-tight">Egendata</span>
            <button
              className="rounded-full p-2 text-zinc-800 hover:text-black dark:bg-zinc-800 dark:text-zinc-300  dark:hover:bg-zinc-900  dark:hover:text-white"
              onClick={() => setDrawerOpen(false)}
            >
              <ExitIcon className="h-6 w-6" />
            </button>
          </div>
        </div>
        <div className="w-full flex-1 px-4 py-4 text-zinc-800 dark:text-zinc-300">
          <ul className="grid grid-cols-1 divide-y divide-zinc-200 text-xl dark:divide-zinc-600">
            {!disabledNav && (
              <>
                <li>
                  <Link href="/home">
                    <div className="h-full w-full py-4">Inbox</div>
                  </Link>
                </li>
                <li>
                  <Link href="/contents">
                    <div className="h-full w-full py-4">Contents</div>
                  </Link>
                </li>
                <li>
                  <Link href="/mydata">
                    <div className="h-full w-full py-4">MyData</div>
                  </Link>
                </li>
              </>
            )}
            <li>
              <button className="py-4 hover:text-black dark:hover:text-white" onClick={() => switchColorMode()}>
                Switch color mode
              </button>
            </li>
            <li>
              Language:{' '}
              <button
                className="px-2 py-4 hover:text-black dark:hover:text-white"
                disabled={language === 'en'}
                onClick={() => setLanguage('en')}
                lang="en"
              >
                EN
              </button>
              <span className="px-1 text-zinc-200 dark:text-zinc-600">|</span>
              <button
                className="px-2 py-4 hover:text-black dark:hover:text-white"
                disabled={language === 'sv'}
                onClick={() => setLanguage('sv')}
                lang="sv"
              >
                SV
              </button>
            </li>
            {user && router.pathname !== '/request' && (
              <>
                <li>
                  <Link href="#">
                    <div className="h-full w-full py-4">Account details</div>
                  </Link>
                </li>
                <li>
                  <button
                    className="h-full w-full py-4 text-left hover:text-black dark:hover:text-white"
                    onClick={onSignOut}
                  >
                    Log out {user.name}
                  </button>
                </li>
              </>
            )}
          </ul>
        </div>
      </Transition>
    </nav>
  );
}
