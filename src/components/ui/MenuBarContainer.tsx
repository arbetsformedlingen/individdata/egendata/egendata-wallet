import React, { useState } from 'react';
import { useSession } from 'next-auth/react';
import signOut from '@app/lib/signOut';
import { useRouter } from 'next/router';

import { api } from '@app/utils/api';
import MenuBar from './MenuBar';

type Props = {
  disabledNav?: boolean;
};

const MenuBarContainer = ({ disabledNav = false }: Props) => {
  const { data: session, status } = useSession();
  const user = api.profile.whoami.useQuery(undefined, { enabled: status === 'authenticated' });

  const router = useRouter();

  const handleNav = (path: string) => {
    router.push(path);
  };

  const handleSignOut = async () => {
    signOut(session!.idToken);
  };

  return (
    <MenuBar
      user={user.data}
      currentPath={router.pathname}
      onNav={handleNav}
      onSignOut={handleSignOut}
      disabledNav={disabledNav}
    ></MenuBar>
  );
};

export default MenuBarContainer;
