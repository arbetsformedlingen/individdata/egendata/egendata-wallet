import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import { Modal, ModalActions, ModalContent, ModalTitle } from './Modal';
import Button from './Button';

export default {
  title: 'Modal',
  component: Modal,
  argTypes: {},
} as Meta<typeof Modal>;

export const Standard: StoryFn<typeof Modal> = (args) => (
  <Modal {...args}>
    <ModalTitle>Hello</ModalTitle>
    <ModalContent>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a aliquam enim.</p>
      <p>
        Nunc bibendum eros eu risus convallis tristique. Integer eu eros eget odio ultricies pulvinar vitae ut eros.
        Suspendisse ut accumsan ex. Nunc sit amet erat sodales, tincidunt quam nec, sodales lectus. Curabitur vel libero
        vel purus ultrices posuere.
      </p>
    </ModalContent>
    <ModalActions>
      <Button
        text="Click me"
        variant={'primary'}
        onClick={function (): void {
          throw new Error('Function not implemented.');
        }}
      />
    </ModalActions>
  </Modal>
);

Standard.args = { open: true };

export const OnlyContent: StoryFn<typeof Modal> = (args) => (
  <Modal {...args}>
    <ModalContent>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a aliquam enim. Nunc bibendum eros eu risus
        convallis tristique. Integer eu eros eget odio ultricies pulvinar vitae ut eros. Suspendisse ut accumsan ex.
        Nunc sit amet erat sodales, tincidunt quam nec, sodales lectus. Curabitur vel libero vel purus ultrices posuere.
      </p>
    </ModalContent>
  </Modal>
);

OnlyContent.args = { open: true };

export const NoTitle: StoryFn<typeof Modal> = (args) => (
  <Modal {...args}>
    <ModalContent>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean a aliquam enim. Nunc bibendum eros eu risus
        convallis tristique. Integer eu eros eget odio ultricies pulvinar vitae ut eros. Suspendisse ut accumsan ex.
        Nunc sit amet erat sodales, tincidunt quam nec, sodales lectus. Curabitur vel libero vel purus ultrices posuere.
      </p>
    </ModalContent>
    <ModalActions>
      <Button
        text="Click me"
        variant={'primary'}
        onClick={function (): void {
          throw new Error('Function not implemented.');
        }}
      />
    </ModalActions>
  </Modal>
);

NoTitle.args = { open: true };
