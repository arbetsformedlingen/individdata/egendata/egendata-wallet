import React, { useEffect } from 'react';
import { createPortal } from 'react-dom';

type ModalProps = {
  open: Boolean;
  onClose: () => void;
  children: React.ReactNode;
};

export const ModalTitle = (props: { children: React.ReactNode }) => {
  return <h2 className="flex-1 text-lg font-medium text-gray-800 dark:text-white">{props.children}</h2>;
};

export const ModalContent = (props: { children: React.ReactNode }) => {
  return <div>{props.children}</div>;
};

export const ModalActions = (props: { children: React.ReactNode }) => {
  return <div className="flex justify-center pt-4">{props.children}</div>;
};

// https://biagio.dev/posts/tailwindcss-react-modal

export const Modal = (props: ModalProps) => {
  const { open, onClose, children } = props;

  const escHandler = ({ key }: { key: string }) => {
    if (key === 'Escape') {
      onClose();
    }
  };

  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.addEventListener('keydown', escHandler);
    }

    return () => {
      if (typeof window !== 'undefined') {
        window.removeEventListener('keydown', escHandler);
      }
    };
  });

  if (typeof document !== 'undefined') {
    return createPortal(
      <>
        {open && (
          <div className="fixed inset-0 z-10 overflow-y-auto">
            <div className="fixed inset-0 h-full w-full bg-black/30 backdrop-blur-sm" onClick={onClose}></div>
            <div className="flex min-h-screen items-center px-4 py-8">
              <div className="relative mx-auto flex w-full max-w-2xl flex-col rounded-lg bg-white p-4 pt-8 shadow-lg dark:bg-zinc-900">
                <button
                  className="align-center absolute right-2 top-2 flex justify-end text-zinc-600 dark:text-zinc-300"
                  onClick={onClose}
                >
                  Close
                  <div className="h-6 w-6">
                    <svg
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="1.5"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                      aria-hidden="true"
                    >
                      <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12"></path>
                    </svg>
                  </div>
                </button>
                {children}
              </div>
            </div>
          </div>
        )}
      </>,
      document.body,
    );
  } else {
    return null;
  }
};

export default Modal;
