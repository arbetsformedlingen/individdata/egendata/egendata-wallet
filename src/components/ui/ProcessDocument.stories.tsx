import { RequestState } from '@app/types';
import { Meta, StoryFn } from '@storybook/react';

import React from 'react';
import ProcessDocument from './ProcessDocument';

export default {
  title: 'ProcessDocument',
  component: ProcessDocument,
  argTypes: {
    state: {
      options: [
        'received',
        'fetching',
        'available',
        'preview',
        'consent',
        'sharing',
        'shared',
        'timeout',
        'missing',
        'error',
      ],
      control: { type: 'select' },
    },
    request: {
      control: 'object',
    },
  },
} as Meta<typeof ProcessDocument>;

const Template: StoryFn<typeof ProcessDocument> = (args) => <ProcessDocument {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  state: 'received',
  request: {
    uuid: '3a4601e2-7028-4f37-929f-4c60df075733',
    url: new URL('https://example.com/request'),
    state: RequestState.RECEIVED,
    type: 'someType',
    purpose: 'some purpose',
    created: new Date(),
    provider: {
      name: 'Some Name',
      storage: 'https://example.com/provider/storage',
      documentTitle: { default: 'Title', en: 'Title', sv: 'Titel' },
    },
    providerWebId: new URL('https://example.com/provider'),
    requestor: {
      name: 'Some Name',
      storage: 'https://example.com/requestor/storage',
      consentChecks: [
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        'Proin eu massa massa.',
        'Phasellus dictum orci purus, lobortis rhoncus sem mattis in.',
      ],
    },
    requestorWebId: new URL('https://example.com/requestor'),
    returnUrl: new URL('https://example.com/some/path'),
    documentType: 'Some type',
    unread: true,
  },
  onGetClick: () => null,
  onShowConsentClick: () => null,
};
