import React, { useState } from 'react';
import { ArrowDown, ArrowUpRight, DocumentIcon } from '@app/icons/icons';
import { ExtendedRequestState, RequestWithDetails } from '@app/types';
import { FormattedMessage } from 'react-intl';
import Button from './Button';
import { Modal, ModalContent, ModalTitle } from './Modal';
import { stringFromLocalizedString, useLanguage } from '@app/lib/i18n';

type Props = {
  request: RequestWithDetails;
  state: ExtendedRequestState;
  onGetClick: () => void;
  onShowConsentClick: () => void;
};

const ProcessDocument = ({ request, state, onGetClick, onShowConsentClick }: Props) => {
  const { language } = useLanguage();
  const [showInfo, setShowInfo] = useState(false);

  const documentTitle = stringFromLocalizedString(language, request.provider.documentTitle);

  return (
    <div className="mx-auto flex w-full select-none flex-col gap-4 pt-4 text-center dark:bg-zinc-800 dark:text-white md:w-8/12">
      {state === 'received' && (
        <h5>
          1.{' '}
          <FormattedMessage
            id="gpDDd6"
            defaultMessage="Get your data"
            description="First step of processing request."
          />
        </h5>
      )}
      {state === 'fetching' && (
        <h5>
          <FormattedMessage
            id="wdTCcn"
            defaultMessage="Waiting for your data"
            description="First step of processing request."
          />
        </h5>
      )}
      {state === 'available' && (
        <h5>
          2.{' '}
          <FormattedMessage
            id="pum/RZ"
            defaultMessage="View and share your document"
            description="Second step of processing request."
          />
        </h5>
      )}

      {state === 'sharing' && (
        <>
          <FormattedMessage
            id="20LWpl"
            defaultMessage="Your shared document"
            description="Second step of processing request."
          />
        </>
      )}

      <div className="flex flex-row items-center justify-between rounded-lg bg-zinc-100 px-4 py-8 dark:bg-zinc-900">
        <div>
          <h4 className="text-left text-lg">
            <DocumentIcon className="relative -top-1 inline-block h-8 w-8" />
            {documentTitle}
          </h4>

          {['received', 'fetching'].includes(state) && (
            <FormattedMessage
              id="FG6mxq"
              defaultMessage="From: <bold>{provider}</bold>"
              description="Process document from provider."
              values={{
                provider: request.provider.name,
                bold: (msg) => <span className="text-[#65D36E]">{msg}</span>,
              }}
            />
          )}

          {['available', 'sharing'].includes(state) && (
            <FormattedMessage
              id="xoQ5iA"
              defaultMessage="Share with: <bold>{requestor}</bold>"
              description="Share document with requestor."
              values={{
                requestor: request.requestor.name,
                bold: (msg) => <span className="text-[#65D36E]">{msg}</span>,
              }}
            />
          )}
        </div>

        {state === 'received' && (
          <Button variant="outlined" onClick={onGetClick}>
            <FormattedMessage id="qrKUHl" defaultMessage="Get data" description="Process document get data button." />
            <ArrowDown className="relative left-1 top-1 h-6 w-6" />
          </Button>
        )}

        {state === 'fetching' && (
          <Button variant="outlined" onClick={() => setShowInfo(true)}>
            <FormattedMessage id="7TkBpL" defaultMessage="Waiting" description="Process document get data button." />
          </Button>
        )}

        {state === 'available' && (
          <Button variant="outlined" endIcon={<ArrowUpRight className=" h-6 w-6" />} onClick={onShowConsentClick}>
            <FormattedMessage
              id="s2YZRa"
              defaultMessage="View and share data"
              description="Process document view and share data button."
            />
          </Button>
        )}

        {state === 'shared' && (
          <Button
            variant="outlined"
            disabled
            endIcon={<ArrowUpRight />}
            onClick={() => console.log('Already sharing.')}
          >
            <FormattedMessage
              id="8quOpv"
              defaultMessage="Manage data"
              description="Process document manage data button."
            />
          </Button>
        )}
      </div>

      <Modal open={showInfo} onClose={() => setShowInfo(false)}>
        <ModalTitle>Title</ModalTitle>
        <ModalContent>
          <FormattedMessage
            id="Hzensj"
            defaultMessage="Your fetching is in progress but might take a while."
            description="Modal for waiting for fetching."
          />
        </ModalContent>
      </Modal>
    </div>
  );
};

export default ProcessDocument;
