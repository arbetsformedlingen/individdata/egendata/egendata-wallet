import React, { useState } from 'react';

import { api } from '@app/utils/api';
import ProcessDocument from './ProcessDocument';

type Props = {
  requestId: string;
  onGetClick: () => void;
  onShowConsentClick: () => void;
};

const ProcessDocumentContainer = ({ requestId, onGetClick, onShowConsentClick }: Props) => {
  const request = api.request.getRequestDetails.useQuery({ id: requestId });

  if (!request.data) {
    return <></>;
  }

  return (
    <ProcessDocument
      request={request.data}
      state={request.data?.state}
      onGetClick={onGetClick}
      onShowConsentClick={onShowConsentClick}
    />
  );
};

export default ProcessDocumentContainer;
