import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import ProgressBar from './ProgressBar';

export default {
  title: 'ProgressBar',
  component: ProgressBar,
} as Meta<typeof ProgressBar>;

const Template: StoryFn<typeof ProgressBar> = (args) => <ProgressBar />;

export const Standard = Template.bind({});
Standard.args = {};
