const ProgressBar = () => {
  return (
    <div className="flex h-4 w-full overflow-hidden rounded-full border-2 border-zinc-300 bg-zinc-50 dark:border-zinc-900 dark:bg-zinc-800">
      <div className="h-full w-full flex-1 animate-[barberpole_10s_linear_infinite]  bg-[repeating-linear-gradient(-45deg,_transparent,_transparent_1rem,_#65D36E_1rem,_#65D36E_2rem)] [background-size:200%]"></div>
    </div>
  );
};

export default ProgressBar;
