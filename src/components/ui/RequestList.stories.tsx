import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import RequestList from './RequestList';

import { RequestState } from '@app/types';

export default {
  title: 'RequestList',
  component: RequestList,
  argTypes: {
    requests: {
      control: 'object',
    },
    hasNextPage: {},
    handleNextPage: { action: 'clicked' },
    handleSelectItem: { action: 'clicked' },
  },
} as Meta<typeof RequestList>;

const Template: StoryFn<typeof RequestList> = (args) => <RequestList {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  requests: [
    {
      uuid: 'a9a6d44c-b9ef-465a-855c-740d4224bd19',
      purpose: 'To process your data.',
      created: new Date(2023, 1, 7),
      requestor: { name: 'ACME Inc.', storage: 'https://example.com/some/path' },
      url: new URL('https://example.com/a9a6d44c-b9ef-465a-855c-740d4224bd19'),
      state: RequestState.RECEIVED,
      type: '',
      documentType: '',
      providerWebId: new URL('https://example.com/providerWebId'),
      requestorWebId: new URL('https://example.com/requestorWebId'),
      returnUrl: new URL('https://example.com/some/path'),
    },
    {
      uuid: '8aa1f644-ffc3-49d1-8132-d6c4341d4242',
      purpose: 'To process your data.',
      created: new Date(2023, 2, 28),
      unread: true,
      requestor: { name: 'Evil Corp.', storage: 'https://example.com/some/path' },
      url: new URL('https://example.com/8aa1f644-ffc3-49d1-8132-d6c4341d4242'),
      state: RequestState.FETCHING,
      type: '',
      documentType: '',
      providerWebId: new URL('https://example.com/providerWebId'),
      requestorWebId: new URL('https://example.com/requestorWebId'),
      returnUrl: new URL('https://example.com/some/path'),
    },
    {
      uuid: '3fc2e526-6f88-49fb-bd21-cf1aabd07b85',
      purpose: 'To process your data.',
      created: new Date(2023, 3, 1),
      requestor: { name: 'Weyland Consortium', storage: 'https://example.com/some/path' },
      url: new URL('https://example.com/3fc2e526-6f88-49fb-bd21-cf1aabd07b85'),
      state: RequestState.RECEIVED,
      type: '',
      documentType: '',
      providerWebId: new URL('https://example.com/providerWebId'),
      requestorWebId: new URL('https://example.com/requestorWebId'),
      returnUrl: new URL('https://example.com/some/path'),
    },
  ],
};

export const Empty = Template.bind({});
Empty.args = { requests: [] };
