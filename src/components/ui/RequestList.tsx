import React from 'react';
import { FormattedMessage } from 'react-intl';
import { OrganizationAgentInfo, RequestInfo } from '@app/types';
import RequestListItem from './RequestListItem';

type RequestListProps = {
  requests: (RequestInfo & { requestor: OrganizationAgentInfo })[];
  onNextPage: () => void;
  hasNextPage: boolean;
};

function RequestList({ requests, onNextPage, hasNextPage }: RequestListProps) {
  return (
    <ul className="mx-auto flex w-full flex-col gap-2 dark:bg-zinc-800 md:w-8/12">
      {requests && requests.map((request) => <RequestListItem key={request.uuid} {...request} />)}
      <div className="text-center">
        <button
          className="p-2 text-zinc-700 hover:text-black dark:text-zinc-300 dark:hover:text-white"
          disabled={!hasNextPage}
          onClick={() => onNextPage()}
        >
          <FormattedMessage
            defaultMessage="Show older requests"
            id="0P50ON"
            description="Home page listing of requests."
          />
        </button>
      </div>
    </ul>
  );
}

export default RequestList;
