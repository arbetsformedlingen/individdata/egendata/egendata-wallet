import React from 'react';
import { api } from '@app/utils/api';
import { OrganizationAgentInfo, RequestInfo } from '@app/types';
import RequestList from './RequestList';

function RequestListContainer() {
  const requests = api.request.getInfiniteRequests.useInfiniteQuery(
    { limit: 10 },
    {
      getNextPageParam: (lastPage) =>
        lastPage.nextCursor && lastPage.nextCursor < lastPage.total ? lastPage.nextCursor : undefined,
    },
  );

  const items = requests.data?.pages
    .flatMap((page) => page.items)
    .filter((request) => request !== undefined) as (RequestInfo & {
    requestor: OrganizationAgentInfo;
  })[];

  return (
    <RequestList
      requests={items}
      onNextPage={() => requests.fetchNextPage()}
      hasNextPage={requests.hasNextPage ?? false}
    />
  );
}

export default RequestListContainer;
