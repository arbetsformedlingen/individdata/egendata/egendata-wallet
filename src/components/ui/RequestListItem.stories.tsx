import { Meta, StoryFn } from '@storybook/react';

import React from 'react';
import RequestListItem from './RequestListItem';

export default {
  title: 'RequestListItem',
  component: RequestListItem,
  argTypes: {
    purpose: { type: 'string' },
    created: { control: 'date' },
    requestor: {
      control: 'object',
    },
    unread: { control: 'boolean' },
    providerWebId: { table: { disable: true } },
    requestorWebId: { table: { disable: true } },
    documentType: { table: { disable: true } },
    returnUrl: { table: { disable: true } },
    url: { table: { disable: true } },

    handleClick: { action: 'clicked' },
  },
} as Meta<typeof RequestListItem>;

const Template: StoryFn<typeof RequestListItem> = (args) => <RequestListItem {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  uuid: 'a9a6d44c-b9ef-465a-855c-740d4224bd19',
  url: new URL('https://example.com/some/path/a9a6d44c-b9ef-465a-855c-740d4224bd19'),
  purpose: 'To process your data.',
  created: new Date(),
  providerWebId: new URL('https://example.com/provider'),
  requestorWebId: new URL('https://example.com/requestor'),
  documentType: 'https://example.com/#documentType',
  requestor: { name: 'ACME Inc.', storage: 'https://example.com/some/path' },
};

export const Logo = Template.bind({});
Logo.args = {
  ...Standard.args,
  requestor: {
    name: 'ACME Inc.',
    storage: 'https://example.com/some/path',
    logo: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxyZWN0IHN0eWxlPSJmaWxsOiNlNjM5NDYiIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgeD0iMCIgeT0iMCIgLz48Y2lyY2xlIHN0eWxlPSJmaWxsOiM0NTdiOWQiIGN4PSIxMiIgY3k9IjEyIiByPSI0IiAvPjwvc3ZnPgo=',
  },
};

export const Unread = Template.bind({});
Unread.args = {
  ...Standard.args,
  unread: true,
};
