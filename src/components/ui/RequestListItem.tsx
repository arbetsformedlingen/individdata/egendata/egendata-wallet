import React from 'react';
import { OrganizationAgentInfo, RequestInfo } from '@app/types';
import { useRouter } from 'next/router';

type RequestListItemProps = RequestInfo & {
  requestor: OrganizationAgentInfo;
};

function RequestListItem({ uuid, purpose, created, unread = false, requestor }: RequestListItemProps) {
  const router = useRouter();

  return (
    <li
      className="flex w-full select-none gap-2 rounded-full bg-zinc-100 p-2 hover:bg-zinc-200 hover:text-black dark:bg-zinc-900  dark:text-zinc-300 dark:hover:bg-zinc-800 dark:hover:text-white"
      onClick={() => router.push(`/request/${uuid}`)}
    >
      <div>
        {/* eslint-disable-next-line @next/next/no-img-element */}
        <img
          src={
            requestor.logo ??
            'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxyZWN0IHN0eWxlPSJmaWxsOnRyYW5zcGFyZW50IiB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHg9IjAiIHk9IjAiIC8+PGNpcmNsZSBzdHlsZT0iZmlsbDojNDU3YjlkIiBjeD0iMTIiIGN5PSIxMiIgcj0iNCIgLz48L3N2Zz4K'
          }
          alt={`${requestor.name} logotype`}
          className="rounded-full"
          width="24px"
          height="24px"
        />
      </div>
      <div className="w-3/12">{requestor.name}</div>
      <div className="flex-grow">{purpose ?? 'Unknown purpose'}</div>
      <div className="flex-initial pr-2">{(created && created.toISOString().substring(0, 10)) ?? '????-??-??'}</div>
    </li>
  );
}

export default RequestListItem;
