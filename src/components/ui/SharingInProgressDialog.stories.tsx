import React from 'react';

import { Meta, StoryFn } from '@storybook/react';
import SharingInProgressDialog from './SharingInProgressDialog';

export default {
  title: 'SharingInProgressDialog',
  component: SharingInProgressDialog,
  argTypes: {},
} as Meta<typeof SharingInProgressDialog>;

const Template: StoryFn<typeof SharingInProgressDialog> = (args) => <SharingInProgressDialog {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  open: true,
  state: 'sharing',
};
