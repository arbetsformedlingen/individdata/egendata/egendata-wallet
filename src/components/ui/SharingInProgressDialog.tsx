import React from 'react';
import { ExtendedRequestState } from '@app/types';
import { FormattedMessage } from 'react-intl';
import { CloseIcon, EmailIcon, ErrorIcon, OkIcon } from '../../icons/dialog';
import ProgressBar from './ProgressBar';
import { Dialog } from '@headlessui/react';

type SharingInProgressDialogProps = {
  open: boolean;
  onClose: () => void;
  state: ExtendedRequestState | 'success';
  error: boolean;
};

const SharingInProgressDialog = ({ open, onClose, state, error }: SharingInProgressDialogProps) => {
  return (
    <Dialog open={open} onClose={onClose} className="relative z-50">
      {/* Backdrop */}
      <div className="fixed inset-0 flex bg-black/20 backdrop-blur-sm"></div>

      {/* Centering container */}
      <div className="fixed inset-0 flex items-center justify-center p-4">
        <Dialog.Panel className="w-full max-w-sm rounded-lg border border-zinc-300 bg-zinc-50 p-4 text-center shadow-lg dark:border-zinc-900 dark:bg-zinc-800">
          <div className="flex flex-col items-center gap-8 py-8">
            {state === 'sharing' && !error && (
              <>
                <p>
                  <FormattedMessage defaultMessage="Sharing..." id="sg8aJi" description="Sharing document modal." />
                </p>
                <ProgressBar />
              </>
            )}
            {state === 'success' && !error && (
              <>
                <OkIcon className="h-8 w-8" />
                <p>
                  <FormattedMessage
                    defaultMessage="Your {document} is now being shared with {requestor}."
                    id="BA6wMO"
                    description="Sharing document modal, successful."
                    values={{
                      document: '_document_',
                      requestor: '_requestor_',
                    }}
                  />
                </p>
                <p>
                  <FormattedMessage
                    defaultMessage="You can always revoke your consent under Consents."
                    id="UCFPOA"
                    description="Sharing document modal, successful information."
                  />
                </p>
                <p>
                  <FormattedMessage
                    defaultMessage="Add your email address to get notified when further actions are needed."
                    id="Z35aBz"
                    description="Sharing document modal, get notified by email."
                  />
                </p>
              </>
            )}
            {error && (
              <>
                <ErrorIcon className="h-8 w-8" />
                <FormattedMessage
                  defaultMessage="Something went wrong when sharing your document. Try again later."
                  id="/+PMp+"
                  description="Sharing document modal, error."
                />
              </>
            )}
          </div>
        </Dialog.Panel>
      </div>
    </Dialog>
  );
};

export default SharingInProgressDialog;
