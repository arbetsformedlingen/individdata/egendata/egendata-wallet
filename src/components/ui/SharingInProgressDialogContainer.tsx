import { ExtendedRequestState } from '@app/types';
import { useState } from 'react';
import SharingInProgressDialog from './SharingInProgressDialog';

const SharingInProgressDialogContainer = () => {
  const [open, setOpen] = useState(false);
  const [email, setEmail] = useState('');
  const [state, setState] = useState<ExtendedRequestState>('sharing');

  setTimeout(() => {
    if (state === 'sharing') {
      setState('error');
    }
  }, 15 * 1000);

  function handleAddEmail() {
    console.log(`Adding email: ${email}`);
  }

  function onClose() {
    console.log('closing');
  }

  return <SharingInProgressDialog open={open} onClose={onClose} state={'received'} error={false} />;
};

export default SharingInProgressDialogContainer;
