import { Meta, StoryFn } from '@storybook/react';

import React from 'react';
import Stepper from './Stepper';

export default {
  title: 'Stepper',
  component: Stepper,
  argTypes: {
    visualState: {
      options: ['received', 'fetching', 'available', 'sharing', 'shared', 'landing'],
      default: 'received',
      control: { type: 'select' },
    },
  },
} as Meta<typeof Stepper>;

const Template: StoryFn<typeof Stepper> = (args) => <Stepper {...args} />;

export const Standard = Template.bind({});
Standard.args = {
  requestorName: 'Evil Corp.',
  documentTitle: 'Birth Certificate',
};

export const Landing = Template.bind({});
Landing.args = {
  ...Standard.args,
  visualState: 'landing',
};
