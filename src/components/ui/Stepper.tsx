import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Arrow, CheckIcon, StepOneActiveIcon, StepOneIcon, StepTwoActiveIcon, StepTwoIcon } from '../../icons/stepper';
import { ExtendedRequestState } from '@app/types';

type Props = {
  visualState: ExtendedRequestState | 'landing';
  requestorName: string;
  documentTitle: string;
};

const Stepper = ({ visualState, requestorName, documentTitle }: Props) => {
  let stepOne = <StepOneIcon className="h-8 w-8" />;
  let stepTwo = <StepTwoIcon className="h-8 w-8" />;
  let arrowActive = true;

  if (visualState !== 'landing') {
    if (visualState === 'received') {
      stepOne = <StepOneActiveIcon className="h-8 w-8" />;
    } else {
      stepOne = <CheckIcon className="h-8 w-8" />;
    }

    arrowActive = ['fetching', 'available', 'sharing'].includes(visualState);

    if (visualState === 'available') {
      stepTwo = <StepTwoActiveIcon className="h-8 w-8" />;
    } else if (visualState === 'sharing' || visualState === 'shared') {
      stepTwo = <CheckIcon className="h-8 w-8" />;
    }
  }

  return (
    <div className="mx-auto flex w-full select-none flex-col gap-4 rounded-lg bg-blue-700 px-4 py-8 text-center md:w-8/12">
      {visualState === 'landing' && <h6 className="-mb-4 text-lg text-white">Egendata</h6>}
      <div className="flex flex-row items-center">
        <div className="grow"></div>
        <div className="flex-none">{stepOne}</div>
        <div className="flex w-0 grow-[2] justify-center">
          <Arrow className={arrowActive ? 'text-[#65D36E]' : 'text-white text-opacity-20'} />
        </div>
        <div className="flex-none">{stepTwo}</div>
        <div className="grow"></div>
      </div>

      <div className="flex flex-row gap-2 text-lg text-white">
        <div className="flex-1">
          <FormattedMessage id="o1FDX1" defaultMessage="Get your data" description="Stepper first step title." />
        </div>
        <div className="flex-1">
          <FormattedMessage id="kmUDMm" defaultMessage="Share your data" description="Stepper second step title." />
        </div>
      </div>

      {visualState !== 'landing' && (
        <div className="flex flex-row gap-2 text-slate-200">
          <div className="flex-1">
            <FormattedMessage
              id="8GfnrI"
              defaultMessage="Consent and fetch your {documentTitle}"
              description="Stepper first step description."
              values={{
                documentTitle: documentTitle,
              }}
            />
          </div>
          <div className="flex-1">
            <FormattedMessage
              id="g8BAJB"
              defaultMessage="Review your data and share it with {requestor}"
              description="Stepper second step description."
              values={{
                requestor: requestorName,
              }}
            />
          </div>
        </div>
      )}
    </div>
  );
};

export default Stepper;
