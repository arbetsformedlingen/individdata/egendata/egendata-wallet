import { api } from '@app/utils/api';
import Stepper from './Stepper';
import { stringFromLocalizedString, useLanguage } from '@app/lib/i18n';

type Props = { requestId: string; landing?: never } | { requestId?: never; landing: boolean };

export function StepperContainer({ requestId = '', landing = false }: Props) {
  const { language } = useLanguage();
  const request = api.request.getRequestDetails.useQuery({ id: requestId }, { enabled: requestId !== '' });

  const documentTitle = stringFromLocalizedString(language, request.data?.provider.documentTitle);

  return (
    <Stepper
      visualState={landing ? 'landing' : request.data?.state!}
      requestorName={request.data?.requestor.name ?? 'Unknown'}
      documentTitle={documentTitle}
    />
  );
}

export default StepperContainer;
