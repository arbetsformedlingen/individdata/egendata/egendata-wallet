import { Meta, StoryFn } from '@storybook/react';

import React from 'react';
import ThemeSwitch from './ThemeSwitch';

export default {
  title: 'ThemeSwitch',
  component: ThemeSwitch,
  argTypes: {
    current: {
      options: ['light', 'dark'],
      control: { type: 'select' },
      defaultValue: 'light',
    },
  },
} as Meta<typeof ThemeSwitch>;

const Template: StoryFn<typeof ThemeSwitch> = (args) => <ThemeSwitch {...args} />;

export const Light = Template.bind({});
Light.args = {
  current: 'light',
};

export const Dark = Template.bind({});
Dark.args = {
  current: 'dark',
};
