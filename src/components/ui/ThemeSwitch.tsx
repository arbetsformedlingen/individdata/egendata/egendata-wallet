import { DarkModeIcon, LightModeIcon } from '@app/icons/icons';
import { type ColorMode } from '@app/lib/darkMode';

const ThemeSwitch = ({ current, switchColorMode }: { current: ColorMode; switchColorMode: () => void }) => {
  return (
    <button
      onClick={switchColorMode}
      className="mx-1 flex-grow-0 rounded-full bg-zinc-50 p-2 text-zinc-800 hover:bg-zinc-300 hover:text-black dark:bg-zinc-800 dark:text-zinc-300 dark:hover:bg-zinc-900 dark:hover:text-white"
    >
      {current === 'light' && <DarkModeIcon className="h-6 w-6" />}
      {current !== 'light' && <LightModeIcon className="h-6 w-6" />}
    </button>
  );
};

export default ThemeSwitch;
