import { useEffect, useState } from 'react';
import { type ColorMode, getColorMode, switchColorMode } from '@app/lib/darkMode';
import ThemeSwitch from './ThemeSwitch';

const ThemeSwitchContainer = () => {
  const [colorMode, setColorMode] = useState<ColorMode | undefined>();

  useEffect(() => {
    if (!colorMode) {
      setColorMode(getColorMode());
    }
  }, [colorMode]);

  const switchMode = () => {
    setColorMode(switchColorMode());
  };

  return <ThemeSwitch current={colorMode ?? 'dark'} switchColorMode={switchMode} />;
};

export default ThemeSwitchContainer;
