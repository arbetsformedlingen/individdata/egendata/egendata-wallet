/* eslint-disable @typescript-eslint/ban-ts-comment */
import { z } from 'zod';

/**
 * Specify your server-side environment variables schema here.
 * This way you can ensure the app isn't built with invalid env vars.
 */
const server = z.object({
  NODE_ENV: z.enum(['development', 'test', 'production']),
  NEXTAUTH_SECRET: process.env.NODE_ENV === 'production' ? z.string().min(1) : z.string().min(1).optional(),
  NEXTAUTH_URL: z.string().url(),
  SOLID_CLIENT_ID: z.string(),
  SOLID_CLIENT_SECRET: z.string(),
  EGENDATA_INFRA_ID: z.string(),
  EGENDATA_INFRA_SECRET: z.string(),
  EGENDATA_SCHEMA_URL: z.string().url(),
  EGENDATA_PATH_FRAGMENT: z.string(),
  IDP_BASE_URL: z.string().url(),
  POD_BASE_URL: z.string().url(),
  NEXT_MANUAL_SIG_HANDLE: z.boolean().optional(),
});

/**
 * Specify your client-side environment variables schema here.
 * This way you can ensure the app isn't built with invalid env vars.
 * To expose them to the client, prefix them with `NEXT_PUBLIC_`.
 */
const client = z.object({
  // NEXT_PUBLIC_CLIENTVAR: z.string().min(1),
  NEXT_PUBLIC_COMMIT: z.string().optional(),
});

/**
 * You can't destruct `process.env` as a regular object in the Next.js
 * edge runtimes (e.g. middlewares) or client-side so we need to destruct manually.
 * @type {Record<keyof z.infer<typeof server> | keyof z.infer<typeof client>, string | undefined>}
 */
const processEnv = {
  NODE_ENV: process.env.NODE_ENV,
  NEXTAUTH_SECRET: process.env.NEXTAUTH_SECRET,
  NEXTAUTH_URL: process.env.NEXTAUTH_URL,
  SOLID_CLIENT_ID: process.env.SOLID_CLIENT_ID,
  SOLID_CLIENT_SECRET: process.env.SOLID_CLIENT_SECRET,
  EGENDATA_INFRA_ID: process.env.EGENDATA_INFRA_ID,
  EGENDATA_INFRA_SECRET: process.env.EGENDATA_INFRA_SECRET,
  EGENDATA_SCHEMA_URL: process.env.EGENDATA_SCHEMA_URL,
  EGENDATA_PATH_FRAGMENT: process.env.EGENDATA_PATH_FRAGMENT,
  IDP_BASE_URL: process.env.IDP_BASE_URL,
  POD_BASE_URL: process.env.POD_BASE_URL,
  // NEXT_PUBLIC_CLIENTVAR: process.env.NEXT_PUBLIC_CLIENTVAR,
  NEXT_PUBLIC_COMMIT: process.env.NEXT_PUBLIC_COMMIT,
};

// Don't touch the part below
// --------------------------

const merged = server.merge(client);
/** @type z.infer<merged>
 *  @ts-ignore - can't type this properly in jsdoc */
let env = process.env;

if (!!process.env.SKIP_ENV_VALIDATION == false) {
  const isServer = typeof window === 'undefined';

  const parsed = isServer
    ? merged.safeParse(processEnv) // on server we can validate all env vars
    : client.safeParse(processEnv); // on client we can only validate the ones that are exposed

  if (parsed.success === false) {
    console.error('❌ Invalid environment variables:', parsed.error.flatten().fieldErrors);
    throw new Error('Invalid environment variables');
  }

  /** @type z.infer<merged>
   *  @ts-ignore - can't type this properly in jsdoc */
  env = new Proxy(parsed.data, {
    get(target, prop) {
      if (typeof prop !== 'string') return undefined;
      // Throw a descriptive error if a server-side env var is accessed on the client
      // Otherwise it would just be returning `undefined` and be annoying to debug
      if (!isServer && !prop.startsWith('NEXT_PUBLIC_'))
        throw new Error(
          process.env.NODE_ENV === 'production'
            ? '❌ Attempted to access a server-side environment variable on the client'
            : `❌ Attempted to access server-side environment variable '${prop}' on the client`,
        );
      /*  @ts-ignore - can't type this properly in jsdoc */
      return target[prop];
    },
  });
}

export { env };
