import { describe, expect, it, vi } from 'vitest';
import SolidProvider, { KeyPair, SolidProfile, generateDPoPKeyPair } from './SolidProvider';
import { type IdTokenClaims, type TokenSet } from 'openid-client/types';
import { AuthorizationEndpointHandler, OAuthConfig, TokenEndpointHandler } from 'next-auth/providers/oauth';
import { BaseClient } from 'openid-client';

describe('generateDPoPKeyPair', () => {
  it('should return a pair of JWKs', async () => {
    const keys = await generateDPoPKeyPair();

    expect(keys.privateKey.kty).toBe('EC');
    expect(keys.privateKey.x).toBeDefined();
    expect(keys.privateKey.y).toBeDefined();
    expect(keys.privateKey.crv).toBe('P-256');
    expect(keys.privateKey.d).toBeDefined();

    expect(keys.publicKey.kty).toBe('EC');
    expect(keys.publicKey.x).toBeDefined();
    expect(keys.publicKey.y).toBeDefined();
    expect(keys.publicKey.crv).toBe('P-256');
    expect(keys.publicKey.alg).toBe('ES256');
  });
});

describe('SolidProvider', () => {
  it('should have correct parameters', () => {
    const provider = SolidProvider({ clientId: 'some-client-id', clientSecret: 'some-very-secret-secret' });

    expect(provider.id).toBe('solid');

    expect(provider.type).toBe('oauth');
    expect(provider.wellKnown?.endsWith('.well-known/openid-configuration')).toBeTruthy();
    expect((provider.authorization as AuthorizationEndpointHandler).params?.grant_type).toStrictEqual([
      'authorization_code',
      'refresh_token',
    ]);
    expect((provider.authorization as AuthorizationEndpointHandler).params?.scope).toBe('openid offline_access webid');
    expect((provider.authorization as AuthorizationEndpointHandler).params?.prompt).toBe('consent');
    expect(provider.idToken).toBeTruthy();
    expect(provider.checks).toStrictEqual(['pkce', 'state']);
    expect(provider.client?.authorization_signed_response_alg).toBe('RS256');
    expect(provider.client?.id_token_signed_response_alg).toBe('ES256');
    expect((provider.token as TokenEndpointHandler).url).toBeDefined();
    expect((provider.token as TokenEndpointHandler).request).toBeTypeOf('function');
    expect(provider.profile).toBeTypeOf('function');

    expect(provider.options?.clientId).toBe('some-client-id');
    expect(provider.options?.clientSecret).toBe('some-very-secret-secret');
  });

  it('should have a token-request function that returns', async () => {
    const provider = SolidProvider({ clientId: '', clientSecret: '' });

    const params = { code: '' };
    const checks = { code_verifier: '' };
    const client = {
      grant: vi.fn(
        (): TokenSet => ({
          access_token: '',
          expired: () => false,
          claims: (): IdTokenClaims => ({
            aud: '',
            exp: 123,
            iat: 456,
            iss: '',
            sub: '',
          }),
        }),
      ),
    } as unknown as BaseClient;
    const provider_ = {} as OAuthConfig<Record<any, any>> & { signinUrl: string; callbackUrl: string };

    const token = provider.token as TokenEndpointHandler;

    const { tokens } = await token.request!({ params, checks, client, provider: provider_ });

    expect((tokens.keys as KeyPair).privateKey).toBeDefined();
    expect((tokens.keys as KeyPair).publicKey).toBeDefined();
    expect(tokens.access_token).toBeDefined();
  });

  it('should have a profile function that returns', async () => {
    const provider = SolidProvider({ clientId: '', clientSecret: '' });

    const profileRetval = await provider.profile({ sub: 'some-sub-string' }, {});

    expect(profileRetval.id).toBe('some-sub-string');
    expect(profileRetval.webid).toBe('some-sub-string');
  });
});
