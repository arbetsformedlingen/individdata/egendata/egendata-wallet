import { env } from '@app/env.mjs';
import { JWK, exportJWK, generateKeyPair } from 'jose';
import type { OAuthConfig, OAuthUserConfig } from 'next-auth/providers';
export interface SolidProfile extends Record<string, any> {
  sub: string;
}

export type KeyPair = {
  privateKey: JWK;
  publicKey: JWK;
};

export async function generateDPoPKeyPair(): Promise<KeyPair> {
  const { privateKey, publicKey } = await generateKeyPair('ES256');

  const dpopKeyPair = {
    privateKey: await exportJWK(privateKey),
    publicKey: await exportJWK(publicKey),
  };

  dpopKeyPair.publicKey.alg = 'ES256';

  return dpopKeyPair;
}

export default function SolidProvider<P extends SolidProfile>(options: OAuthUserConfig<P>): OAuthConfig<P> {
  return {
    id: 'solid',
    name: 'Solid',
    type: 'oauth',
    wellKnown: new URL('.well-known/openid-configuration', env.IDP_BASE_URL).toString(),
    authorization: {
      params: {
        grant_type: ['authorization_code', 'refresh_token'],
        scope: 'openid offline_access webid',
        prompt: 'consent', // https://openid.net/specs/openid-connect-core-1_0.html#OfflineAccess
      },
    },
    idToken: true,
    checks: ['pkce', 'state'], // TODO: Is "state" useful?
    client: {
      authorization_signed_response_alg: 'RS256',
      id_token_signed_response_alg: 'ES256',
    },
    token: {
      url: new URL('.oidc/token', env.IDP_BASE_URL).toString(),
      async request({ params, checks, client, provider }) {
        // TODO: Make sure to run checks and validate properly!

        const dpopKeyPair = await generateDPoPKeyPair();

        // Request DPoP token.
        const tokens = await client.grant(
          {
            grant_type: 'authorization_code',
            code: params.code,
            redirect_uri: provider.callbackUrl,
            code_verifier: checks.code_verifier,
          },
          {
            DPoP: { key: dpopKeyPair.privateKey, format: 'jwk' },
          },
        );

        tokens.keys = dpopKeyPair;

        return { tokens };
      },
    },
    profile(profile) {
      return {
        id: profile.sub,
        webid: profile.sub,
      };
    },
    options,
  };
}
