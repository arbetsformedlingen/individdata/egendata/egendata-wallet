import { afterEach, describe, expect, it, vi } from 'vitest';
import { defineColorMode, getColorMode, switchColorMode } from './darkMode';

describe('getColorMode', () => {
  afterEach(() => {
    vi.resetAllMocks();
  });

  it('retrieves light color mode from local storage', () => {
    const getItemSpy = vi.spyOn(Object.getPrototypeOf(localStorage), 'getItem').mockReturnValue('light');
    vi.spyOn(window, 'matchMedia').mockReturnValue({ matches: true } as MediaQueryList);

    const mode = getColorMode();

    expect(mode).toBe('light');
    expect(getItemSpy).toBeCalledWith('color-mode');
  });

  it('retrieves dark color mode from local storage', () => {
    const getItemSpy = vi.spyOn(Object.getPrototypeOf(localStorage), 'getItem').mockReturnValue('dark');
    vi.spyOn(window, 'matchMedia').mockReturnValue({ matches: true } as MediaQueryList);

    const mode = getColorMode();

    expect(mode).toBe('dark');
    expect(getItemSpy).toBeCalledWith('color-mode');
  });

  it('returns dark color mode when unset local storage but preference set to dark', () => {
    const getItemSpy = vi.spyOn(Object.getPrototypeOf(localStorage), 'getItem').mockReturnValue(null);
    vi.spyOn(window, 'matchMedia').mockReturnValue({ matches: true } as MediaQueryList);

    const mode = getColorMode();

    expect(mode).toBe('dark');
    expect(getItemSpy).toBeCalledWith('color-mode');
  });

  it('defaults to light color mode when local storage unset and no preference not set to dark', () => {
    const getItemSpy = vi.spyOn(Object.getPrototypeOf(localStorage), 'getItem').mockReturnValue(null);
    vi.spyOn(window, 'matchMedia').mockReturnValue({ matches: false } as MediaQueryList);

    const mode = getColorMode();

    expect(mode).toBe('light');
    expect(getItemSpy).toBeCalledWith('color-mode');
  });
});

describe('switchColorMode', () => {
  it('switches to dark mode when no preference set (assume light mode)', () => {
    const getItemSpy = vi.spyOn(Object.getPrototypeOf(localStorage), 'getItem').mockReturnValue(null);
    vi.spyOn(window, 'matchMedia').mockReturnValue({ matches: false } as MediaQueryList);

    const newMode = switchColorMode();

    expect(newMode).toBe('dark');
    expect(getItemSpy).toBeCalledWith('color-mode');
  });

  it('switches to light mode when dark mode preference set', () => {
    const getItemSpy = vi.spyOn(Object.getPrototypeOf(localStorage), 'getItem').mockReturnValue('null');
    vi.spyOn(Object, 'hasOwn').mockReturnValue(false);
    vi.spyOn(window, 'matchMedia').mockReturnValue({ matches: true } as MediaQueryList);

    const newMode = switchColorMode();

    expect(newMode).toBe('light');
    expect(getItemSpy).toBeCalledWith('color-mode');
  });

  it('updates the class of the <html> tag when switching to light mode', () => {
    vi.spyOn(Object.getPrototypeOf(localStorage), 'getItem').mockReturnValue('dark');
    vi.spyOn(window, 'matchMedia').mockReturnValue({ matches: false } as MediaQueryList);
    const addSpy = vi.spyOn(document.documentElement.classList, 'remove');

    const newMode = switchColorMode();

    expect(newMode).toBe('light');
    expect(addSpy).toBeCalledWith('dark');
  });

  it('updates the class of the <html> tag when switching to dark mode', () => {
    vi.spyOn(Object.getPrototypeOf(localStorage), 'getItem').mockReturnValue('light');
    vi.spyOn(window, 'matchMedia').mockReturnValue({ matches: false } as MediaQueryList);
    const addSpy = vi.spyOn(document.documentElement.classList, 'add');

    const newMode = switchColorMode();

    expect(newMode).toBe('dark');
    expect(addSpy).toBeCalledWith('dark');
  });

  it('switches to light mode when local storage set to dark mode', () => {
    const getItemSpy = vi.spyOn(Object.getPrototypeOf(localStorage), 'getItem').mockReturnValue('dark');
    const setItemSpy = vi.spyOn(Object.getPrototypeOf(localStorage), 'setItem');
    vi.spyOn(window, 'matchMedia').mockReturnValue({ matches: false } as MediaQueryList);

    const newMode = switchColorMode();

    expect(newMode).toBe('light');
    expect(setItemSpy).toBeCalledWith('color-mode', 'light');
    expect(getItemSpy).toBeCalledWith('color-mode');
  });

  it('switches to dark mode when local storage set to light mode', () => {
    const getItemSpy = vi.spyOn(Object.getPrototypeOf(localStorage), 'getItem').mockReturnValue('light');
    const setItemSpy = vi.spyOn(Object.getPrototypeOf(localStorage), 'setItem');
    vi.spyOn(window, 'matchMedia').mockReturnValue({ matches: false } as MediaQueryList);

    const newMode = switchColorMode();

    expect(newMode).toBe('dark');
    expect(setItemSpy).toBeCalledWith('color-mode', 'dark');

    expect(getItemSpy).toBeCalledWith('color-mode');
  });
});

describe('defineColorMode', () => {
  it('adds light class to html element', () => {
    vi.spyOn(Object.getPrototypeOf(localStorage), 'getItem').mockReturnValue('light');
    const removeSpy = vi.spyOn(document.documentElement.classList, 'remove');
    vi.spyOn(window, 'matchMedia').mockReturnValue({ matches: false } as MediaQueryList);

    defineColorMode();

    expect(removeSpy).toBeCalledWith('dark');
  });

  it('adds dark class to html element', () => {
    vi.spyOn(Object.getPrototypeOf(localStorage), 'getItem').mockReturnValue('dark');
    const addSpy = vi.spyOn(document.documentElement.classList, 'add');
    vi.spyOn(window, 'matchMedia').mockReturnValue({ matches: false } as MediaQueryList);

    defineColorMode();

    expect(addSpy).toBeCalledWith('dark');
  });
});
