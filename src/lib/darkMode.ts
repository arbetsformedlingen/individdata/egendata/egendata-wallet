export type ColorMode = 'dark' | 'light';

export const getColorMode = (): ColorMode => {
  const persistedColorPreference = localStorage.getItem('color-mode');
  const hasPersistedPreference = typeof persistedColorPreference === 'string';

  if (hasPersistedPreference) {
    return persistedColorPreference as ColorMode;
  }

  const mql = window.matchMedia('(prefers-color-scheme: dark)');
  const hasMediaQueryPreference = typeof mql.matches === 'boolean';

  if (hasMediaQueryPreference) {
    return mql.matches ? 'dark' : 'light';
  }

  return 'light';
};

export const switchColorMode = (): ColorMode => {
  const darkModeActive =
    localStorage.getItem('color-mode') === 'dark' ||
    (!Object.hasOwn(localStorage, 'color-mode') && window.matchMedia('(prefers-color-scheme: dark)').matches);

  if (darkModeActive) {
    localStorage.setItem('color-mode', 'light');
    document.documentElement.classList.remove('dark');
    return 'light';
  } else {
    localStorage.setItem('color-mode', 'dark');
    document.documentElement.classList.add('dark');
    return 'dark';
  }
};

export const defineColorMode = () => {
  const darkModeActive =
    localStorage.getItem('color-mode') === 'dark' ||
    (!Object.hasOwn(localStorage, 'color-mode') && window.matchMedia('(prefers-color-scheme: dark)').matches);

  if (darkModeActive) {
    document.documentElement.classList.add('dark');
  } else {
    document.documentElement.classList.remove('dark');
  }
};
