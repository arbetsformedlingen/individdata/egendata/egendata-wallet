import { Mocked, afterAll, afterEach, beforeAll, describe, expect, it, vi } from 'vitest';
import { importJWK, jwtVerify } from 'jose';

import fetchFactory, { generateDPoP } from './fetchFactory';
import logger from './logger';

describe('fetchFactory', () => {
  let fetchMock: Mocked<typeof fetch>;

  beforeAll(() => {
    fetchMock = vi.fn();
    vi.stubGlobal('fetch', fetchMock);
  });

  afterAll(() => {
    vi.unstubAllGlobals();
  });

  afterEach(() => {
    vi.resetAllMocks();
  });

  it('throws when missing keyPair', () => {
    const props = { keyPair: undefined, dpopToken: 'some-token-value' };

    expect(() => fetchFactory(props)).toThrow();
  });

  it('throws when missing dpopToken', () => {
    const props = { keyPair: { privateKey: 'private-key', publicKey: 'publicKey' }, dpopToken: undefined };

    expect(() => fetchFactory(props)).toThrow();
  });

  it('returns a fetch function that sets DPoP headers', async () => {
    const mockGenerateDPoP = vi.fn(async () => 'dpop-header-token-value');

    const fetcher = fetchFactory(
      {
        keyPair: {},
        dpopToken: 'dpop-token-value',
      },
      mockGenerateDPoP,
    );

    await fetcher('/test');

    expect(mockGenerateDPoP).toBeCalledTimes(1);

    expect(fetchMock).toBeCalledTimes(1);
    expect(fetchMock).toBeCalledWith('/test', {
      headers: new Headers({ authorization: 'DPoP dpop-token-value', dpop: 'dpop-header-token-value' }),
      method: 'GET',
    });
  });

  it('returns a fetch function that sets DPoP headers and preserves custom headers', async () => {
    const mockGenerateDPoP = vi.fn(async () => 'dpop-header-token-value');

    const fetcher = fetchFactory(
      {
        keyPair: {},
        dpopToken: 'dpop-token-value',
      },
      mockGenerateDPoP,
    );

    await fetcher('/test', { headers: { Accept: 'application/json' }, method: 'POST' });

    expect(mockGenerateDPoP).toBeCalledTimes(1);

    expect(fetch).toBeCalledTimes(1);
    expect(fetch).toBeCalledWith('/test', {
      headers: new Headers({
        accept: 'application/json',
        authorization: 'DPoP dpop-token-value',
        dpop: 'dpop-header-token-value',
      }),
      method: 'POST',
    });
  });
});

describe('generateDPoP', () => {
  it('generates valid DPoP token for request', async () => {
    const keys = {
      privateKey: {
        kty: 'EC',
        x: 'ltIfue2sUa1PBb066AdMW6Kx1ciqrX2d8blxZI4yo_c',
        y: 'EmHtdrra0wpniikNykwoHPwVgCMzpnDaNafXYtdSJrY',
        crv: 'P-256',
        d: 'cCDaWqR6lR4m3abelmsc5ztw2whxg1QMUEIwFxpJQ0A',
      },
      publicKey: {
        kty: 'EC',
        x: 'ltIfue2sUa1PBb066AdMW6Kx1ciqrX2d8blxZI4yo_c',
        y: 'EmHtdrra0wpniikNykwoHPwVgCMzpnDaNafXYtdSJrY',
        crv: 'P-256',
        alg: 'ES256',
      },
    };

    const dpop = await generateDPoP({
      jwkPrivateKey: keys.privateKey,
      jwkPublicKey: keys.publicKey,
      url: 'https://example.com/some/path',
      method: 'GET',
    });

    const { payload, protectedHeader } = await jwtVerify(dpop, await importJWK(keys.privateKey));

    expect(payload.htu).toBe('https://example.com/some/path');
    expect(payload.htm).toBe('GET');
    expect(protectedHeader.typ).toBe('dpop+jwt');
  });
});
