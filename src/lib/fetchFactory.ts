import { env } from '../env.mjs';
import { JWK, SignJWT, importJWK } from 'jose';
import { v4 as uuid } from 'uuid';

import { logger as parentLogger } from './logger';
import { generateDPoPKeyPair } from './SolidProvider';
// import { FetchInterface } from '@egendata/egendata-interop';
export const logger = parentLogger.child({ module: 'fetchFactory' });

// export type FetchInterface = typeof nodeFetch;
// Reexport FetchInterface from egendata-interop to avoid type issues.
// export { type FetchInterface } from '@egendata/egendata-interop';
export type FetchInterface = (url: URL | RequestInfo, init?: RequestInit) => Promise<Response>;

export type generateDPoPProps = {
  jwkPrivateKey: JWK;
  jwkPublicKey: JWK;
  method: string;
  url: string;
};

export async function generateDPoP(props: generateDPoPProps) {
  const { jwkPrivateKey, jwkPublicKey, method, url } = props;
  jwkPrivateKey.alg = 'ES256';
  const privateKey = await importJWK(jwkPrivateKey);

  const reformedURL = new URL(url);
  reformedURL.hash = '';
  reformedURL.search = '';

  return new SignJWT({
    htu: reformedURL.toString(),
    htm: method.toUpperCase(),
    jti: uuid(),
  })
    .setProtectedHeader({
      alg: 'ES256',
      jwk: jwkPublicKey,
      typ: 'dpop+jwt',
    })
    .setIssuedAt()
    .sign(privateKey);
}

export const getInfraAccessToken = async () => {
  logger.debug('Fetching access token ...');
  const dpopKeyPair = await generateDPoPKeyPair();
  const authString = `${encodeURIComponent(env.EGENDATA_INFRA_ID)}:${encodeURIComponent(env.EGENDATA_INFRA_SECRET)}`;
  const tokenUrl = new URL('/.oidc/token', env.IDP_BASE_URL);

  const method = 'POST';
  const headers = {
    // The header needs to be in base64 encoding.
    authorization: `Basic ${Buffer.from(authString).toString('base64')}`,
    'content-type': 'application/x-www-form-urlencoded',
    dpop: await generateDPoP({
      jwkPrivateKey: dpopKeyPair.privateKey,
      jwkPublicKey: dpopKeyPair.publicKey,
      method: 'POST',
      url: tokenUrl.toString(),
    }),
  };
  const body = new URLSearchParams({
    grant_type: 'client_credentials',
    scope: 'webid',
  });

  logger.trace({ method, headers, body }, 'Attempting to fetch tokens.');
  const response = await fetch(tokenUrl, { method, headers, body });

  if (!response.ok) {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    const { error, error_description } = (await response.json()) as any;
    throw new Error(`Could not fetch access token: ${error}, ${error_description}`);
  }

  const data = (await response.json()) as any;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { access_token } = data;
  const expiresAt = Date.now() + data.expires_in * 1000;
  const rv = { accessToken: access_token, dpopKeyPair, expiresAt };
  logger.trace(rv, 'Returning tokens.');
  return rv;
};

export type fetchFactoryProps = {
  keyPair:
    | {
        privateKey: JWK;
        publicKey: JWK;
      }
    | unknown;
  dpopToken: string | unknown;
};

let count = 0;

export default function fetchFactory(props: fetchFactoryProps, generator = generateDPoP): FetchInterface {
  if (!(props.keyPair && props.dpopToken)) throw 'Missing keyPair and/or DPoP access token.';

  const { privateKey, publicKey } = props.keyPair as { privateKey: JWK; publicKey: JWK };
  const { dpopToken } = props;

  return async (input, init?): Promise<Response> => {
    let _fetch = fetch;

    if (logger.isLevelEnabled('trace')) {
      _fetch = async (input, init) => {
        // const id = uuid().slice(0, 8);
        const id = count++;
        const color = ['GET', 'HEAD'].includes(init?.method ?? 'GET')
          ? { out: '\u001b[45m', in: '\u001b[105m' }
          : { out: '\u001b[46m', in: '\u001b[106m' };

        logger.trace({ input, init }, `${color.out}>> Fetch #${id} \u001b[0m (${init?.method}, DPoP) ${input}`);
        return fetch(input, init).then(
          (success) => {
            logger.trace(`${color.in}<< Fetch #${id} \u001b[0m (${init?.method}, ${success.status}) ${input}`);
            return success;
          },
          (error) => {
            logger.trace(error, `❌ Fetch #${id} failed (${init?.method}, ${error.status}) ${input}`);
            return error;
          },
        );
      };
    }

    init = init || {};
    init.method = init.method || 'GET';

    const dpopHeader = await generator({
      jwkPrivateKey: privateKey,
      jwkPublicKey: publicKey,
      method: init.method,
      url: input.toString(),
    });

    /** A Headers object, an object literal, or an array of two-item arrays to set request's headers. */
    init.headers = new Headers(init.headers || {});
    init.headers.set('Authorization', `DPoP ${dpopToken}`);
    init.headers.set('DPoP', dpopHeader);

    return _fetch(input, init);
  };
}
