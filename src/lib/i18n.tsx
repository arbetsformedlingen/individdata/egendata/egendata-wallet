import React, { createContext, useContext, useEffect, useState } from 'react';
import { IntlProvider } from 'react-intl';
import LOCALES, { AVAILABLE_LANGUAGES, DEFAULT_LANGUAGE } from '@app/locales';
import { LocalizedString } from '@app/types';
import { Literal } from 'n3';

interface CustomIntlProviderProps {
  children: React.ReactNode;
}

export interface SetLanguageInterface {
  language: string;
  setLanguage: (language: string) => void;
}

export const useLanguage = () => {
  const { language, setLanguage: setLanguageContext } = useContext(LanguageContext);

  const setLanguage = (newLanguage: string) => {
    setLanguageContext(newLanguage);
    persistLanguage(newLanguage);
  };

  return { language, setLanguage };
};

export const getPreferredLanguage = (): string => {
  const storedLanguage = localStorage.getItem('language');

  if (storedLanguage) return storedLanguage;

  for (const lang of window.navigator.languages) {
    const language = lang.split('-')[0];
    if (Object.keys(AVAILABLE_LANGUAGES).includes(language)) {
      return language;
    }
  }
  return 'sv';
};

export const persistLanguage = (language: string) => {
  if (Object.keys(AVAILABLE_LANGUAGES).includes(language)) {
    localStorage.setItem('language', language);
  }
};

export const LanguageContext = createContext<SetLanguageInterface>({ language: 'sv', setLanguage: () => {} });

export const CustomIntlProvider = ({ children }: CustomIntlProviderProps) => {
  const [language, setLanguage] = useState(DEFAULT_LANGUAGE);
  const value = { language, setLanguage };

  useEffect(() => {
    if (window) {
      setLanguage(getPreferredLanguage());
    }
  }, []);

  return (
    <LanguageContext.Provider value={value}>
      <IntlProvider locale={language} messages={LOCALES[language]}>
        {children}
      </IntlProvider>
    </LanguageContext.Provider>
  );
};

export const stringFromLocalizedString = (
  language: string,
  translations?: LocalizedString,
  defaultText?: string,
): string => {
  if (!translations) return defaultText ?? 'Unknown';

  if (!(translations['default'] || translations[DEFAULT_LANGUAGE])) {
    throw new Error('Missing default language for localized string.');
  }

  return translations[language] ?? translations['default'];
};

export const localizedStringFromLiterals = (literals: Literal[]): LocalizedString => {
  const translations: { [key: string]: string } = {};

  for (const s of literals) {
    translations[s.language === '' ? 'default' : s.language] = s.value;
  }

  return translations;
};
