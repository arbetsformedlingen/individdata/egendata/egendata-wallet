import { afterAll, afterEach, beforeAll, describe, expect, it } from 'vitest';
import { changeToFetching, changeToSharing } from './requestStateChanger';
import { SetupServerApi, setupServer } from 'msw/node';
import { MockedRequest, matchRequestUrl, rest } from 'msw';
import { env } from '@app/env.mjs';

import { handlers as requestHandlers, urls as requestUrls } from '../../tests/mocks/requests';
import { handlers as agentHandlers, urls as agentUrls } from '../../tests/mocks/agents';

// https://mswjs.io/docs/extensions/life-cycle-events#asserting-request-payload
function innerWaitForRequest(server: SetupServerApi, method: string, url: string) {
  let requestId = '';

  return new Promise<MockedRequest>((resolve, reject) => {
    server.events.on('request:start', (req) => {
      const matchesMethod = req.method.toLowerCase() === method.toLowerCase();
      const matchesUrl = matchRequestUrl(req.url, url).matches;

      if (matchesMethod && matchesUrl) requestId = req.id;
    });

    server.events.on('request:match', (req) => {
      if (req.id === requestId) resolve(req);
    });

    server.events.on('request:unhandled', (req) => {
      if (req.id === requestId) {
        reject(new Error(`The ${req.method} ${req.url.href} request was unhandled.`));
      }
    });
  });
}

describe('changeToFetching', () => {
  let server: SetupServerApi;

  beforeAll(() => {
    server = setupServer(...agentHandlers, ...requestHandlers);
    server.listen();
  });
  afterAll(() => server.close());
  afterEach(() => server.resetHandlers());

  const waitForRequest = (method: string, url: string) => {
    return innerWaitForRequest(server, method, url);
  };

  it('should create correct resources', async () => {
    const webId = agentUrls.PERSON_WEBID;
    const seeAlso = agentUrls.PERSON_PRIVATE;
    const requestURL = new URL(requestUrls.RECEIVED);

    const handlers = [
      rest.put(`${requestUrls.BASE}egendata/requests/:request/fetch`, (req, res, ctx) => {
        return res(ctx.status(201));
      }),
      rest.put(`${requestUrls.BASE}egendata/requests/:request/fetch.acl`, (req, res, ctx) => {
        return res(ctx.status(201));
      }),
      rest.put(`${requestUrls.BASE}egendata/requests/:request/data`, (req, res, ctx) => {
        return res(ctx.status(201));
      }),
      rest.put(`${requestUrls.BASE}egendata/requests/:request/data.acl`, (req, res, ctx) => {
        return res(ctx.status(201));
      }),
      rest.put(`${env.POD_BASE_URL}template-test1-source/egendata/inbox/:uuid`, (req, res, ctx) => {
        return res(ctx.status(201));
      }),
    ];

    server.use(...handlers);

    // Set up request listeners but don't resolve them yet.
    const pendingFetchRequest = waitForRequest('PUT', `${requestUrls.BASE}egendata/requests/:uuid/fetch`);
    const pendingFetchACLRequest = waitForRequest('PUT', new URL('fetch.acl', requestURL).toString());
    const pendingDataRequest = waitForRequest('PUT', new URL('data', requestURL).toString());
    const pendingDataACLRequest = waitForRequest('PUT', new URL('data.acl', requestURL).toString());
    const pendingProviderNotificationRequest = waitForRequest(
      'PUT',
      `${env.POD_BASE_URL}template-test1-source/egendata/inbox/:uuid`,
    );

    // Act
    await changeToFetching(webId, seeAlso, requestURL, fetch);

    // Await the requests
    const fetchRequest = await pendingFetchRequest;
    const fetchACLRequest = await pendingFetchACLRequest;
    const dataRequest = await pendingDataRequest;
    const dataACLRequest = await pendingDataACLRequest;
    const providerNotificationRequest = await pendingProviderNotificationRequest;

    expect(fetchRequest.headers.get('content-type')).toBe('text/turtle');
    expect(fetchACLRequest.headers.get('content-type')).toBe('text/turtle');
    expect(dataRequest.headers.get('content-type')).toBe('text/turtle');
    expect(dataACLRequest.headers.get('content-type')).toBe('text/turtle');
    expect(providerNotificationRequest.headers.get('content-type')).toBe('text/turtle');

    const fetchRequestText = await fetchRequest.text();
    expect(fetchRequestText).toContain('@prefix egendata: <https://egendata.se/schema/core/v1#> .');
    expect(fetchRequestText).toContain('<> a egendata:OutboundDataRequest ;');

    const fetchACLRequestText = await fetchACLRequest.text();
    expect(fetchACLRequestText).toContain('@prefix acl: <http://www.w3.org/ns/auth/acl#> .');
    // Permissions for owner
    expect(fetchACLRequestText).toContain(
      `<#owner> a acl:Authorization ;\n  acl:accessTo <${env.POD_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/egendata/requests/048ff7da-f622-44a7-96f5-e68abf7876e3/fetch> ;\n  acl:agent <${env.IDP_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/profile/card#me> ;\n  acl:mode acl:Control, acl:Write, acl:Append, acl:Read .`,
    );
    // Permissions for provider
    expect(fetchACLRequestText).toContain(
      `<#provider> a acl:Authorization ;\n  acl:accessTo <${env.POD_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/egendata/requests/048ff7da-f622-44a7-96f5-e68abf7876e3/fetch> ;\n  acl:agent <${env.IDP_BASE_URL}template-test1-source/profile/card#me> ;\n  acl:mode acl:Read .`,
    );

    const dataACLRequestText = await dataACLRequest.text();
    expect(dataACLRequestText).toContain('@prefix acl: <http://www.w3.org/ns/auth/acl#> .');
    // Permissions for owner
    expect(dataACLRequestText).toContain(
      `<#owner> a acl:Authorization ;\n  acl:accessTo <${env.POD_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/egendata/requests/048ff7da-f622-44a7-96f5-e68abf7876e3/data> ;\n  acl:agent <${env.IDP_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/profile/card#me> ;\n  acl:mode acl:Control, acl:Write, acl:Append, acl:Read .`,
    );
    // Permissions for provider
    expect(dataACLRequestText).toContain(
      `<#provider> a acl:Authorization ;\n  acl:accessTo <${env.POD_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/egendata/requests/048ff7da-f622-44a7-96f5-e68abf7876e3/data> ;\n  acl:agent <${env.IDP_BASE_URL}template-test1-source/profile/card#me> ;\n  acl:mode acl:Write, acl:Append .`,
    );

    const providerNotificationRequestText = await providerNotificationRequest.text();
    expect(providerNotificationRequestText).toContain('@prefix egendata: <https://egendata.se/schema/core/v1#> .');
    expect(providerNotificationRequestText).toContain('<> a egendata:Notification ;');
    expect(providerNotificationRequestText).toContain(
      `egendata:refersTo <${env.POD_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/egendata/requests/048ff7da-f622-44a7-96f5-e68abf7876e3/fetch>`,
    );
  });
});

describe('changeToSharing', () => {
  let server: SetupServerApi;

  beforeAll(() => {
    server = setupServer(...agentHandlers, ...requestHandlers);
    server.listen();
  });
  afterAll(() => server.close());
  afterEach(() => server.resetHandlers());

  const waitForRequest = (method: string, url: string) => {
    return innerWaitForRequest(server, method, url);
  };
  it('should create correct resources', async () => {
    const webId = agentUrls.PERSON_WEBID;
    const seeAlso = agentUrls.PERSON_PRIVATE;
    const requestURL = new URL(requestUrls.AVAILABLE);

    const handlers = [
      rest.put(`${requestUrls.BASE}egendata/requests/:request/consent`, (req, res, ctx) => {
        return res(ctx.status(201));
      }),
      rest.put(`${requestUrls.BASE}egendata/requests/:request/fetch.acl`, (req, res, ctx) => {
        return res(ctx.status(201));
      }),
      rest.put(`${requestUrls.BASE}egendata/requests/:request/data.acl`, (req, res, ctx) => {
        return res(ctx.status(201));
      }),
      rest.put(`${env.POD_BASE_URL}template-test1-sink/egendata/inbox/:uuid`, (req, res, ctx) => {
        return res(ctx.status(201));
      }),
    ];

    server.use(...handlers);

    // Set up request listeners but don't resolve them yet.
    const pendingConsentRequest = waitForRequest('PUT', new URL('consent', requestURL).toString());
    const pendingFetchACLRequest = waitForRequest('PUT', new URL('fetch.acl', requestURL).toString());
    const pendingDataACLRequest = waitForRequest('PUT', new URL('data.acl', requestURL).toString());
    const pendingRequestorNotificationRequest = waitForRequest(
      'PUT',
      `${env.POD_BASE_URL}template-test1-sink/egendata/inbox/:uuid`,
    );

    // Act
    await changeToSharing(webId, requestURL, fetch);

    const consentRequest = await pendingConsentRequest;
    const fetchACLRequest = await pendingFetchACLRequest;
    const dataACLRequest = await pendingDataACLRequest;
    const requestorNotificationRequest = await pendingRequestorNotificationRequest;

    const consentRequestText = await consentRequest.text();
    expect(consentRequestText).toContain(`@prefix egendata: <https://egendata.se/schema/core/v1#> .`);
    expect(consentRequestText).toContain(`<> a egendata:ConsumerConsent ;`);

    const fetchACLRequestText = await fetchACLRequest.text();
    // Permissions for owner
    expect(fetchACLRequestText).toContain(
      `<#owner> a acl:Authorization ;\n  acl:accessTo <${env.POD_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/egendata/requests/f6616ed8-3ff8-4050-a654-32a8b39bd40a/fetch> ;\n  acl:agent <${env.IDP_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/profile/card#me> ;\n  acl:mode acl:Control, acl:Write, acl:Append, acl:Read .`,
    );
    // Permissions for provider
    expect(fetchACLRequestText).not.toContain(`<#provider> a acl:Authorization ;`);

    const dataACLRequestText = await dataACLRequest.text();
    expect(dataACLRequestText).toContain(`@prefix acl: <http://www.w3.org/ns/auth/acl#> .`);
    // Permissions for owner
    expect(dataACLRequestText).toContain(
      `<#owner> a acl:Authorization ;\n  acl:accessTo <${env.POD_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/egendata/requests/f6616ed8-3ff8-4050-a654-32a8b39bd40a/data> ;\n  acl:agent <${env.IDP_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/profile/card#me> ;\n  acl:mode acl:Control, acl:Write, acl:Append, acl:Read .`,
    );
    // Permissions for provider
    expect(dataACLRequestText).toContain(
      `<#requestor> a acl:Authorization ;\n  acl:accessTo <${env.POD_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/egendata/requests/f6616ed8-3ff8-4050-a654-32a8b39bd40a/data> ;\n  acl:agent <${env.IDP_BASE_URL}template-test1-sink/profile/card#me> ;\n  acl:mode acl:Read .`,
    );

    const requestorNotificationRequestText = await requestorNotificationRequest.text();
    expect(requestorNotificationRequestText).toContain(`@prefix egendata: <https://egendata.se/schema/core/v1#> .`);
    expect(requestorNotificationRequestText).toContain(`<> a egendata:Notification ;`);
    expect(requestorNotificationRequestText).toContain(
      `egendata:refersTo <${env.POD_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/egendata/requests/f6616ed8-3ff8-4050-a654-32a8b39bd40a/data>`,
    );
  });
});
