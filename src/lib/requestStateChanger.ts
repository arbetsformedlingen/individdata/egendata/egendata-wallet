import { env } from '@app/env.mjs';
import { Thing, getDatetime, getSolidDataset, getStringNoLocale, getThing, getUrl } from '@inrupt/solid-client';
import { FetchInterface } from './fetchFactory';
import { aclURL, turtleACL, turtleNotification, turtleProviderRequest, turtleRequestorConsent } from './solid';
import { v4 as uuid } from 'uuid';
import { requestFromURL } from '@app/lib/solid';

function processRequest(thing: Thing) {
  return {
    url: thing.url,
    type: getUrl(thing, 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type') ?? '',
    documentTitle: getStringNoLocale(thing, `${env.EGENDATA_SCHEMA_URL}documentTitle`),
    documentType: getStringNoLocale(thing, `${env.EGENDATA_SCHEMA_URL}documentType`),
    id: getStringNoLocale(thing, `${env.EGENDATA_SCHEMA_URL}id`),
    providerWebId: getStringNoLocale(thing, `${env.EGENDATA_SCHEMA_URL}providerWebId`),
    purpose: getStringNoLocale(thing, `${env.EGENDATA_SCHEMA_URL}purpose`),
    requestorWebId: getStringNoLocale(thing, `${env.EGENDATA_SCHEMA_URL}requestorWebId`),
    returnUrl: getStringNoLocale(thing, `${env.EGENDATA_SCHEMA_URL}returnUrl`),
    created: getDatetime(thing, 'http://purl.org/dc/terms/created'),
  };
}

export const changeToFetching = async (webId: string, seeAlso: string, requestURL: URL, fetch: FetchInterface) => {
  let ds;

  const requestWithDetails = await requestFromURL(requestURL, { fetch });
  const { uuid: requestId, documentType, providerWebId } = requestWithDetails;

  ds = await getSolidDataset(seeAlso, { fetch });
  const profileThing = getThing(ds, seeAlso) as Thing;
  const ssn = getStringNoLocale(profileThing, `${env.EGENDATA_SCHEMA_URL}dataSubjectIdentifier`) as string;

  const providerStorageURL =
    getUrl(
      getThing(await getSolidDataset(providerWebId.toString(), { fetch }), providerWebId.toString()) as Thing,
      'http://www.w3.org/ns/pim/space#storage',
    ) ?? '';

  const fetchURL = new URL(`fetch`, requestURL);
  const dataLocation = new URL(`data`, requestURL);
  const inboxLocation = new URL(`../../inbox/`, requestURL);

  // Write provider request
  const providerRequest = turtleProviderRequest({
    id: requestId,
    dataSubjectIdentifier: ssn,
    dataLocation: dataLocation.toString(),
    notificationInbox: inboxLocation.toString(),
    documentType: documentType,
    date: new Date(),
  });
  await fetch(fetchURL.toString(), {
    method: 'PUT',
    body: providerRequest,
    headers: { 'Content-Type': 'text/turtle' },
  });

  // Write ACL for fetchURL
  const providerRequestACL = turtleACL(fetchURL.toString(), [
    { label: 'owner', agent: webId, mode: ['Control', 'Write', 'Append', 'Read'] },
    { label: 'provider', agent: providerWebId.toString(), mode: ['Read'] },
  ]);
  await fetch(aclURL(fetchURL).toString(), {
    method: 'PUT',
    body: providerRequestACL,
    headers: { 'Content-Type': 'text/turtle' },
  });
  const dataURL = new URL(`data`, requestURL);

  // Write data resource
  await fetch(dataURL.toString(), {
    method: 'PUT',
    body: '',
    headers: { 'Content-Type': 'text/turtle' },
  });

  // Write ACL for data resource
  const dataACL = turtleACL(dataURL.toString(), [
    { label: 'owner', agent: webId, mode: ['Control', 'Write', 'Append', 'Read'] },
    { label: 'provider', agent: providerWebId.toString(), mode: ['Write', 'Append'] },
  ]);
  await fetch(aclURL(dataURL).toString(), {
    method: 'PUT',
    body: dataACL,
    headers: { 'Content-Type': 'text/turtle' },
  });

  // TODO: Change to newly generated UUID instead of providerRequestId.
  const notificationURL = new URL(`${env.EGENDATA_PATH_FRAGMENT}inbox/${requestId}`, providerStorageURL);

  // Write notification
  const notification = turtleNotification({ url: fetchURL.toString(), type: 'OutboundDataRequest' });
  const result = await fetch(notificationURL.toString(), {
    method: 'PUT',
    body: notification,
    headers: { 'Content-Type': 'text/turtle' },
  });
};

export const changeToSharing = async (webId: string, requestURL: URL, fetch: FetchInterface) => {
  let ds, acl;

  const requestWithDetails = await requestFromURL(requestURL, { fetch });
  const { uuid: requestId, requestorWebId } = requestWithDetails;

  // const baseURL = new URL('../../', requestURL);
  // const providerRequestId = requestURL.pathname.split('/').pop() ?? '';

  // // Fetch basic data from provider request
  // const requestThing = getThing(
  //   await getSolidDataset(requestURL.toString(), { fetch }),
  //   requestURL.toString(),
  // ) as Thing;
  // const requestorWebId = getStringNoLocale(requestThing, `${env.EGENDATA_SCHEMA_URL}requestorWebId`) ?? '';

  const dataLocation = new URL(`data`, requestURL);
  const requestorConsentURL = new URL(`consent`, requestURL);

  // Write requestor consent
  const requestorConsent = turtleRequestorConsent({
    consentDocument: 'Consent text.',
    requestorWebId: requestorWebId.toString(),
    requestId: requestId,
    sharedData: dataLocation.toString(),
    date: new Date(),
  });
  await fetch(requestorConsentURL.toString(), {
    method: 'PUT',
    body: requestorConsent,
    headers: { 'Content-Type': 'text/turtle' },
  });

  // Update ACL for data resource
  const updatedDataACL = turtleACL(dataLocation.toString(), [
    { label: 'owner', agent: webId, mode: ['Control', 'Write', 'Append', 'Read'] },
    { label: 'requestor', agent: requestorWebId.toString(), mode: ['Read'] },
  ]);
  await fetch(aclURL(dataLocation).toString(), {
    method: 'PUT',
    body: updatedDataACL,
    headers: { 'Content-Type': 'text/turtle' },
  });

  const fetchLocation = new URL(`fetch`, requestURL);
  // Update ACL for fetch resource
  const updatedFetchACL = turtleACL(fetchLocation.toString(), [
    { label: 'owner', agent: webId, mode: ['Control', 'Write', 'Append', 'Read'] },
  ]);
  await fetch(aclURL(fetchLocation).toString(), {
    method: 'PUT',
    body: updatedFetchACL,
    headers: { 'Content-Type': 'text/turtle' },
  });

  // Write notification
  const requestorStorageURL =
    getUrl(
      getThing(await getSolidDataset(requestorWebId.toString(), { fetch }), requestorWebId.toString()) as Thing,
      'http://www.w3.org/ns/pim/space#storage',
    ) ?? '';

  if (requestorStorageURL) {
    const notificationURL = new URL(`${env.EGENDATA_PATH_FRAGMENT}inbox/${uuid()}`, requestorStorageURL);

    const notification = turtleNotification({
      url: dataLocation.toString(),
      type: 'OutboundDataResponse',
    });
    const result = await fetch(notificationURL.toString(), {
      method: 'PUT',
      body: notification,
      headers: { 'Content-Type': 'text/turtle' },
    });

    // console.log(`Put notification in ${notificationURL.toString()}.`);
  }
};
