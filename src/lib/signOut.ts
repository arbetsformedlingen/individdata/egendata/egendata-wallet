import { signOut as nextAuthSignOut } from 'next-auth/react';

const signOut = async (idToken: string) => {
  const params = new URLSearchParams({ id_token: idToken });

  nextAuthSignOut({ callbackUrl: `/api/fedlogout?${params.toString()}` });
};

export default signOut;
