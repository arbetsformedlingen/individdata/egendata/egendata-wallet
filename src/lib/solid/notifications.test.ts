import { afterAll, afterEach, beforeAll, describe, expect, it, vi } from 'vitest';
import { SetupServerApi, setupServer } from 'msw/node';
import { getSubscriptionEndpoint, listAllNotifications, processNotification, subscribe } from './notifications';
import { env } from '@app/env.mjs';

import { handlers } from '@tests/mocks/misc';
import { rest } from 'msw';

describe('subscribe', () => {
  let server: SetupServerApi;

  beforeAll(() => {
    server = setupServer(...handlers);
    server.listen({ onUnhandledRequest: 'error' });
  });

  afterAll(() => {
    server.close();
  });

  afterEach(() => {
    server.resetHandlers();
  });

  it('should return subscription url', async () => {
    const url = `${env.POD_BASE_URL}50b989cc-3f23-4687-9326-f7a1f95812f9/egendata/inbox/`;
    const response = await subscribe(url, { fetcher: fetch });
    expect(response).toBe(
      `${env.POD_BASE_URL}.notifications/WebSocketChannel2023/6ffc2e6f-2e86-4510-a4a6-df4e24955b7c`,
    );
  });
});

describe('getSubscriptionEndpoint', () => {
  let server: SetupServerApi;

  beforeAll(() => {
    server = setupServer(...handlers);
    server.listen({ onUnhandledRequest: 'error' });
  });

  afterAll(() => {
    server.close();
  });

  afterEach(() => {
    server.resetHandlers();
  });

  it('should return the endpoint url for server string', async () => {
    const endpoint = await getSubscriptionEndpoint(env.POD_BASE_URL);

    expect(endpoint).toBe(`${env.POD_BASE_URL}.notifications/WebSocketChannel2023/`);
  });

  it('should return the endpoint url for server url', async () => {
    const endpoint = await getSubscriptionEndpoint(env.POD_BASE_URL);

    expect(endpoint).toBe(`${env.POD_BASE_URL}.notifications/WebSocketChannel2023/`);
  });

  it('should return the endpoint url for resource url', async () => {
    const server = new URL(`${env.POD_BASE_URL}50b989cc-3f23-4687-9326-f7a1f95812f9/egendata/inbox/`);
    const endpoint = await getSubscriptionEndpoint(server);

    expect(endpoint).toBe(`${env.POD_BASE_URL}.notifications/WebSocketChannel2023/`);
  });
});

describe('listAllNotifications', () => {
  let server: SetupServerApi;

  beforeAll(() => {
    server = setupServer(...handlers);
    server.listen({ onUnhandledRequest: 'error' });
  });

  afterAll(() => {
    server.close();
  });

  afterEach(() => {
    server.resetHandlers();
  });

  it('should return a list of notification URLs', async () => {
    const inboxURL = new URL(`${env.POD_BASE_URL}934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/inbox/`);

    const notifications = await listAllNotifications(inboxURL);
    expect(notifications).toHaveLength(3);

    const strings = notifications.map((url) => url.toString());

    expect(
      strings.includes(
        'https://pod.localhost/934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/inbox/557442fb-1d9e-4d47-b1f1-15ab51f47a06',
      ),
    ).toBeTruthy();
    expect(
      strings.includes(
        'https://pod.localhost/934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/inbox/78827cc7-fd3c-4a9d-9f46-d0187a65157f',
      ),
    ).toBeTruthy();
    expect(
      strings.includes(
        'https://pod.localhost/934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/inbox/3388295f-356b-4182-af27-c950ed2244f8',
      ),
    ).toBeTruthy();
  });
});

describe('processNotification', () => {
  let server: SetupServerApi;

  beforeAll(() => {
    server = setupServer(...handlers);
    server.listen({ onUnhandledRequest: 'error' });
  });

  afterAll(() => {
    server.close();
  });

  afterEach(() => {
    server.resetHandlers();
  });

  it('should throw if notification not there', async () => {
    const notificationURL = new URL(
      `${env.POD_BASE_URL}934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/inbox/c91099f6-f4d0-41ed-8818-85f71e75f51e`,
    );

    server.use(
      rest.get(notificationURL.toString(), (req, res, ctx) => {
        return res(ctx.status(404));
      }),
    );

    await expect(processNotification(notificationURL)).rejects.toThrowError('Failed to fetch notification.');
  });

  it('should handle link messages', async () => {
    const notificationURL = new URL(
      `${env.POD_BASE_URL}934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/inbox/4c824b1a-8b92-4a1d-a29c-893cb70433f7`,
    );

    const requestURL = new URL(
      `${env.POD_BASE_URL}934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/requests/7f117ebe-c6b8-43bf-b404-3881adf310fd/error`,
    );

    server.use(
      rest.delete(notificationURL.toString(), (req, res, ctx) => {
        return res(ctx.status(200));
      }),
      rest.delete(new URL('fetch.acl', requestURL).toString(), (req, res, ctx) => {
        return res(ctx.status(200));
      }),
      rest.delete(new URL('data.acl', requestURL).toString(), (req, res, ctx) => {
        return res(ctx.status(200));
      }),
    );

    await expect(processNotification(notificationURL)).resolves.not.toThrow();
  });

  it('should handle error messages', async () => {
    const notificationURL = new URL(
      `${env.POD_BASE_URL}934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/inbox/dae5a2d8-4b05-4a86-8767-72a992fa737d`,
    );
    const requestURL = new URL(
      `${env.POD_BASE_URL}934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/requests/7f117ebe-c6b8-43bf-b404-3881adf310fd/`,
    );

    server.use(
      rest.put(new URL('error', requestURL).toString(), (req, res, ctx) => {
        return res(ctx.status(201));
      }),
      rest.delete(notificationURL.toString(), (req, res, ctx) => {
        return res(ctx.status(200));
      }),
      rest.delete(new URL('fetch.acl', requestURL).toString(), (req, res, ctx) => {
        return res(ctx.status(200));
      }),
      rest.delete(new URL('data.acl', requestURL).toString(), (req, res, ctx) => {
        return res(ctx.status(200));
      }),
    );

    await expect(processNotification(notificationURL)).resolves.not.toThrow();
  });
});
