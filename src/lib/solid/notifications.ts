import N3 from 'n3';
import { env } from '@app/env.mjs';

import { logger as baseLogger } from '@app/lib/logger';
import { turtleErrorRequest } from './templates';

const logger = baseLogger.child({ module: 'notifications' });

// https://solidproject.org/TR/notifications-protocol#notification-channel
// https://solid.github.io/notifications/websocket-channel-2023
// https://solid.github.io/notifications/webhook-channel-2023

export const subscribe = async (topic: string | URL, fetcher = { fetcher: fetch }) => {
  const endpoint = await getSubscriptionEndpoint(topic, fetcher);
  const subscriptionId = await getWebsocketSubscriptionUrl(endpoint, topic.toString(), fetcher);
  return subscriptionId;
};

export const getSubscriptionEndpoint = async (resource: string | URL, { fetcher } = { fetcher: fetch }) => {
  const { origin } = new URL(resource);
  const solidUrl = new URL('/.well-known/solid', origin);

  const solid = await fetcher(solidUrl, {
    headers: {
      Accept: 'text/turtle',
    },
  });

  if (!solid.ok) {
    const msg = 'Failed to read metadata .well-known/solid';
    logger.info(solid, msg);
    throw new Error('Failed to read metadata .well-known/solid');
  }

  const text = await solid.text();

  const subscriptionUrl = getSubscriptionUrl(text);
  if (!subscriptionUrl) {
    throw new Error('No subscriptionUrl found!');
  }
  logger.debug(subscriptionUrl, 'subscriptionUrl: ');
  return subscriptionUrl;
};

const getSubscriptionUrl = (subscriptionEndpoints: any) => {
  const N3Parser = new N3.Parser();
  const store = new N3.Store(N3Parser.parse(subscriptionEndpoints));
  const obj = store.getSubjects(
    'http://www.w3.org/ns/solid/notifications#channelType',
    'http://www.w3.org/ns/solid/notifications#WebSocketChannel2023',
    null,
  );
  const subscriptionUrl = obj ? (obj[0] ? obj[0].value : undefined) : undefined;
  logger.trace('obj: %o', subscriptionUrl);
  return subscriptionUrl;
};

const getWebsocketSubscriptionUrl = async (
  subscriptionEndpoint: string,
  topic: string,
  { fetcher } = { fetcher: fetch },
) => {
  const data = {
    '@context': ['https://www.w3.org/ns/solid/notification/v1'],
    type: 'http://www.w3.org/ns/solid/notifications#WebSocketChannel2023',
    topic,
  };
  const body = JSON.stringify(data);
  const result = await fetcher(subscriptionEndpoint, {
    method: 'POST',
    body: body,
    headers: { 'Content-Type': 'application/ld+json' },
  });
  if (!result.ok) {
    throw new Error('Subscription failed');
  }
  const json = await result.json();
  const websocketSubscriptionUrl = json.receiveFrom;
  logger.debug(websocketSubscriptionUrl, 'websocketSubscriptionUrl: ');
  return websocketSubscriptionUrl;
};

export const listAllNotifications = async (url: URL, { fetcher } = { fetcher: fetch }): Promise<URL[]> => {
  const response = await fetcher(url, {
    headers: {
      Accept: 'text/turtle',
    },
  });

  if (!response.ok) {
    const msg = 'Failed to list contents of inbox.';
    logger.info({ url, status: response.status, statusText: response.statusText }, msg);
    throw new Error(msg);
  }

  try {
    const N3Parser = new N3.Parser();
    const store = new N3.Store(N3Parser.parse(await response.text()));

    const notifications = store
      .getObjects(null, 'http://www.w3.org/ns/ldp#contains', null)
      .map((node) => new URL(node.value, url));

    return notifications;
  } catch (error) {
    const msg = 'Failed to parse contents of inbox.';
    logger.info({ error, text: await response.text() }, msg);
    throw new Error(msg);
  }
};

export const processNotification = async (url: URL, { fetcher } = { fetcher: fetch }) => {
  logger.debug({ url }, 'Starting processing of notification.');
  const response = await fetcher(url, {
    headers: {
      Accept: 'text/turtle',
    },
  });

  if (!response.ok) {
    const msg = 'Failed to fetch notification.';
    logger.info({ url, status: response.status, statusText: response.statusText }, msg);
    throw new Error(msg);
  }

  const N3Parser = new N3.Parser();
  const store = new N3.Store(N3Parser.parse(await response.text()));

  // What kind of notification?
  const type = store.getObjects(null, 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', null)[0].value;

  logger.debug({ url, type }, `Notification is for type ${type}`);

  if (type === `${env.EGENDATA_SCHEMA_URL}InboundDataResponseLink`)
    return processInboundDataResponseLink(url, store, { fetcher });

  if (type === `${env.EGENDATA_SCHEMA_URL}InboundDataResponseError`)
    return processInboundDataResponseError(url, store, { fetcher });

  const msg = 'Unknown notification type received.';
  logger.info({ url: url }, msg);
  throw new Error(msg);
};

const processInboundDataResponseLink = async (url: URL, store: N3.Store, { fetcher }: { fetcher: typeof fetch }) => {
  logger.debug({ url }, 'Starting processing of InboundDataResponseLink.');
  const dataLocation = store.getObjects(null, `${env.EGENDATA_SCHEMA_URL}InboundDataResponse`, null)[0].value;

  const requestURL = new URL('.', dataLocation);

  await removeFetchPermissions(requestURL, { fetcher });
  await removeDataPermissions(requestURL, { fetcher });
  await removeNotificationResource(url, { fetcher });
};

const processInboundDataResponseError = async (url: URL, store: N3.Store, { fetcher }: { fetcher: typeof fetch }) => {
  let response;
  const id = store.getObjects(null, `${env.EGENDATA_SCHEMA_URL}requestId`, null)[0].value;
  const fetchLocation = store.getObjects(null, `${env.EGENDATA_SCHEMA_URL}fetchLocation`, null)[0].value;
  const errorId = store.getObjects(null, `${env.EGENDATA_SCHEMA_URL}errorId`, null)[0].value;
  const errorMessage = store.getObjects(null, `${env.EGENDATA_SCHEMA_URL}errorMessage`, null)[0].value;

  logger.info({ id, fetchLocation, errorId }, errorMessage);

  const requestURL = new URL('.', fetchLocation);

  const errorLocation = new URL('error', requestURL);

  const body = turtleErrorRequest({ errorId, errorMessage, timestamp: new Date() });
  response = await fetcher(errorLocation, {
    method: 'PUT',
    body: body,
    headers: { 'Content-Type': 'text/turtle' },
  });

  if (!response.ok) {
    const msg = 'Failed to persist error message.';
    logger.info({ url: errorLocation, status: response.status, statusText: response.statusText }, msg);
    throw new Error(msg);
  }

  await removeFetchPermissions(requestURL, { fetcher });
  await removeDataPermissions(requestURL, { fetcher });
  await removeNotificationResource(url, { fetcher });
};

const removeFetchPermissions = async (requestURL: URL, { fetcher }: { fetcher: typeof fetch }) => {
  const fetchACLURL = new URL('fetch.acl', requestURL);
  const response = await fetcher(fetchACLURL, {
    method: 'DELETE',
  });

  if (!response.ok) {
    const msg = 'Failed to revoke permissions on fetch resource.';
    logger.info({ url: fetchACLURL, status: response.status, statusText: response.statusText }, msg);
  }
};

const removeDataPermissions = async (requestURL: URL, { fetcher }: { fetcher: typeof fetch }) => {
  const dataACLURL = new URL('data.acl', requestURL);
  const response = await fetcher(dataACLURL, {
    method: 'DELETE',
  });

  if (!response.ok) {
    const msg = 'Failed to revoke permissions on data resource.';
    logger.info({ url: dataACLURL, status: response.status, statusText: response.statusText }, msg);
  }
};

const removeNotificationResource = async (url: URL, { fetcher }: { fetcher: typeof fetch }) => {
  const response = await fetcher(url, {
    method: 'DELETE',
  });

  if (!response.ok) {
    const msg = 'Failed to remove processed notification.';
    logger.warn({ url, status: response.status, statusText: response.statusText }, msg);
    throw new Error(msg);
  }
};
