import { describe, expect, it, vi } from 'vitest';

import { verifyUserPodStructure } from './pod';
import { turtleEgendataACL } from './templates';

describe('verifyUserPodStructure', () => {
  it('does nothing when pod initialized earlier', async () => {
    vi.mock(
      '../../env.mjs',
      vi.fn(() => ({
        env: vi.fn(() => ({ NODE_ENV: 'development' })),
      })),
    );

    // Gobble up all debug log messages.
    vi.mock(
      '../logger',
      vi.fn(() => ({ default: { debug: vi.fn() } })),
    );

    const webid = 'https://identity-provider.localhost/e401cb59-a516-4a57-9c43-6edaf58bdd92/profile/card#me';
    const storage = 'https://pod-provider.localhost/e401cb59-a516-4a57-9c43-6edaf58bdd92/';

    const mockFetch = vi.fn();
    mockFetch.mockImplementationOnce(() => ({
      text: vi.fn(() => turtleEgendataACL({ id: webid, storage })),
      ok: true,
      status: 200,
    }));

    await verifyUserPodStructure({
      webid,
      storage,
      fetcher: mockFetch,
    });

    expect(mockFetch).toBeCalledTimes(1);
    expect(mockFetch).toBeCalledWith(expect.any(URL), expect.objectContaining({ method: 'GET' }));
  });

  it('creates resources when missing', async () => {
    const webid = 'https://identity-provider.localhost/e401cb59-a516-4a57-9c43-6edaf58bdd92/profile/card#me';
    const storage = 'https://pod-provider.localhost/e401cb59-a516-4a57-9c43-6edaf58bdd92/';

    const mockFetch = vi.fn();
    mockFetch.mockResolvedValueOnce({
      text: vi.fn(() => 'some garbage'),
      ok: true,
      status: 200,
    });

    mockFetch.mockResolvedValueOnce({
      ok: true,
    });

    await verifyUserPodStructure({
      webid,
      storage,
      fetcher: mockFetch,
    });

    expect(mockFetch).toBeCalledTimes(2);
    expect(mockFetch).toBeCalledWith(expect.any(URL), expect.objectContaining({ method: 'GET' }));
    expect(mockFetch).toBeCalledWith(expect.any(URL), expect.objectContaining({ method: 'PUT' }));
  });
});
