import { env } from '@app/env.mjs';
import { FetchInterface } from '../fetchFactory';
import logger from '../logger';
import { turtleEgendataACL } from './templates';

export type VerifyUserPodStructureProps = { webid: string; storage: string; fetcher: FetchInterface };

export const verifyUserPodStructure = async ({ webid, storage, fetcher }: VerifyUserPodStructureProps) => {
  if (env.NODE_ENV === 'test') return;

  const egendataURL = new URL(`${env.EGENDATA_PATH_FRAGMENT}inbox/`, storage);

  const check = await fetcher(new URL('.acl', egendataURL), { method: 'GET' });
  const currentACL = await check.text();

  if (!check.ok && check.status !== 404) throw Error('Could not verify user pod structure.');

  const preferredACL = turtleEgendataACL({ id: webid, storage: storage });

  if (currentACL !== preferredACL) {
    logger.debug('Issue detected with user pod structure, attempting to fix...');

    const response = await fetcher(new URL('.acl', egendataURL), {
      method: 'PUT',
      body: preferredACL,
      headers: { 'Content-Type': 'text/turtle' },
    });

    if (response.ok) {
      logger.debug('Created egendata structure in user pod.');
    } else {
      logger.warn({ status: response.status, statusText: response.statusText }, 'Failed to create user pod structure.');
      throw new Error('Something is wrong with the users pod.');
    }
  }
};
