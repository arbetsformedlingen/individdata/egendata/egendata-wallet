import { afterAll, afterEach, beforeAll, describe, expect, it } from 'vitest';
import { SetupServerApi, setupServer } from 'msw/node';
import { rest } from 'msw';
import { env } from '@app/env.mjs';

import { handlers as agentHandlers, urls } from '@tests/mocks/agents';

import {
  OrganizationProfileData,
  PersonProfileData,
  fetchBasicProfileData,
  fetchProfile,
  urlWithoutHash,
} from './profile';

describe('fetchBasicProfileData', () => {
  let server: SetupServerApi;

  beforeAll(() => {
    server = setupServer(...agentHandlers);
    server.listen({ onUnhandledRequest: 'error' });
  });

  afterAll(() => server.close());

  afterEach(() => server.resetHandlers());

  it('returns personal profile data', async () => {
    const profile = await fetchBasicProfileData(
      `${env.IDP_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/profile/card#me`,
    );

    expect(profile.storage).toBe(`${env.POD_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/`);
    expect(profile.seeAlso).toBe(`${env.IDP_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/profile/private`);
  });
});

describe('fetchProfileData', () => {
  let server: SetupServerApi;

  beforeAll(() => {
    server = setupServer(...agentHandlers);
    server.listen({ onUnhandledRequest: 'error' });
  });

  afterAll(() => server.close());

  afterEach(() => server.resetHandlers());

  it('should handle valid Personal Agent profile', async () => {
    const webid = `${env.IDP_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/profile/card#me`;
    server.use(
      rest.get(`${env.IDP_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/profile/private`, (req, res, ctx) => {
        res(ctx.status(404));
      }),
    );
    const profile = (await fetchProfile({ webid, fetcher: fetch })) as PersonProfileData;

    expect(profile.type).toBe('Person');
    expect(profile.firstName).toBe('Kalle');
    expect(profile.lastName).toBe('Anka');
  });

  it('should handle valid Organizational Agent profile', async () => {
    const webid = urls.ORG_PROVIDER_WEBID;
    const profile = (await fetchProfile({ webid, fetcher: fetch })) as OrganizationProfileData;

    expect(profile.type).toBe('Organization');
    expect(profile.name).toBe('TemplateTest1Source');
    expect(profile.logo!).toStrictEqual(
      new URL('https://identity-provider.localhost/template-test1-source/profile/logo'),
    );
  });

  it('should handle valid Organizational Agent profile without logo', async () => {
    const webid = urls.ORG_REQUESTOR_WEBID;
    const profile = (await fetchProfile({ webid, fetcher: fetch })) as OrganizationProfileData;

    expect(profile.type).toBe('Organization');
    expect(profile.name).toBe('TemplateTest1Sink');
    expect(profile.logo).toBeUndefined();
  });

  it('should throw on invalid webid', async () => {
    const webid = 'https://example.com/nobody';
    server.use(rest.get(webid, (req, res, ctx) => res(ctx.status(404))));

    const action = async () => {
      await fetchProfile({ webid, fetcher: fetch });
    };

    expect.assertions(1);
    await expect(action()).rejects.toThrow();
  });

  it('xx', async () => {
    const nonAgentUri = 'https://example.com/non-agent-resource';
    server.use(
      rest.get(nonAgentUri, (req, res, ctx) =>
        res(ctx.set('Content-Type', 'text/turtle'), ctx.body(`<x>\n  a <http://xmlns.com/foaf/0.1/SomethingElse>.`)),
      ),
    );

    expect.assertions(1);
    await expect(fetchProfile({ webid: nonAgentUri, fetcher: fetch })).rejects.toThrowError('Not a profile document');
  });

  it('should throw if not of type Personal Profile Document', async () => {
    const nonAgentUri = 'https://example.com/non-agent-resource';
    server.use(
      rest.get(nonAgentUri, (req, res, ctx) =>
        res(
          ctx.set('Content-Type', 'text/turtle'),
          ctx.body(`<>\n  <http://xmlns.com/foaf/0.1/Person>\n  <http://example.org/books/Huckleberry_Finn>.`),
        ),
      ),
    );

    expect.assertions(1);
    await expect(fetchProfile({ webid: nonAgentUri, fetcher: fetch })).rejects.toThrowError('Not a profile document');
  });

  it('should throw if unknown profile type', async () => {
    const webid = 'https://example.com/some/other/profile';

    expect.assertions(1);
    await expect(fetchProfile({ webid, fetcher: fetch })).rejects.toThrowError('Unknown profile type.');
  });
});

describe('urlWithoutHash', () => {
  it('should remove # part', () => {
    const url = new URL('https://example.com/some/path/with#fragment');

    expect(urlWithoutHash(url)).toStrictEqual(new URL('https://example.com/some/path/with'));
  });
});
