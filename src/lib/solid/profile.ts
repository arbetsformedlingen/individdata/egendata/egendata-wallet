import { type Thing, getSolidDataset, getStringNoLocale, getThing, getUrl, getUrlAll } from '@inrupt/solid-client';
import { FetchInterface } from '../fetchFactory';

export type FetchProfileProps = {
  webid: string;
  fetcher: FetchInterface;
};

export type PersonProfileData = {
  // Personal Agent
  type: 'Person';
  name: string;
  firstName: string;
  lastName: string;
};

export type OrganizationProfileData = {
  // Organizational Agent
  type: 'Organization';
  name: string;
  logo?: URL;
};

export type AgentProfileData = PersonProfileData | OrganizationProfileData;

export const urlWithoutHash = (url: URL): URL => {
  const noHash = new URL(url);
  noHash.hash = '';
  return noHash;
};

export async function fetchBasicProfileData(webId: string) {
  const profile = getThing(await getSolidDataset(webId), webId) as Thing;
  const storage = getUrl(profile, 'http://www.w3.org/ns/pim/space#storage') ?? '';
  const seeAlso = getUrl(profile, 'http://www.w3.org/2000/01/rdf-schema#seeAlso') ?? '';
  return { storage, seeAlso };
}

export const fetchProfile = async ({ webid, fetcher }: FetchProfileProps): Promise<AgentProfileData> => {
  const url = new URL(webid);
  const urlNoHash = urlWithoutHash(url);

  // Fetch basic data from provider request
  const ds = await getSolidDataset(urlNoHash.toString(), { fetch: fetcher });

  // Check to make sure it is a Personal Profile Document
  const thing = getThing(ds, urlNoHash.toString()) as Thing;
  if (!thing) throw new Error('Not a profile document');

  const type = getUrl(thing, 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type');
  if (type !== 'http://xmlns.com/foaf/0.1/PersonalProfileDocument') {
    throw new Error('Not a profile document');
  }

  // Check Profile type
  const profileThing = getThing(ds, url.toString()) as Thing;
  const agentTypes = getUrlAll(profileThing, 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type');

  if (agentTypes.includes('http://xmlns.com/foaf/0.1/Person')) {
    // Attempt to fetch and parse personal agent data
    const privateUrl = getUrl(profileThing, 'http://www.w3.org/2000/01/rdf-schema#seeAlso')!;
    const detailsDS = await getSolidDataset(privateUrl, { fetch: fetcher });
    const detailsThing = getThing(detailsDS, privateUrl) as Thing;

    const firstName = getStringNoLocale(detailsThing, 'http://xmlns.com/foaf/0.1/firstName');
    const lastName = getStringNoLocale(detailsThing, 'http://xmlns.com/foaf/0.1/lastName');

    if (firstName !== null && lastName !== null) {
      return {
        type: 'Person',
        name: `${firstName} ${lastName}`,
        firstName,
        lastName,
      };
    }
  }

  if (agentTypes.includes('http://xmlns.com/foaf/0.1/Organization')) {
    // Attempt to parse organisational agent data
    const name = getStringNoLocale(profileThing, 'http://xmlns.com/foaf/0.1/name');
    const logo = getUrl(profileThing, 'http://xmlns.com/foaf/0.1/logo');

    if (name !== null) {
      return {
        type: 'Organization',
        name,
        ...(logo ? { logo: new URL(logo) } : {}),
      };
    }
  }

  // Unknown profile type
  throw new Error('Unknown profile type.');
};
