import { afterAll, afterEach, beforeAll, describe, expect, it } from 'vitest';
import { requestFromURL, requestFromURLWithDetails } from './request';
import { rest } from 'msw';
import { SetupServerApi, setupServer } from 'msw/node';
import { env } from '@app/env.mjs';

import { handlers as requestHandlers, urls as requestURLs } from '@tests/mocks/requests';
import { handlers as agentHandlers, urls as agentURLs } from '@tests/mocks/agents';
import { handlers as templateHandlers } from '@tests/mocks/templateDocs';
import { handlers as egendataHandlers, urls as egendataURLs } from '@tests/mocks/egendata';

describe('requestFromURL', () => {
  let server: SetupServerApi;

  beforeAll(() => {
    server = setupServer(...requestHandlers);
    server.listen({ onUnhandledRequest: 'error' });
  });

  afterAll(() => {
    server.close();
  });

  afterEach(() => {
    server.resetHandlers();
  });

  it('handles request in `received` state', async () => {
    const url = new URL(requestURLs.RECEIVED);

    const requestInfo = await requestFromURL(url, { fetch });

    expect(requestInfo.state).toBe('received');
  });

  it('handles request in `fetching` state', async () => {
    const url = new URL(requestURLs.FETCHING);
    const requestInfo = await requestFromURL(url, { fetch });

    expect(requestInfo.state).toBe('fetching');
  });

  it('handles request in `available` state', async () => {
    const url = new URL(requestURLs.AVAILABLE);
    const requestInfo = await requestFromURL(url, { fetch });

    expect(requestInfo.state).toBe('available');
  });

  it('handles request in `shared` state', async () => {
    const url = new URL(requestURLs.SHARED);
    const requestInfo = await requestFromURL(url, { fetch });

    expect(requestInfo.state).toBe('shared');
  });

  it('should throw if request url does not end with `/`', async () => {
    const url = new URL('http://example.com/some/path');

    await expect(() => requestFromURL(url, { fetch })).rejects.toThrowError('URL has to end with `/`');
  });

  it('should throw if request id can not be found', async () => {
    const url = new URL(
      'ada94091-ad15-4712-a208-bfaeadefc4cd/',
      `${env.POD_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/egendata/requests/`,
    );

    server.use(
      rest.get(url.toString(), (req, res, ctx) => {
        return res(ctx.status(404));
      }),
    );
    await expect(() => requestFromURL(url, { fetch })).rejects.toThrowError('Failed to fetch request data');
  });

  it('should throw if /subject resouce is missing', async () => {
    const url = new URL('https://example.com/some/path/');

    server.use(
      rest.get(url.toString(), (req, res, ctx) =>
        res(
          ctx.body(
            `@prefix dc: <http://purl.org/dc/terms/>.\n@prefix ldp: <http://www.w3.org/ns/ldp#>.\n@prefix posix: <http://www.w3.org/ns/posix/stat#>.\n@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.\n<> a ldp:Container, ldp:BasicContainer, ldp:Resource;\n  dc:modified "2023-02-28T12:44:20.000Z"^^xsd:dateTime;\n  posix:mtime 1677588260.`,
          ),
          ctx.status(200),
          ctx.set('Content-Type', 'text/turtle'),
        ),
      ),
    );

    await expect(() => requestFromURL(url, { fetch })).rejects.toThrowError('Missing subject resource.');
  });
});

describe('requestFromURLWithDetails', () => {
  let server: SetupServerApi;

  beforeAll(() => {
    server = setupServer(...requestHandlers, ...agentHandlers, ...templateHandlers, ...egendataHandlers);
    server.listen({ onUnhandledRequest: 'error' });
  });

  afterAll(() => {
    server.close();
  });

  afterEach(() => {
    server.resetHandlers();
  });

  it.skip('should return some data', async () => {
    console.log(egendataURLs);

    const response = await requestFromURLWithDetails(new URL(requestURLs.RECEIVED), { fetch });

    expect(response.created).toBeDefined();
    expect(response.documentType).toBe('JobSeekerRegistrationStatus');
    expect(response.provider).toBeDefined();
    expect(response.requestor).toBeDefined();
    expect(response.purpose).toBe('Insurance claim');
  });
});
