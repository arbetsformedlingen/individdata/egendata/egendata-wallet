import { env } from '@app/env.mjs';
import {
  SolidDataset,
  Thing,
  WithResourceInfo,
  getContainedResourceUrlAll,
  getDatetime,
  getInteger,
  getSolidDataset,
  getStringNoLocale,
  getThing,
  getUrl,
} from '@inrupt/solid-client';
import { MinimalRequestInfo, RequestInfo, RequestState, RequestWithDetails } from '@app/types';
import { FetchInterface } from '../fetchFactory';
import { logger as baseLogger } from '../logger';
import { extractConsentCheck, extractConsentText, extractDocumentTitle } from '@app/utils/egendataInteropUtil';

const logger = baseLogger.child({ module: 'solid' });

export const requestFromURL = async (url: URL, { fetch }: { fetch: FetchInterface }): Promise<RequestInfo> => {
  if (!url.pathname.endsWith('/')) throw new Error('URL has to end with `/`');

  let ds;
  try {
    ds = await getSolidDataset(url.toString(), { fetch });
  } catch (error: any) {
    logger.info({ url }, 'Failed to fetch request data');
    throw new Error('Failed to fetch request data');
  }

  const requestInfo = requestFromContainerDS(ds);

  const subjectURL = new URL('subject', url);

  const subject = getThing(await getSolidDataset(subjectURL.toString(), { fetch }), subjectURL.toString()) as Thing;

  return {
    ...requestInfo,
    uuid: getStringNoLocale(subject, `${env.EGENDATA_SCHEMA_URL}requestId`) as string,
    type: getUrl(subject, 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type') as string,
    documentType: getUrl(subject, `${env.EGENDATA_SCHEMA_URL}documentType`) as string,
    providerWebId: new URL(getUrl(subject, `${env.EGENDATA_SCHEMA_URL}providerWebId`) as string),
    purpose: getStringNoLocale(subject, `${env.EGENDATA_SCHEMA_URL}purpose`) as string,
    requestorWebId: new URL(getUrl(subject, `${env.EGENDATA_SCHEMA_URL}requestorWebId`) as string),
    returnUrl: new URL(getUrl(subject, `${env.EGENDATA_SCHEMA_URL}returnUrl`) as string),
    created: getDatetime(subject, 'http://purl.org/dc/terms/created') as Date,
  };
};

export const requestFromContainerDS = (ds: SolidDataset & WithResourceInfo): MinimalRequestInfo => {
  const urls = getContainedResourceUrlAll(ds).map((el) => new URL(el));
  const subject = urls.find((el) => el.pathname.endsWith('/subject'));

  if (!subject) throw Error('Missing subject resource.');

  const url = new URL('./', subject);
  // const id = subject.pathname
  //   .match(/\/requests\/([0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12})/i)
  //   ?.pop();

  // if (!id) throw Error('Expected URL to contain id.');

  const fetch = urls.find((el) => el.pathname.endsWith('/fetch'));
  const data = urls.find((el) => el.pathname.endsWith('/data'));

  if ((fetch && !data) || (!fetch && data)) throw Error('Resource must contain both or neither of fetch and data.');

  if (!fetch && !data) {
    return { url, state: RequestState.RECEIVED };
  }

  const dataSize = getInteger(getThing(ds, data!.toString()) as Thing, 'http://www.w3.org/ns/posix/stat#size');
  if (dataSize === 0) {
    return { url, state: RequestState.FETCHING };
  }

  const consent = urls.find((el) => el.pathname.endsWith('/consent'));
  if (!consent) {
    return { url, state: RequestState.AVAILABLE };
  }

  return { url, state: RequestState.SHARED };
};

export const requestFromURLWithDetails = async (
  url: URL,
  { fetch }: { fetch: FetchInterface },
): Promise<RequestWithDetails> => {
  let response;
  try {
    response = await requestFromURL(url, { fetch });
  } catch (error: any) {
    logger.warn(error, `Failed to fetch information about request ${url.toString()}`);
    throw new Error('Failed to fetch request details.');
  }

  const documentType = new URL(response.documentType).hash.replace('#', '');

  let thing;

  thing = getThing(
    await getSolidDataset(response.requestorWebId.toString(), { fetch }),
    response.requestorWebId.toString(),
  ) as Thing;
  const requestorName = getStringNoLocale(thing, 'http://xmlns.com/foaf/0.1/name');
  const requestorLogo = getUrl(thing, 'http://xmlns.com/foaf/0.1/logo') ?? undefined;

  const requestorStorageURL =
    getUrl(
      getThing(
        await getSolidDataset(response.requestorWebId.toString(), { fetch }),
        response.requestorWebId.toString(),
      ) as Thing,
      'http://www.w3.org/ns/pim/space#storage',
    ) ?? '';

  const webid = new URL(`${env.IDP_BASE_URL}egendata/profile/card#me`);
  const providerDocumentTitle = await extractDocumentTitle(webid, response.documentType);
  const requestorConsentText = await extractConsentText(webid, response.documentType);
  const requestorConsentChecks = await extractConsentCheck(webid, response.documentType);

  const providerName = getStringNoLocale(thing, 'http://xmlns.com/foaf/0.1/name');
  const providerLogo = getUrl(thing, 'http://xmlns.com/foaf/0.1/logo') ?? undefined;

  const providerStorageURL =
    getUrl(
      getThing(
        await getSolidDataset(response.providerWebId.origin + response.providerWebId.pathname, { fetch }),
        response.providerWebId.toString(),
      ) as Thing,
      'http://www.w3.org/ns/pim/space#storage',
    ) ?? '';

  return {
    ...response,
    documentType,
    requestor: {
      name: requestorName!,
      logo: requestorLogo,
      storage: requestorStorageURL,
      consentText: requestorConsentText,
      consentChecks: requestorConsentChecks,
    },
    provider: {
      name: providerName!,
      logo: providerLogo,
      storage: providerStorageURL,
      documentTitle: providerDocumentTitle,
    },
  };
};
