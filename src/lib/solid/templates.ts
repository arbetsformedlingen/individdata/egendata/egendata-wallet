import { env } from '@app/env.mjs';

export const egendataSchema = env.EGENDATA_SCHEMA_URL;

export type TurtleEgendataACLProps = {
  id: string;
  storage: string;
};

export const turtleEgendataACL = ({ id, storage }: TurtleEgendataACLProps) => {
  const idURL = new URL(id);
  const storageURL = new URL(`${env.EGENDATA_PATH_FRAGMENT}inbox/`, storage);

  // prettier-ignore
  return `# ACL resource for the egendata inbox\n@prefix acl: <http://www.w3.org/ns/auth/acl#>.\n@prefix foaf: <http://xmlns.com/foaf/0.1/>.\n\n# The inbox can be written to by the public, but not read.\n<#public>\n  a acl:Authorization;\n  acl:agentClass foaf:Agent;\n  acl:accessTo <${storageURL.toString()}>;\n  acl:default <${storageURL.toString()}>;\n  acl:mode acl:Write, acl:Append.\n\n# The owner and root has full access to the inbox\n<#owner>\n  a acl:Authorization;\n  acl:agent <${idURL.toString()}>;\n  acl:accessTo <${storageURL.toString()}>;\n  acl:default <${storageURL.toString()}>;\n  acl:mode acl:Read, acl:Write, acl:Control.`
};

export type TurtleSubjectRequestProps = {
  id: string;
  requestorWebId: string;
  providerWebId: string;
  documentType: string;
  purpose: string;
  returnUrl: string;
  date: Date;
};

export const turtleSubjectRequest = ({
  id,
  requestorWebId,
  providerWebId,
  documentType,
  purpose,
  returnUrl,
  date,
}: TurtleSubjectRequestProps) => {
  // prettier-ignore
  return `@prefix egendata: <${egendataSchema}> .\n<> a egendata:InboundDataRequest ;\n  egendata:requestId "${id}" ;\n  egendata:requestorWebId <${requestorWebId}> ;\n  egendata:providerWebId <${providerWebId}> ;\n  egendata:documentType <${documentType}> ;\n  egendata:purpose "${purpose}" ;\n  egendata:returnUrl <${returnUrl}> ;\n  <http://purl.org/dc/terms/created> "${date.toISOString()}"^^<http://www.w3.org/2001/XMLSchema#dateTime> .`;
};

export type TurtleProviderRequestProps = {
  id: string;
  dataSubjectIdentifier: string;
  dataLocation: string;
  notificationInbox: string;
  documentType: string;
  date: Date;
};

export const turtleProviderRequest = ({
  // id,
  dataSubjectIdentifier,
  dataLocation,
  notificationInbox,
  documentType,
  date,
}: TurtleProviderRequestProps) => {
  // prettier-ignore
  return `@prefix egendata: <${egendataSchema}> .\n<> a egendata:OutboundDataRequest ;\n  egendata:documentType "${documentType}" ;\n  egendata:dataSubjectIdentifier "${dataSubjectIdentifier}" ;\n  egendata:dataLocation <${dataLocation}> ;\n  egendata:notificationInbox <${notificationInbox}> ;\n  <http://purl.org/dc/terms/created> "${date.toISOString()}"^^<http://www.w3.org/2001/XMLSchema#dateTime> .`;
};

export type TurtleNotificationProps = {
  url: string;
  type: string;
};

export const turtleNotification = ({ url, type }: TurtleNotificationProps) => {
  return `@prefix egendata: <${egendataSchema}> .\n\n<> a egendata:Notification ;\n  egendata:refersTo <${url}> ;\n  egendata:refersToType "${type}" ;\n  # Backwards compatibility\n  egendata:OutboundDataRequest <${url}> .`;
};

export type TurtlerequestorConsentProps = {
  consentDocument: string;
  requestorWebId: string;
  requestId: string;
  sharedData: string;
  date: Date;
};

export const turtleRequestorConsent = ({
  consentDocument,
  requestorWebId,
  requestId,
  sharedData,
  date,
}: TurtlerequestorConsentProps) => {
  return `@prefix egendata: <${egendataSchema}> .\n\n<> a egendata:ConsumerConsent ;\n  egendata:consentDocument "${consentDocument}" ;\n  egendata:requestorWebId <${requestorWebId}> ;\n  egendata:requestId "${requestId}" ;\n  egendata:sharedData "${sharedData}" ;\n  <http://purl.org/dc/terms/created> "${date.toISOString()}"^^<http://www.w3.org/2001/XMLSchema#dateTime> .`;
};

export type TurtleResponseNotificationProps = {
  dataLocation: string;
};

export type TurtleErrorRequestProps = {
  errorId: string;
  errorMessage: string;
  timestamp: Date;
};

export const turtleErrorRequest = ({ errorId, errorMessage, timestamp }: TurtleErrorRequestProps) => {
  return `@prefix egendata: <${egendataSchema}> .\n\n<> a egendata:Error ;\n  egendata:errorId "${errorId}" ;\n  egendata:errorMessage "${errorMessage}" ;\n  <http://purl.org/dc/terms/created> "${timestamp.toISOString()}"^^<http://www.w3.org/2001/XMLSchema#dateTime> .`;
};
