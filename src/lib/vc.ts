import { CredentialSubject } from '@app/types';

export const decode = (data: string): { subject: CredentialSubject } => {
  const json = JSON.parse(Buffer.from(data, 'base64').toString());
  return { subject: json.credentialSubject };
};
