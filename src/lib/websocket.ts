import { signIn, useSession } from 'next-auth/react';
import React, { useEffect, useRef } from 'react';
import { api } from '@app/utils/api';

interface WebSocketProviderProps {
  children: React.ReactNode;
}

export function WebSocketProvider({ children }: WebSocketProviderProps) {
  const { data: session, status } = useSession();
  const websocket = useRef<WebSocket | null>(null);
  const utils = api.useContext();

  useEffect(() => {
    if (session?.error === 'RefreshAccessTokenError') {
      // Try to force sign in, should fix the problem.
      console.log(`Session error (${session.error}), trying to initiate sign in.`);
      signIn('solid', { callbackUrl: '/home' });
    }
  }, [session]);

  api.subscription.getSubscriptionUrl.useQuery(undefined, {
    enabled: status === 'authenticated', // && !websocket.current,
    onSuccess: async (data) => {
      if (!data.url) {
        console.log('No endpoint provided for websocket connection.');
        return;
      }
      const ws = connectWebSocket(data.url);
      websocket.current = ws;
    },
  });

  const connectWebSocket = (endpoint: URL | string): WebSocket => {
    console.log('Setting up websocket connection.');

    const socket = new WebSocket(endpoint);

    socket.addEventListener('open', async (event) => {
      console.log('Connected to notification server.');
    });

    socket.addEventListener('message', async (message) => {
      const data = JSON.parse(message.data);

      console.log('Websocket received message', data);

      if (data['@context'].includes('https://www.w3.org/ns/solid/notification/v1') && data.type.includes('Add')) {
        const resource = data.object;
        const id = resource.split('/').pop();

        console.log(`Invalidating request ${id}.`);
        utils.request.invalidate();
      }
    });

    socket.addEventListener('error', async (error) => {
      console.error(error);
    });

    socket.addEventListener('close', async (event) => {
      console.log(`Disconnected from notification server, (reason: ${event.code})`);
      socket.close();
    });

    return socket;
  };

  return children as JSX.Element;
}
