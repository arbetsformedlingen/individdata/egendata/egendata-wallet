import en from './en.json';
import sv from './sv.json';

export const AVAILABLE_LANGUAGES: { [key: string]: string } = { sv: 'Svenska', en: 'English' };
export const DEFAULT_LANGUAGE = 'sv';

export const LOCALES: { [key: string]: any } = {
  en,
  sv,
};

export default LOCALES;
