import type { AppType } from 'next/app';
import { Session } from 'next-auth';
import { SessionProvider } from 'next-auth/react';
import React from 'react';
import Head from 'next/head';
import Script from 'next/script';
import { WebSocketProvider } from '@app/lib/websocket';
import { CustomIntlProvider } from '@app/lib/i18n';

import { api } from '../utils/api';

import '../styles/globals.css';

const MyApp: AppType<{ session: Session | null }> = ({ Component, pageProps: { session, ...pageProps } }) => {
  return (
    <>
      <Script src="./js/dm.js" />
      <SessionProvider session={session} refetchInterval={0}>
        <CustomIntlProvider>
          <Head>
            <meta name="viewport" content="initial-scale=1 width=device-width" />
            <title>Egendata</title>
          </Head>
          <WebSocketProvider>
            <Component {...pageProps} />
          </WebSocketProvider>
        </CustomIntlProvider>
      </SessionProvider>
    </>
  );
};

export default api.withTRPC(MyApp);
