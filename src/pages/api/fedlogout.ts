import { env } from '@app/env.mjs';
import { NextApiRequest, NextApiResponse } from 'next';
import logger from '@app/lib/logger';

async function federatedLogout(req: NextApiRequest, res: NextApiResponse) {
  const idToken = req.query.id_token;

  if (!idToken) {
    logger.debug('Missing id_token value.');
    res.status(400);
    return;
  }

  const endsessionURL = new URL('.oidc/session/end', env.IDP_BASE_URL);
  endsessionURL.searchParams.set('id_token_hint', idToken as string);
  endsessionURL.searchParams.set('post_logout_redirect_uri', new URL('/', env.NEXTAUTH_URL).toString());

  return res.redirect(endsessionURL.toString());
}

export default federatedLogout;
