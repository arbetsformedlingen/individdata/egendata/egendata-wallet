import Footer from '@ui/FooterContainer';
import MenuBar from '@ui/MenuBarContainer';

function Consents() {
  return (
    <div className="flex min-h-screen flex-col justify-between bg-zinc-50 dark:bg-zinc-800">
      <MenuBar />
      <main className="mx-auto h-full w-full flex-1 pt-10 md:w-8/12">
        <h2>Consents</h2>
      </main>
      <Footer />
    </div>
  );
}

export default Consents;
