/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect } from 'react';
import MenuBar from '@ui/MenuBarContainer';
import Footer from '@ui/FooterContainer';
import RequestList from '@ui/RequestListContainer';

import { env } from '@app/env.mjs';
import { api } from '@app/utils/api';

function HomePage() {
  api.notifications.processAllNotifications.useQuery();

  return (
    <div className="flex min-h-screen flex-col justify-between bg-zinc-50 dark:bg-zinc-800">
      <MenuBar />
      <main className="flex-1 self-center pt-10 md:w-8/12">
        <RequestList />
      </main>
      <Footer />
    </div>
  );
}

export default HomePage;
