import React from 'react';
import Link from 'next/link';
import { FormattedMessage } from 'react-intl';
import { signIn } from 'next-auth/react';
import Footer from '@ui/FooterContainer';

function Index() {
  return (
    <div className="w-fill flex h-screen flex-row">
      <div
        className="flex h-full flex-1 flex-col place-content-center bg-cover text-center text-zinc-100"
        style={{ backgroundImage: 'url(./leaves.jpg)' }}
      >
        <h1 className="text-8xl font-light">
          <FormattedMessage
            id="aeoy6U"
            defaultMessage="Your data in your control"
            description="Index page main text."
            data-testid="title"
          />
        </h1>
        <p className="mt-6 text-2xl">
          <FormattedMessage
            id="J6K/Sm"
            defaultMessage="EgenData is a governmental initiative that allows you to store and transfer digital information between public and private organizations."
            description="Index page main subtitle."
            data-testid="subtitle"
          />
        </p>
      </div>
      <div className="flex h-full flex-1 flex-col justify-between bg-cover text-center">
        <div className="p-4 text-left text-lg">Egendata</div>
        <div>
          <h2 className="text-5xl font-light">
            <FormattedMessage
              id="pKuvHm"
              defaultMessage="Welcome to EgenData"
              description="Index page welcome text."
              data-testid="welcome-text"
            />
          </h2>
          <p className="my-4">
            <FormattedMessage
              defaultMessage="Identify yourself to continue"
              id="ygXRZK"
              description="Index login call to action."
              data-testid="identify-to-continue"
            />
          </p>
          <p>
            <button
              className="rounded-full bg-green-600 px-4 py-2"
              onClick={() => signIn('solid', { callbackUrl: '/home' })}
            >
              <FormattedMessage
                id="4G0feJ"
                defaultMessage="Log in"
                description="Index page log in button."
                data-testid="login-button"
              />
            </button>
          </p>
          <p className="my-4">
            <Link href="http:w3schools.com" className="hover:text-black dark:hover:text-white">
              <FormattedMessage
                id="nGfQl/"
                defaultMessage="How do I login with BankID?"
                description="Index page login help text."
                data-testid="how-to-login"
              />
            </Link>
          </p>
        </div>
        <Footer />
      </div>
    </div>
  );
}

export default Index;
