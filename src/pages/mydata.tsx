import Footer from '@ui/FooterContainer';
import MenuBar from '@ui/MenuBarContainer';

function MyData() {
  return (
    <div className="flex min-h-screen flex-col justify-between bg-zinc-50 dark:bg-zinc-800">
      <MenuBar />
      <main className="flex-1 self-center pt-10 md:w-8/12">
        <h2>My data</h2>
      </main>
      <Footer />
    </div>
  );
}

export default MyData;
