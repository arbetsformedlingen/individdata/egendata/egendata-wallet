import React, { useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import FetchingInProgressDialog from '@ui/FetchingInProgressDialog';
import ConsentDialogContainer from '@ui/ConsentDialogContainer';
import { ExtendedRequestState } from '@app/types';
import Footer from '@ui/FooterContainer';
import MenuBar from '@ui/MenuBarContainer';
import Stepper from '@ui/StepperContainer';
import ProcessDocumentContainer from '@ui/ProcessDocumentContainer';

import { api } from '@app/utils/api';

function RequestPage() {
  const router = useRouter();
  const utils = api.useContext();

  const [state, setState] = useState<ExtendedRequestState>('received');
  const [showModal, setShowModal] = useState(false);
  const timer = useRef<NodeJS.Timeout>();

  const requestState = api.request.changeStateOfRequest.useMutation({
    onError: (error) => {
      console.error('Error getting data', error);
    },
  });

  const { id, redirect } = router.query as { id: string; redirect: string };

  const request = api.request.getRequestDetails.useQuery({ id });

  const handleGetClick = async () => {
    console.log('Handling get click.');

    setState('fetching');
    setShowModal(true);

    timer.current = setTimeout(() => setState('timeout'), 60 * 1000);

    requestState.mutate(
      { id: id as string, newState: 'fetching' },
      {
        onSuccess: () => {
          utils.request.getRequestDetails.invalidate({ id });
        },
        onError: () => {
          setState('error');
        },
      },
    );
  };

  const handleShowConsentClick = () => {
    setState('preview');
    setShowModal(true);
  };

  const handleContinueClick = () => {
    setState('consent');
  };

  const handleConsentClick = async () => {
    console.log('Handling consent click');
    requestState.mutate(
      { id: id as string, newState: 'sharing' },
      {
        onSuccess: () => {
          setState('sharing');
          utils.request.getRequestDetails.invalidate({ id });
        },
        onError: () => {
          setState('error');
        },
      },
    );
  };

  useEffect(() => {
    console.log(`State changed to ${state} (local)`);
  }, [state]);

  useEffect(() => {
    console.log('Effect for request running');
    if (state === 'fetching' && request.data?.state === 'available') {
      clearTimeout(timer.current);
      timer.current = undefined;

      console.log('Closing modal.');
      setShowModal(false);
    }
  }, [state, request]);

  return (
    <div className="flex min-h-screen flex-col justify-between bg-zinc-50 dark:bg-zinc-800">
      <MenuBar disabledNav={redirect === ''} />

      <main className="flex flex-1 flex-col self-center md:w-8/12">
        <div className="flex-1"></div>
        <section>
          {id && (
            <>
              <Stepper requestId={id as string} />

              <ProcessDocumentContainer
                requestId={id as string}
                onGetClick={handleGetClick}
                onShowConsentClick={handleShowConsentClick}
              />
            </>
          )}

          {showModal && ['fetching', 'received', 'timeout'].includes(state) && (
            <FetchingInProgressDialog open={showModal} state={state} onClose={() => setShowModal(false)} />
          )}

          {showModal && ['preview', 'consent', 'sharing'].includes(state) && (
            <ConsentDialogContainer
              requestId={id}
              state={state}
              open={showModal}
              onContinue={handleContinueClick}
              onConsent={handleConsentClick}
              onClose={() => setShowModal(false)}
            />
          )}

          {state === 'error' && <div>ERROR</div>}
        </section>
        <div className="flex-grow-[2]"></div>
      </main>
      <Footer />
    </div>
  );
}

export default RequestPage;
