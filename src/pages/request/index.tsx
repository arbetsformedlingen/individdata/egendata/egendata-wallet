import React, { useEffect, useState } from 'react';
import { Router, useRouter } from 'next/router';
import Link from 'next/link';
import { FormattedMessage } from 'react-intl';
import { signIn, useSession } from 'next-auth/react';
import Footer from '@ui/FooterContainer';
import MenuBar from '@ui/MenuBarContainer';
import Stepper from '@ui/StepperContainer';
import Button from '@ui/Button';

import { api } from '@app/utils/api';

function Landing() {
  const session = useSession();
  const router = useRouter();
  const [requestorWebId, setRequestorWebId] = useState('');

  const create = api.request.createRequest.useMutation({
    onSuccess: (data) => {
      localStorage.removeItem('payload');
      router.push(`/request/${data.id}?redirect`);
    },
    onError: () => {
      throw new Error('Failed to save request.');
    },
  });

  useEffect(() => {
    if (router.isReady) {
      if (router.query.payload) {
        decodePayload(getPayload(router.query));
      }

      if (!router.query.payload && create.isIdle) {
        const raw = localStorage.getItem('payload');
        if (raw) {
          create.mutate(decodePayload(raw));
        } else {
          router.push('/home');
        }
      }
    }
  }, [create, router]);

  const getPayload = (query: Router['query']): string => {
    if (!query.payload) throw Error('Payload missing.');
    return Array.isArray(query.payload) ? query.payload[0] : query.payload;
  };

  const decodePayload = (raw: string) => {
    const payload = JSON.parse(Buffer.from(decodeURIComponent(raw), 'base64').toString('utf8'));
    setRequestorWebId(payload.requestorWebId);
    return payload;
  };

  const handleContinue = async () => {
    const raw = getPayload(router.query);
    if (!raw) throw Error('Missing payload.');

    localStorage.setItem('payload', raw);
    signIn('solid', { callbackUrl: '/request' }, { prompt: 'login' }); // User should be required to sign in.
  };

  const handleReturn = async () => {
    const raw = getPayload(router.query);
    if (!raw) throw Error('Missing payload.');
    const returnUrl = decodePayload(raw).returnUrl;

    window.location.href = returnUrl;
    return;
  };

  const requestor = api.profile.getPublicProfile.useQuery(
    { webid: requestorWebId },
    { enabled: requestorWebId.length > 0 },
  );

  return (
    <div className="flex min-h-screen w-full flex-col  bg-zinc-50 dark:bg-zinc-800">
      <MenuBar disabledNav />

      <main className="flex flex-1 flex-col items-center self-center text-center md:w-8/12">
        <div className="flex-1"></div>
        <section className="max-w-xl">
          <h2 className="pb-8 text-xl">
            <FormattedMessage
              id="JvjNu7"
              defaultMessage="Share your data with <boldThis>{party}</boldThis>"
              description="Landing page main title."
              values={{
                party: requestor.data?.name,
                boldThis: (msg) => <span className="">{msg}</span>,
              }}
            />
          </h2>
          <Stepper landing />
          <p className="m-4 mt-8 text-lg">
            <FormattedMessage
              defaultMessage="To handle your request you need to identify yourself."
              id="5fkC00"
              description="Landing page instruction."
            />
          </p>
          <p className="my-4 text-lg dark:text-zinc-400 ">
            <FormattedMessage
              defaultMessage="If you are a first time user of EgenData an account will be created for you when you log in."
              id="iXMyqV"
              description="Landing page further instruction."
            />
          </p>
          <p className="my-4">
            <FormattedMessage
              id="QEzAbb"
              defaultMessage="By registering you accept our{linebreak}<linkPrivacyPolicy>Privacy Policy</linkPrivacyPolicy> and <linkTermsOfService>Terms of service</linkTermsOfService>."
              description="Landing page terms and conditions text."
              values={{
                linebreak: <br />,
                linkPrivacyPolicy: (text) => (
                  <Link href="/privacypolicy" color="text.primary">
                    {text}
                  </Link>
                ),
                linkTermsOfService: (text) => (
                  <Link href="/termsofservice" color="text.primary">
                    {text}
                  </Link>
                ),
              }}
            />
          </p>

          <div className="flex justify-center py-8">
            <Button variant="outlined" onClick={handleContinue}>
              <FormattedMessage
                id="dZKphV"
                defaultMessage="Sign in and continue"
                description="Landing page sign in button."
              />
            </Button>
          </div>
          <div className="flex justify-center py-8">
            <Button variant="outlined" onClick={handleReturn}>
              {session.status === 'authenticated' ? (
                <FormattedMessage id="l/R89d" defaultMessage="Return" description="Landing page continue button." />
              ) : (
                <FormattedMessage
                  id="f9FRF0"
                  defaultMessage="Cancel and return"
                  description="Landing page sign in cancelletion button."
                />
              )}
            </Button>
          </div>
        </section>
        <div className="flex-grow-[2]"></div>
      </main>
      <Footer>
        <span className="text-zinc-300">
          <FormattedMessage
            id="KCvQSH"
            defaultMessage="EgenData is a governmental initiative that allows you to store and transfer digital information between public and private organtisations."
            description="Landing page footer disclaimer."
          />
        </span>
      </Footer>
    </div>
  );
}

export default Landing;
