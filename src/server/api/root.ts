import { createTRPCRouter } from './trpc';
import { profileRouter } from './routers/profile';
import { requestRouter } from './routers/request';
import { subscriptionRouter } from './routers/subscription';
import { notificationRouter } from './routers/notification';

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here
 */
export const appRouter = createTRPCRouter({
  profile: profileRouter,
  request: requestRouter,
  subscription: subscriptionRouter,
  notifications: notificationRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
