import logger from '@app/lib/logger';
import { env } from '@app/env.mjs';
import fetchFactory from '@app/lib/fetchFactory';
import { listAllNotifications, processNotification } from '@app/lib/solid/notifications';
import { z } from 'zod';

import { createTRPCRouter, protectedProcedure, publicProcedure } from '../trpc';

export const notificationRouter = createTRPCRouter({
  processNotification: protectedProcedure.input(z.object({ url: z.string().url() })).query(async ({ input, ctx }) => {
    const { url } = input;
    const { token } = ctx;

    const authFetch = fetchFactory({ keyPair: token?.keys, dpopToken: token?.accessToken });

    await processNotification(new URL(url), { fetcher: authFetch });
  }),

  processAllNotifications: protectedProcedure.query(async ({ ctx }) => {
    const { token } = ctx;
    const { storage } = ctx.session;

    const authFetch = fetchFactory({ keyPair: token?.keys, dpopToken: token?.accessToken });

    const url = new URL(`${storage}${env.EGENDATA_PATH_FRAGMENT}inbox/`);

    const notifications = await listAllNotifications(url, { fetcher: authFetch });

    for (const notification of notifications) {
      await processNotification(notification, { fetcher: authFetch });
    }

    return true;
  }),
});
