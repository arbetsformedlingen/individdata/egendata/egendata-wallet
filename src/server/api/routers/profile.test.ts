import { afterAll, afterEach, beforeAll, describe, expect, it } from 'vitest';
import { SetupServerApi, setupServer } from 'msw/node';
import { fakeSession } from '@tests/utils/auth';
import { inferProcedureInput } from '@trpc/server';
import { AppRouter, appRouter } from '../root';
import { createInnerTRPCContext } from '../trpc';

import { handlers as agentHandlers, urls } from '@tests/mocks/agents';

describe('whoami', () => {
  let server: SetupServerApi;

  beforeAll(() => {
    server = setupServer(...agentHandlers);
    server.listen();
  });
  afterAll(() => server.close());
  afterEach(() => server.resetHandlers());

  it('returns user profile data', async () => {
    const { token, session } = fakeSession();
    const ctx = createInnerTRPCContext({ token, session });
    const caller = appRouter.createCaller(ctx);

    // const input: inferProcedureInput<AppRouter['profile']['whoami']> = {};

    const request = await caller.profile.whoami();

    expect(request.name).toBe('Kalle Anka');
  });
});

describe('getPublicProfile', () => {
  let server: SetupServerApi;

  beforeAll(() => {
    server = setupServer(...agentHandlers);
    server.listen();
  });
  afterAll(() => server.close());
  afterEach(() => server.resetHandlers());

  it('returns public profile data for organizational agent', async () => {
    const ctx = createInnerTRPCContext({ token: null, session: null });
    const caller = appRouter.createCaller(ctx);

    const input: inferProcedureInput<AppRouter['profile']['getPublicProfile']> = {
      webid: urls.ORG_PROVIDER_WEBID,
    };

    const request = await caller.profile.getPublicProfile(input);

    expect(request.name).toBe('TemplateTest1Source');
  });
});
