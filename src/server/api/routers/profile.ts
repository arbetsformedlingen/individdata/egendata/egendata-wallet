import fetchFactory from '@app/lib/fetchFactory';
import { Thing, getSolidDataset, getStringNoLocale, getThing, getUrl } from '@inrupt/solid-client';
import { TRPCError } from '@trpc/server';
import { z } from 'zod';

import { createTRPCRouter, protectedProcedure, publicProcedure } from '../trpc';

export const profileRouter = createTRPCRouter({
  whoami: protectedProcedure.query(async ({ ctx }) => {
    const { session, token } = ctx;
    try {
      const fetch = fetchFactory({ keyPair: token?.keys, dpopToken: token?.accessToken });

      // Fetch basic data from provider request
      const ds = await getSolidDataset(session.seeAlso, { fetch });
      const requestThing = getThing(ds, session.seeAlso) as Thing;
      const firstName = getStringNoLocale(requestThing, 'http://xmlns.com/foaf/0.1/firstName') ?? undefined;
      const lastName = getStringNoLocale(requestThing, 'http://xmlns.com/foaf/0.1/lastName') ?? undefined;

      return {
        name: `${firstName} ${lastName}`,
        firstName,
        lastName,
      };
    } catch (error) {
      throw new TRPCError({ code: 'NOT_FOUND', message: 'Failed to fetch details.' });
    }
  }),

  getPublicProfile: publicProcedure.input(z.object({ webid: z.string().url() })).query(async ({ input }) => {
    const { webid } = input;

    const thing = getThing(await getSolidDataset(webid.split('#')[0], { fetch }), webid) as Thing;
    const name = getStringNoLocale(thing, 'http://xmlns.com/foaf/0.1/name');
    const logo = getUrl(thing, 'http://xmlns.com/foaf/0.1/logo') ?? undefined;

    if (name === null) {
      throw new TRPCError({ code: 'BAD_REQUEST', message: 'No public profile found.' });
    }

    return { name, logo };
  }),
});
