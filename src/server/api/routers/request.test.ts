import { afterAll, afterEach, beforeAll, describe, expect, it } from 'vitest';
import { SetupServerApi, setupServer } from 'msw/node';
import { createInnerTRPCContext } from '@app/server/api/trpc';
import { AppRouter, appRouter } from '@app/server/api/root';
import { fakeSession } from '../../../../tests/utils/auth';
import { inferProcedureInput } from '@trpc/server';
import { env } from '@app/env.mjs';

import { handlers as requestHandlers } from '../../../../tests/mocks/requests';

describe('getRequest', () => {
  let server: SetupServerApi;

  beforeAll(() => {
    server = setupServer(...requestHandlers);
    server.listen();
  });
  afterAll(() => server.close());
  afterEach(() => server.resetHandlers());

  it('returns request data when provided id', async () => {
    const { token, session } = fakeSession();
    const ctx = createInnerTRPCContext({ token, session });
    const caller = appRouter.createCaller(ctx);

    const input: inferProcedureInput<AppRouter['request']['getRequest']> = {
      id: '048ff7da-f622-44a7-96f5-e68abf7876e3',
    };

    const request = await caller.request.getRequest(input);

    expect(request.type).toBe('https://egendata.se/schema/core/v1#InboundDataRequest');
  });

  it('returns request data when provided url', async () => {
    const { token, session } = fakeSession();
    const ctx = createInnerTRPCContext({ token, session });
    const caller = appRouter.createCaller(ctx);

    const input: inferProcedureInput<AppRouter['request']['getRequest']> = {
      url: `${env.POD_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/egendata/requests/048ff7da-f622-44a7-96f5-e68abf7876e3/`,
    };

    const request = await caller.request.getRequest(input);

    expect(request.type).toBe('https://egendata.se/schema/core/v1#InboundDataRequest');
  });
});
