import { env } from '@app/env.mjs';
import fetchFactory from '@app/lib/fetchFactory';
import logger from '@app/lib/logger';
import { requestFromURL, requestFromURLWithDetails, turtleACL, turtleSubjectRequest } from '@app/lib/solid';
import {
  Thing,
  getContainedResourceUrlAll,
  getSolidDataset,
  getStringNoLocale,
  getTerm,
  getThing,
} from '@inrupt/solid-client';
import { v4 as uuid } from 'uuid';
import { decode } from '@app/lib/vc';

import { TRPCError } from '@trpc/server';
import { z } from 'zod';

import { createTRPCRouter, protectedProcedure } from '../trpc';
import { changeToFetching, changeToSharing } from '@app/lib/requestStateChanger';
import { RequestInfo, RequestState } from '@app/types';

export const requestRouter = createTRPCRouter({
  getRequest: protectedProcedure
    .input(
      z
        .object({ id: z.string().uuid(), url: z.string().url() })
        .partial()
        .refine((data) => data.id || data.url, 'Either id or url should be provided.'),
    )
    .query(async ({ input, ctx }) => {
      const url = input.id
        ? new URL(`${ctx.session.storage}${env.EGENDATA_PATH_FRAGMENT}requests/${input.id}/`)
        : new URL(input.url!);

      let response;
      try {
        response = await requestFromURL(url, { fetch });
      } catch (error: any) {
        logger.warn(error, `Failed to fetch information about request ${url.toString()}`);
        throw new TRPCError({ code: 'INTERNAL_SERVER_ERROR', message: 'Failed to fetch request details.' });
      }
      return response;
    }),

  createRequest: protectedProcedure
    .input(
      z.object({
        requestorWebId: z.string().url(),
        providerWebId: z.string().url(),
        documentType: z.string(),
        purpose: z.string(),
        returnUrl: z.string().url(),
      }),
    )
    .mutation(async ({ input, ctx }) => {
      const { webid, storage } = ctx.session;
      const id = uuid();

      const authFetch = fetchFactory({ keyPair: ctx.token?.keys, dpopToken: ctx.token?.accessToken });

      const subjectRequestURL = new URL(`${storage}${env.EGENDATA_PATH_FRAGMENT}requests/${id}/subject`);

      const data = turtleSubjectRequest({ ...input, id, date: new Date() });
      await authFetch(subjectRequestURL.toString(), {
        method: 'PUT',
        body: data,
        headers: { 'Content-Type': 'text/turtle' },
      });

      const acl = turtleACL(subjectRequestURL.toString(), [
        { label: 'owner', agent: webid as string, mode: ['Control', 'Write', 'Append', 'Read'] },
      ]);
      await authFetch(new URL(`${subjectRequestURL.pathname.split('/').pop()}.acl`, subjectRequestURL.toString()), {
        method: 'PUT',
        body: acl,
        headers: { 'Content-Type': 'text/turtle' },
      });

      logger.info(`Created request ${subjectRequestURL}`);

      return {
        id,
        location: new URL(id, `${env.NEXTAUTH_URL}/api/request/`),
      };
    }),

  changeStateOfRequest: protectedProcedure
    .input(z.object({ id: z.string().uuid(), newState: z.union([z.literal('fetching'), z.literal('sharing')]) }))
    .mutation(async ({ input, ctx }) => {
      const { id, newState } = input;
      const { session } = ctx;

      const authFetch = fetchFactory({ keyPair: ctx.token?.keys, dpopToken: ctx.token?.accessToken });

      if (newState === 'fetching') {
        logger.info(`Changing state of request ${id} to 'fetching'.`);
        const baseURL = new URL(env.EGENDATA_PATH_FRAGMENT as string, session.storage);

        const requestURL = new URL(`requests/${id}/`, baseURL);

        try {
          await changeToFetching(session.webid, session.seeAlso, requestURL, authFetch);
        } catch (error: any) {
          logger.error(error, `Failed to change state of ${requestURL.toString()} to fetching.`);
          throw new Error('Failed to change state.');
        }

        logger.info(`Accepted consent to fetch for ${id}`);

        // res.status(201).json([subjectRequest, consentProvider, dataStore, notification]);
        return { state: RequestState.FETCHING };
      }

      if (newState === 'sharing') {
        logger.info(`Changing state of request ${id} to 'sharing'.`);
        const baseURL = new URL(env.EGENDATA_PATH_FRAGMENT as string, session.storage);

        const requestURL = new URL(`requests/${id}/`, baseURL);

        // let subjectRequest, consentProvider, dataStore, notification;

        try {
          await changeToSharing(session.webid, requestURL, authFetch);
        } catch (error: any) {
          logger.error(error, `Failed to change state of ${requestURL.toString()} to sharing.`);
          throw new Error('Failed to change state.');
        }

        logger.info(`Accepted consent to share for ${id}`);

        // res.status(201).json([subjectRequest, consentProvider, dataStore, notification]);
        return { state: RequestState.SHARED };
      }
    }),

  getInfiniteRequests: protectedProcedure
    .input(z.object({ limit: z.number().min(5).max(100).nullish(), cursor: z.number().nullish() }))
    .query(async ({ input, ctx }) => {
      const limit = input.limit ?? 10;
      const cursor = input.cursor ?? 0;

      const fetch = fetchFactory({ keyPair: ctx.token?.keys, dpopToken: ctx.token?.accessToken });

      // Get list of requests.
      let requests: { url: string; date: Date }[] = [];
      const resourceLocation = `${ctx.session?.storage}${env.EGENDATA_PATH_FRAGMENT}requests/`;
      try {
        const ds = await getSolidDataset(resourceLocation, { fetch });
        requests = getContainedResourceUrlAll(ds).map((url) => {
          const thing = getThing(ds, url) as Thing;
          const term = getTerm(thing, 'http://purl.org/dc/terms/modified')?.value;
          return {
            url,
            date: term ? new Date(term) : new Date(),
          };
        });
        requests.sort((a, b) => (a.date > b.date ? -1 : 1));
      } catch (error: any) {
        if (error.response.status === 401) {
          throw new TRPCError({ code: 'FORBIDDEN', message: 'Not authenticated.' });
        }
        if (error.response.status !== 404) {
          throw new TRPCError({ code: 'NOT_FOUND', message: 'Could not find resource.' });
        }
        return { requests: [], total: 0 };
      }

      const urls = requests.slice(cursor, cursor + limit);
      const nextCursor: typeof cursor = cursor + limit;
      const total = requests.length;

      const items: RequestInfo[] = await Promise.all(
        urls.map((request) => requestFromURLWithDetails(new URL(request.url), { fetch })),
      );

      return { items, nextCursor, total };
    }),

  getRequestDetails: protectedProcedure.input(z.object({ id: z.string() })).query(async ({ input, ctx }) => {
    const { id } = input;

    const fetch = fetchFactory({ keyPair: ctx.token?.keys, dpopToken: ctx.token?.accessToken });

    const requestURL = new URL(`${ctx.session.storage}${env.EGENDATA_PATH_FRAGMENT}requests/${id}/`);

    return await requestFromURLWithDetails(requestURL, { fetch });
  }),

  getCredential: protectedProcedure.input(z.object({ id: z.string().uuid() })).query(async ({ input, ctx }) => {
    const { id } = input;
    const { token, session } = ctx;

    const fetch = fetchFactory({ keyPair: token?.keys, dpopToken: token?.accessToken });

    const credentialUrl = new URL(`${env.EGENDATA_PATH_FRAGMENT}requests/${id}/data`, session.storage);

    const data = getStringNoLocale(
      getThing(await getSolidDataset(credentialUrl.toString(), { fetch }), credentialUrl.toString()) as Thing,
      `${env.EGENDATA_SCHEMA_URL}document`,
    );

    if (data === null) {
      throw new TRPCError({ code: 'BAD_REQUEST', message: 'No credential found.' });
    }

    const credential = decode(data).subject;

    logger.debug(credential);

    return credential;
  }),
});
