import logger from '@app/lib/logger';
import { env } from '@app/env.mjs';
import fetchFactory from '@app/lib/fetchFactory';
import { subscribe } from '@app/lib/solid/notifications';

import { createTRPCRouter, protectedProcedure, publicProcedure } from '../trpc';

export const subscriptionRouter = createTRPCRouter({
  getSubscriptionUrl: protectedProcedure.query(async ({ ctx }) => {
    const { token, session } = ctx;

    const authFetch = fetchFactory({ keyPair: token?.keys, dpopToken: token?.accessToken });

    const topic = new URL(`${env.EGENDATA_PATH_FRAGMENT}inbox/`, session.storage);

    const url = await subscribe(topic, { fetcher: authFetch });

    return { url };
  }),
});
