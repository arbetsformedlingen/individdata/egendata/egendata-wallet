import {
  type Account,
  type NextAuthOptions,
  type Session,
  type Token,
  type TokenSet,
  type User,
  getServerSession,
} from 'next-auth';
import { GetServerSidePropsContext } from 'next';
import SolidProvider from '@app/lib/SolidProvider';
import { v4 as uuid } from 'uuid';
import type { JWK } from 'jose';
import fetchFactory, { generateDPoP } from '@app/lib/fetchFactory';
import { env } from '@app/env.mjs';

import { logger as baseLogger } from '@app/lib/logger';
import { fetchBasicProfileData } from '@app/lib/solid';
import { verifyUserPodStructure } from '@app/lib/solid/pod';

const logger = baseLogger.child({ module: 'auth' });

declare module 'next-auth' {
  interface Session {
    webid: string;
    wsid: string;
    storage: string;
    seeAlso: string;
    dpopToken: string;
    keys: {
      privateKey: JWK;
      publicKey: JWK;
    };
    user: User;
    idToken: string;
    subscription: string;
    error?: string;
  }

  interface Token extends TokenSet {
    accessToken: string;
    accessTokenExpiresAt: number;
    refreshToken: string;
    idToken: string;
    keys: {
      privateKey: JWK;
      publicKey: JWK;
    };
    error?: string;
    user: User;
    webid: string;
    wsid: string;
    storage: string;
    seeAlso: string;
  }

  interface User {
    id: string;
    webid: string;
  }

  interface Account {
    keys: {
      privateKey: JWK;
      publicKey: JWK;
    };
  }
}

// https://next-auth.js.org/tutorials/refresh-token-rotation
const refreshAccessToken = async (token: Partial<Token>): Promise<Token> => {
  try {
    if (!token.refreshToken) {
      const msg = 'No refresh token to use.';
      logger.debug({ token }, msg);
      throw new Error(msg);
    }

    const url = new URL('.oidc/token', env.IDP_BASE_URL);
    const params = new URLSearchParams({
      grant_type: 'refresh_token',
      refresh_token: encodeURIComponent(token.refreshToken!),
    });

    const dpopHeader = await generateDPoP({
      jwkPrivateKey: token.keys!.privateKey,
      jwkPublicKey: token.keys!.publicKey,
      method: 'POST',
      url: url.toString(),
    });

    const auth = `${encodeURIComponent(env.SOLID_CLIENT_ID)}:${encodeURIComponent(env.SOLID_CLIENT_SECRET)}`;

    const headers = {
      Authorization: `Basic ${Buffer.from(auth).toString('base64')}`,
      DPoP: dpopHeader,
      'Content-Type': 'application/x-www-form-urlencoded',
    };

    const response = await fetch(url, {
      headers,
      method: 'POST',
      body: params.toString(),
    });

    if (!response.ok) {
      const msg = 'Failed to refresh tokens.';
      logger.info({ status: response.status, statusText: response.statusText, headers, body: params.toString() });
      throw Error(msg, {
        cause: Error(`${response.statusText} (${response.status})`),
      });
    }

    const refreshedTokens = await response.json();

    logger.info('Successfully refreshed token.');

    return {
      ...token,
      accessToken: refreshedTokens.access_token,
      accessTokenExpiresAt: Date.now() + refreshedTokens.expires_in * 1000,
      refreshToken: refreshedTokens.refresh_token ?? token.refreshToken,
    } as Token;
  } catch (error) {
    logger.info(error, 'RefreshAccessTokenError');

    return {
      ...token,
      error: 'RefreshAccessTokenError',
    } as Token;
  }
};

let promiseStartedAt = 0;
let promise: Promise<Token>;
/**
 * Make sure that we only make one attempt at a time to refresh tokens.
 */
const throttledRefreshAccessToken = (token: Partial<Token>) => {
  if (Date.now() - promiseStartedAt > 3000) {
    promise = refreshAccessToken(token);
    promiseStartedAt = Date.now();
  }

  return promise;
};

export const authOptions: NextAuthOptions = {
  debug: env.NODE_ENV !== 'production',
  providers: [SolidProvider({ clientId: env.SOLID_CLIENT_ID, clientSecret: env.SOLID_CLIENT_SECRET })],
  callbacks: {
    async jwt({
      token,
      user,
      account,
    }: {
      token: Partial<Token>;
      user?: User;
      account?: Account | null;
    }): Promise<Partial<Token>> {
      if (account) {
        // Save access and refresh tokens in JWT on login
        logger.trace({ token, account, user }, 'Login data');

        if (!user) throw new Error('Missing user details.');

        const { storage, seeAlso } = await fetchBasicProfileData(user.webid);

        try {
          const fetcher = fetchFactory({ dpopToken: account.access_token, keyPair: account.keys });
          await verifyUserPodStructure({
            webid: user.webid,
            storage,
            fetcher,
          });
        } catch (error: any) {
          logger.warn(error, 'Could not verify user pod structure.');
        }

        const accessTokenExpiresAt = account.expires_at! * 1000;

        logger.trace({ expires_at: account.expires_at, accessTokenExpiresAt, now: Date.now() });

        return {
          webid: user.webid,
          accessToken: account.access_token, // DPoP bound access token
          accessTokenExpiresAt,
          refreshToken: account.refresh_token,
          idToken: account.id_token,
          keys: account.keys,
          wsid: uuid(),
          user,
          storage,
          seeAlso,
        };
      } else if (Date.now() < token.accessTokenExpiresAt!) {
        // Access token is still valid, return it
        return token;
      } else {
        return throttledRefreshAccessToken(token);
      }
    },
    async session({ session, token }: { session: Partial<Session>; token: Partial<Token> }): Promise<Session> {
      session.user = token.user as User;

      // Why the conditional?
      if (token.webid && !(session.webid || session.storage || session.seeAlso)) {
        session.webid = token.webid;
        session.wsid = token.wsid;
        session.idToken = token.idToken;
        session.storage = token.storage;
        session.seeAlso = token.seeAlso;
        session.error = token.error;
      }
      return session as Session;
    },
  },
  events: {
    signIn: (event) => {
      logger.info(`User successfully signed in, ${event.user.webid} (${event.account!.provider}).`);
    },
    signOut: (token) => {
      logger.info(`Signing out user ${token.token.webid}.`);
    },
  },
  logger: {
    error(code, metadata) {
      logger.error(metadata, code);
    },
    warn(code) {
      logger.warn(code);
    },
    debug(code, metadata) {
      logger.debug(metadata, code);
    },
  },
};

/**
 * Wrapper for `getServerSession` so that you don't need to import the
 * `authOptions` in every file.
 *
 * @see https://next-auth.js.org/configuration/nextjs
 **/
export const getServerAuthSession = (ctx: {
  req: GetServerSidePropsContext['req'];
  res: GetServerSidePropsContext['res'];
}) => {
  return getServerSession(ctx.req, ctx.res, authOptions);
};
