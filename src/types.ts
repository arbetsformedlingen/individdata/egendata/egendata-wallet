export type ExtendedRequestState =
  | 'received'
  | 'fetching'
  | 'available'
  | 'preview'
  | 'consent'
  | 'sharing'
  | 'shared'
  | 'timeout'
  | 'missing'
  | 'error';

// export type RequestState = 'received' | 'fetching' | 'available' | 'shared';

export enum RequestState {
  RECEIVED = 'received',
  FETCHING = 'fetching',
  AVAILABLE = 'available',
  SHARED = 'shared',
  SHARING = 'sharing',
}

export type MinimalRequestInfo = {
  url: URL;
  state: ExtendedRequestState;
};

export type RequestInfo = MinimalRequestInfo & {
  uuid: string;
  type: string;
  purpose: string;
  created: Date;
  providerWebId: URL;
  requestorWebId: URL;
  returnUrl: URL;
  documentType: string;
  unread?: boolean;
};

export type OrganizationAgentInfo = {
  name: string;
  logo?: string;
  storage: string;
};

export type RequestWithDetails = RequestInfo & {
  provider: OrganizationAgentInfo & {
    documentTitle?: LocalizedString;
  };
  requestor: OrganizationAgentInfo & {
    consentText?: LocalizedString;
    consentChecks?: string[]; // { [key: string]: string }[]
  };
};

export type Notification = {
  type: string[];
  object: { topic: URL; id: URL };
  published: Date;
  unsubscribe_endpoint: URL;
};

export type CredentialSubject = {
  type: string;
  subject: string;
  status: boolean;
  statusChangedDate: Date;
};

export type LocalizedString = { [key: string]: string };
