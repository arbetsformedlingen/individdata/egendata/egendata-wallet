import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import { extractDocumentTitle } from '@app/utils/egendataInteropUtil';

import { Registry, RegistrySet } from '@egendata/egendata-interop';

vi.mock('@egendata/egendata-interop', () => {
  const RegistrySet = vi.fn();
  RegistrySet.prototype.dataRegistries = vi.fn();
  RegistrySet.prototype.map = vi.fn();
  return { RegistrySet };
});

describe.skip('egendata-interop-mocked', () => {
  let registrySet: any;

  beforeEach(() => {
    registrySet = new RegistrySet(new URL('http://example.com/profile/card#me'), vi.fn());
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  it('should return registry for egendata', async () => {
    const registries: Registry[] = [];
    registrySet.dataRegistries.mockResolvedValueOnce(registries);
    const webid = new URL('https://idp-dev.egendata.se/egendata/profile/card#me');
    const documentTitle = await extractDocumentTitle(webid, 'JobSeekerRegistrationStatus');
    expect(documentTitle).toBe('Arbetslöshetsintyg');
  });
});

describe.skip('egendata-interop', () => {
  let registrySet: any;

  beforeEach(() => {
    registrySet = new RegistrySet(new URL('http://example.com/profile/card#me'), vi.fn());
  });

  afterEach(() => {
    vi.clearAllMocks();
  });

  it('should return registry for egendata', async () => {
    const registries: Registry[] = [];
    registrySet.dataRegistries.mockResolvedValueOnce(registries);
    const webid = new URL('https://idp-dev.egendata.se/egendata/profile/card#me');
    const documentTitle = await extractDocumentTitle(webid, 'JobSeekerRegistrationStatus');
    expect(documentTitle).toBe('Arbetslöshetsintyg');
  });
});
