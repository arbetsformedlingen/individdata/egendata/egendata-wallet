import { env } from '@app/env.mjs';
import fetchFactory, { FetchInterface, getInfraAccessToken } from '@app/lib/fetchFactory';
import { localizedStringFromLiterals } from '@app/lib/i18n';
import { logger as parentLogger } from '@app/lib/logger';
import { LocalizedString } from '@app/types';
import { RegistrySet } from '@egendata/egendata-interop';
import { Literal } from 'n3';

export const logger = parentLogger.child({ module: 'egendataInterop' });

const cache: { infraFetch?: FetchInterface; expiresAt?: number } = {};

const getInfaFetch = async () => {
  if (cache.infraFetch && cache.expiresAt && cache.expiresAt > Date.now()) {
    return cache.infraFetch;
  }

  const { accessToken, dpopKeyPair, expiresAt } = await getInfraAccessToken();
  cache.infraFetch = fetchFactory({ keyPair: dpopKeyPair, dpopToken: accessToken });
  cache.expiresAt = expiresAt;

  logger.debug({ cache }, 'Fetched new access tokens for infra.');

  return cache.infraFetch;
};

export const extractDocumentTitle = async (webid: URL, documentType: string): Promise<LocalizedString> => {
  const infraFetch = await getInfaFetch();

  const dataRegistries = (await new RegistrySet(webid, infraFetch).dataRegistries) ?? [];

  const shapeTree = new URL('http://data.example/shapetree/pm#DocumentTypeMetadata');
  const search = {
    pred: `${env.EGENDATA_SCHEMA_URL}documentType`,
    obj: documentType,
  };

  const registrations = (
    await Promise.all(dataRegistries.map((registry) => registry.registrations.find(shapeTree)))
  ).flat();
  // .filter((registration) => registration !== undefined);

  const stores = (
    await Promise.all(
      registrations.map((registration) => {
        return registration?.children.find(search);
      }),
    )
  ).flat();

  const documentStrings = stores
    .map((store) =>
      store?.getQuads(null, `${env.EGENDATA_SCHEMA_URL}documentTitle`, null, null).map((quad) => {
        return quad.object as Literal;
      }),
    )
    .flat();

  if (documentStrings.length === 0) {
    throw new Error('Expected a document title');
  }

  return localizedStringFromLiterals(documentStrings);
};

export const extractPurpose = async (webid: URL, documentType: string) => {
  const infraFetch = await getInfaFetch();
  const dataRegistries = (await new RegistrySet(webid, infraFetch).dataRegistries) ?? [];

  const shapeTree = new URL('http://data.example/shapetree/pm#RequestMetadata');
  const search = {
    pred: `${env.EGENDATA_SCHEMA_URL}documentType`,
    obj: documentType,
  };

  const registrations = (
    await Promise.all(
      dataRegistries.map((registry) => {
        return registry.registrations.find(shapeTree);
      }),
    )
  )
    .flat()
    .filter((registration) => registration !== undefined);

  const stores = (
    await Promise.all(
      registrations.map(async (registration) => {
        const children = registration?.children.find(search);
        return children;
      }),
    )
  ).flat();

  const purposes = stores
    .map((store) =>
      store?.getQuads(null, `${env.EGENDATA_SCHEMA_URL}purpose`, null, null).map((quad) => {
        return quad.object as Literal;
      }),
    )
    .flat();

  if (purposes.length === 0) {
    throw new Error('Expected a purpose');
  }

  return localizedStringFromLiterals(purposes);
};

export const extractConsentText = async (webid: URL, documentType: string) => {
  const infraFetch = await getInfaFetch();
  const dataRegistries = (await new RegistrySet(webid, infraFetch).dataRegistries) ?? [];

  const shapeTree = new URL('http://data.example/shapetree/pm#RequestMetadata');
  const search = {
    pred: `${env.EGENDATA_SCHEMA_URL}consentText`,
  };

  const registrations = (
    await Promise.all(
      dataRegistries.map((registry) => {
        return registry.registrations.find(shapeTree);
      }),
    )
  )
    .flat()
    .filter((registration) => registration !== undefined);

  const stores = (
    await Promise.all(
      registrations.map(async (registration) => {
        const children = registration?.children.find(search);
        return children;
      }),
    )
  ).flat();

  const consentTexts = stores
    .map((store) =>
      store?.getQuads(null, `${env.EGENDATA_SCHEMA_URL}consentText`, null, null).map((quad) => {
        return quad.object as Literal;
      }),
    )
    .flat();

  if (consentTexts.length === 0) {
    throw new Error('Expected a consentText');
  }

  return localizedStringFromLiterals(consentTexts);
};

export const extractConsentCheck = async (webid: URL, documentType: string): Promise<string[]> => {
  const infraFetch = await getInfaFetch();
  const dataRegistries = (await new RegistrySet(webid, infraFetch).dataRegistries) ?? [];

  const shapeTree = new URL('http://data.example/shapetree/pm#RequestMetadata');
  const search = {
    pred: `${env.EGENDATA_SCHEMA_URL}consentCheck`,
  };

  const registrations = (
    await Promise.all(
      dataRegistries.map((registry) => {
        return registry.registrations.find(shapeTree);
      }),
    )
  )
    .flat()
    .filter((registration) => registration !== undefined);

  const stores = (
    await Promise.all(
      registrations.map(async (registration) => {
        const children = registration?.children.find(search);
        return children;
      }),
    )
  ).flat();

  const consentChecks = stores
    .map((store) =>
      store?.getQuads(null, `${env.EGENDATA_SCHEMA_URL}consentCheck`, null, null).map((quad) => {
        return quad.object as Literal;
      }),
    )
    .flat();

  return consentChecks.map((literal) => literal?.value) as string[];
};
