import { rest } from 'msw';
import { turtle } from './ctx/turtle';
import { env } from '@app/env.mjs';

export const urls = {
  PERSON_WEBID: `${env.IDP_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/profile/card#me`,
  PERSON: `${env.IDP_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/profile/card`,
  PERSON_PRIVATE: `${env.IDP_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/profile/private`,
  ORG_PROVIDER_WEBID: `${env.IDP_BASE_URL}template-test1-source/profile/card#me`,
  ORG_PROVIDER: `${env.IDP_BASE_URL}template-test1-source/profile/card`,
  ORG_REQUESTOR_WEBID: `${env.IDP_BASE_URL}template-test1-sink/profile/card#me`,
  ORG_REQUESTOR: `${env.IDP_BASE_URL}template-test1-sink/profile/card`,
};

export const handlers = [
  // Public part of Person profile
  rest.get(urls.PERSON, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/personalAgentProfile.ttl'));
  }),

  // Private part of Person profile
  rest.get(urls.PERSON_PRIVATE, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/personalAgentProfilePrivate.ttl'));
  }),

  // Organization profile, provider
  rest.get(`${env.IDP_BASE_URL}template-test1-source/profile/card`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/organisationalAgentProfileProvider.ttl'));
  }),

  // Organization profile, requestor, without logo
  rest.get(`${env.IDP_BASE_URL}template-test1-sink/profile/card`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/organisationalAgentProfileRequestor.ttl'));
  }),

  // Erronous profile document
  rest.get('https://example.com/some/other/profile', (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/unknownTypeProfile.ttl'));
  }),
];
