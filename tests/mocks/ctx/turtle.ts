import { compose, context } from 'msw';
import Mustache from 'mustache';
import { env } from '@app/env.mjs';

import { readFileSync } from 'fs';
import path from 'path';

export const turtle = (file: string) => {
  const basePath = process.cwd();
  const ttl = readFileSync(path.join(basePath, file));

  return compose(context.set('Content-Type', 'text/turtle'), context.body(Mustache.render(ttl.toString(), env)));
};
