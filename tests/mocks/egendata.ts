import { rest } from 'msw';
import { turtle } from './ctx/turtle';
import { env } from '@app/env.mjs';

export const urls = {
  EGENDATA_WEBID: `${env.IDP_BASE_URL}egendata/profile/card#me`,
  REGISTRYSET: `${env.IDP_BASE_URL}egendata/registryset`,
  DATA_REGISTRY: `${env.IDP_BASE_URL}egendata/data/registry`,
  DATA_REGISTRATION: `${env.IDP_BASE_URL}egendata/data/f33ae00d-8f62-46ed-aac8-40335886e7e2`,
  DATA: `${env.IDP_BASE_URL}egendata/data/bded8341-0a63-4634-abe4-3627f4e6cab7`,
};

export const handlers = [
  // Egendata profile
  rest.get(urls.EGENDATA_WEBID.split('#')[0], (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/egendataProfile.ttl'));
  }),

  // Egendata Registry Set
  rest.get(urls.REGISTRYSET, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/egendataRegistrySet.ttl'));
  }),

  // Egendata Data Registry
  rest.get(urls.DATA_REGISTRY, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/egendataDataRegistry.ttl'));
  }),

  // Egendata Data Registraton
  rest.get(urls.DATA_REGISTRATION, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/egendataDataRegistration-f33ae00d-8f62-46ed-aac8-40335886e7e2.ttl'));
  }),

  // Egendata Data Entity
  rest.get(urls.DATA, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/egendataData-bded8341-0a63-4634-abe4-3627f4e6cab7.ttl'));
  }),
];
