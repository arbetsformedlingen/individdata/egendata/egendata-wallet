import { rest } from 'msw';
import { turtle } from './ctx/turtle';
import { env } from '@app/env.mjs';

export const handlers = [
  // Pristine pod
  rest.get(`${env.POD_BASE_URL}e401cb59-a516-4a57-9c43-6edaf58bdd92/egendata/inbox/.acl`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/podAcl.ttl'));
  }),

  // /.well-known/solid
  rest.get(`${env.POD_BASE_URL}.well-known/solid`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/well-known-solid.ttl'));
  }),

  // Notification endpoint
  rest.post(`${env.POD_BASE_URL}.notifications/WebSocketChannel2023/`, (req, res, ctx) => {
    return res(
      ctx.json({
        '@env': 'https://www.w3.org/ns/solid/notification/v1',
        type: 'http://www.w3.org/ns/solid/notifications#WebSocketChannel2023',
        receiveFrom: `${env.POD_BASE_URL}.notifications/WebSocketChannel2023/6ffc2e6f-2e86-4510-a4a6-df4e24955b7c`,
      }),
    );
  }),

  // Inbox
  rest.get(`${env.POD_BASE_URL}934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/inbox/`, (req, res, ctx) => {
    return res(
      ctx.set('Content-Type', 'text/turtle'),
      ctx.body(`@prefix dc: <http://purl.org/dc/terms/>.
      @prefix ldp: <http://www.w3.org/ns/ldp#>.
      @prefix posix: <http://www.w3.org/ns/posix/stat#>.
      @prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
      
      <> a ldp:Container, ldp:BasicContainer, ldp:Resource;
          dc:modified "2023-05-10T06:46:29.000Z"^^xsd:dateTime.

      <3388295f-356b-4182-af27-c950ed2244f8> a ldp:Resource, <http://www.w3.org/ns/iana/media-types/text/turtle#Resource>;
          dc:modified "2022-11-25T15:49:33.000Z"^^xsd:dateTime.
      <78827cc7-fd3c-4a9d-9f46-d0187a65157f> a ldp:Resource, <http://www.w3.org/ns/iana/media-types/text/turtle#Resource>;
          dc:modified "2023-01-03T14:22:38.000Z"^^xsd:dateTime.
      <557442fb-1d9e-4d47-b1f1-15ab51f47a06> a ldp:Resource, <http://www.w3.org/ns/iana/media-types/text/turtle#Resource>;
          dc:modified "2022-11-28T10:11:01.000Z"^^xsd:dateTime.
      <> posix:mtime 1683701189;
          ldp:contains <3388295f-356b-4182-af27-c950ed2244f8>, <78827cc7-fd3c-4a9d-9f46-d0187a65157f>, <557442fb-1d9e-4d47-b1f1-15ab51f47a06>.
      <3388295f-356b-4182-af27-c950ed2244f8> posix:mtime 1669391373.
      <78827cc7-fd3c-4a9d-9f46-d0187a65157f> posix:mtime 1672755758.
      <557442fb-1d9e-4d47-b1f1-15ab51f47a06> posix:mtime 1669630261.
      `),
    );
  }),

  // Notification of success
  rest.get(
    `${env.POD_BASE_URL}934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/inbox/4c824b1a-8b92-4a1d-a29c-893cb70433f7`,
    (req, res, ctx) => {
      return res(
        ctx.set('Content-Type', 'text/turtle'),
        ctx.body(`@prefix egendata: <https://egendata.se/schema/core/v1#> .
      <> a egendata:InboundDataResponseLink ;
        egendata:requestId "0a6d0b54-81d1-4ccd-90a6-13c94fa3fdc6" ;
        egendata:dataLocation <${env.POD_BASE_URL}934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/requests/7f117ebe-c6b8-43bf-b404-3881adf310fd/data> .`),
      );
    },
  ),
  // Notification of error
  rest.get(
    `${env.POD_BASE_URL}934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/inbox/dae5a2d8-4b05-4a86-8767-72a992fa737d`,
    (req, res, ctx) => {
      return res(
        ctx.set('Content-Type', 'text/turtle'),
        ctx.body(`@prefix egendata: <https://egendata.se/schema/core/v1#> .
      <> a egendata:InboundDataResponseError ;
        egendata:requestId "0a6d0b54-81d1-4ccd-90a6-13c94fa3fdc6" ;
        egendata:fetchLocation <${env.POD_BASE_URL}934651e2-45a2-409f-89cd-ec7a4efdfcd5/egendata/requests/7f117ebe-c6b8-43bf-b404-3881adf310fd/fetch> ;
        egendata:errorId "E1" ;
        egendata:errorMessage "Error message for logging purposes." .`),
      );
    },
  ),
];
