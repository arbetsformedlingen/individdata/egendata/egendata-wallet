import { rest } from 'msw';
import { turtle } from './ctx/turtle';
import { env } from '@app/env.mjs';

const base = new URL('f64de22f-f370-48dd-b960-5e40c01b0f1e/', env.POD_BASE_URL);

export const urls = {
  BASE: base,
  RECEIVED: new URL('egendata/requests/048ff7da-f622-44a7-96f5-e68abf7876e3/', base).toString(),
  FETCHING: new URL('egendata/requests/1cf68ff4-06a4-47dd-a8be-ad365f6c07f5/', base).toString(),
  AVAILABLE: new URL('egendata/requests/f6616ed8-3ff8-4050-a654-32a8b39bd40a/', base).toString(),
  SHARED: new URL('egendata/requests/225d6fc1-b02a-4797-b1e2-3d2f026d7b31/', base).toString(),
};

export const handlers = [
  // Request in state received
  rest.get(urls.RECEIVED, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/receivedRequestContainer.ttl'));
  }),
  rest.get(`${urls.RECEIVED}subject`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/receivedRequestSubject.ttl'));
  }),

  // Request in state fetching
  rest.get(urls.FETCHING, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/fetchingRequestContainer.ttl'));
  }),
  rest.get(`${urls.FETCHING}subject`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/fetchingRequestSubject.ttl'));
  }),
  rest.get(`${urls.FETCHING}fetch`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/fetchingRequestFetch.ttl'));
  }),
  rest.get(`${urls.FETCHING}data`, (req, res, ctx) => {
    return res(ctx.body(''));
  }),

  // Request in state available
  rest.get(urls.AVAILABLE, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/availableRequestContainer.ttl'));
  }),
  rest.get(`${urls.AVAILABLE}subject`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/availableRequestSubject.ttl'));
  }),
  rest.get(`${urls.AVAILABLE}fetch`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/availableRequestFetch.ttl'));
  }),
  rest.get(`${urls.AVAILABLE}data`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/availableRequestData.ttl'));
  }),

  // Request in state shared
  rest.get(urls.SHARED, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/sharedRequestContainer.ttl'));
  }),
  rest.get(`${urls.SHARED}subject`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/sharedRequestSubject.ttl'));
  }),
  rest.get(`${urls.SHARED}fetch`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/sharedRequestFetch.ttl'));
  }),
  rest.get(`${urls.SHARED}data`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/sharedRequestData.ttl'));
  }),
  rest.get(`${urls.SHARED}consent`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/sharedRequestConsent.ttl'));
  }),
];
