import { rest } from 'msw';
import { turtle } from './ctx/turtle';
import { env } from '@app/env.mjs';

export const handlers = [
  // Provider template document
  rest.get(
    `${env.POD_BASE_URL}template-test1-source/egendata/templates/JobSeekerRegistrationStatus`,
    (req, res, ctx) => {
      return res(turtle('tests/mocks/turtle/templateDocProvider.ttl'));
    },
  ),

  // Consumer template document
  rest.get(`${env.POD_BASE_URL}template-test1-sink/egendata/templates/JobSeekerRegistrationStatus`, (req, res, ctx) => {
    return res(turtle('tests/mocks/turtle/templateDocRequestor.ttl'));
  }),
];
