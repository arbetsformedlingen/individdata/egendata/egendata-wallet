import { afterEach, expect } from 'vitest';
import { cleanup } from '@testing-library/react';
import matchers from '@testing-library/jest-dom/matchers';
import { fetch } from 'whatwg-fetch';
import { config as dotenv } from 'dotenv';

dotenv();

expect.extend(matchers);

afterEach(() => {
  cleanup();
});

// Required for MSW to work.
global.fetch = fetch;
