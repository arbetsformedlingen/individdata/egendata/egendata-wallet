import { env } from '@app/env.mjs';

export const fakeSession = () => {
  const webid = `${env.IDP_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/profile/card#me`;
  const keys = {
    privateKey: {
      kty: 'EC',
      x: 'Wnohu2u10nvVK9DLVzjA59o-KQlqTuDpHAD8pivt-WY',
      y: 'Ey1yhc8dYegX5dNXBBfp56qAr4u9tQdrU18uFfS5rYU',
      crv: 'P-256',
      d: '8leggABqT77jMfI2fPQ5abGYxLg4yuMMqAw2Yb8ftrc',
    },
    publicKey: {
      kty: 'EC',
      x: 'Wnohu2u10nvVK9DLVzjA59o-KQlqTuDpHAD8pivt-WY',
      y: 'Ey1yhc8dYegX5dNXBBfp56qAr4u9tQdrU18uFfS5rYU',
      crv: 'P-256',
    },
  };
  const dpopToken =
    'eyJhbGciOiJFUzI1NiIsInR5cCI6ImF0K2p3dCIsImtpZCI6ImhEOXpYYTdQNkJTaUtPaGNVWFRRM0Eya2sycjVSVEh4ajhvX0dGYnl3ZFUifQ.eyJ3ZWJpZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzAwMi9kNWI4NWI1YS1iYmMyLTQ3MGYtOWRhNi05Nzc5Yzc0YjQ4NDcvcHJvZmlsZS9jYXJkI21lIiwianRpIjoid1dtdDRINmRYZmpYUVEwazk0aUcwIiwic3ViIjoiaHR0cDovL2xvY2FsaG9zdDozMDAyL2Q1Yjg1YjVhLWJiYzItNDcwZi05ZGE2LTk3NzljNzRiNDg0Ny9wcm9maWxlL2NhcmQjbWUiLCJpYXQiOjE2Njg1MDYyMDYsImV4cCI6MTY2ODUwOTgwNiwic2NvcGUiOiJ3ZWJpZCIsImNsaWVudF9pZCI6ImU3bERUT25FUnlzb243X3NVVVA5ciIsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzAwMi8iLCJhdWQiOiJzb2xpZCIsImNuZiI6eyJqa3QiOiJQb2drWXlMbkZINWdCbkE2OXA2VEVDNmhDdnF3cURRMHNmZTNJLWxDOEhBIn19.AuMVX0lFR4jIbuFZuinJ_ZmhrkoTQym0sTbDNCMDRT3pPoVQej6-U5gzTNYraOIs5aCe2ARbqjdmKb7WCQDiqg';

  return {
    session: {
      webid,
      wsid: '',
      storage: `${env.POD_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/`,
      seeAlso: `${env.IDP_BASE_URL}f64de22f-f370-48dd-b960-5e40c01b0f1e/profile/private`,
      keys,
      dpopToken,
      idToken: '',
      subscription: '',
      user: {
        id: webid,
        webid,
      },
      expires: '',
    },
    token: {
      keys,
      dpopToken,
    },
  };
};
