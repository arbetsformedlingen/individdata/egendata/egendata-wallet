import path from 'path';
import { configDefaults, defineConfig } from 'vitest/config';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  test: {
    outputDiffMaxSize: 5000,
    alias: {
      '@app': path.resolve(__dirname, './src'),
      '@ui': path.resolve(__dirname, './src/components/ui'),
      '@tests': path.resolve(__dirname, './tests'),
    },
    environment: 'jsdom',
    env: {
      EGENDATA_SCHEMA_URL: 'https://egendata.se/schema/core/v1#',
      NEXTAUTH_URL: 'https://localhost',
      IDP_BASE_URL: 'https://idp.localhost/',
      POD_BASE_URL: 'https://pod.localhost/',
    },
    exclude: [...configDefaults.exclude, '**/.devcontainer/**', '*.stories.(t|j)sx?', 'e2e/**'],
    setupFiles: ['./tests/setup.ts'],
    coverage: {
      all: true,
      include: ['src/**'],
      exclude: [
        ...configDefaults.coverage.exclude!,
        '.devcontainer/**',
        '.storybook/**',
        '__mocks__/**',
        '**/*{.,-}{stories,spec}.{js,cjs,mjs,ts,tsx,jsx}',
      ],
      provider: 'istanbul',
      reporter: ['text', 'html'],
    },
  },
});
